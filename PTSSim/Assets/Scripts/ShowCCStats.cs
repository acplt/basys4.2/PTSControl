﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script to display the general <see cref="ControlComponent"/> states in a text component, which is cyclic updated.
/// </summary>
public class ShowCCStats : MonoBehaviour
{
    [Tooltip("Time between polling new state information from the control component. Can only be changed on startup.")]
    public float UpdateTime = 0.1f;
    [Tooltip("Show the sender id of the current occupier controlling this control component.")]
    public bool OCCUPIER = true;
    [Tooltip("Show the current occupation state (FREE, OCCUPIED, PRIO)")]
    public bool OCCST = false;
    [Tooltip("Show the current execution mode (AUTO, MANUAL, ...)")]
    public bool EXMODE = false;
    [Tooltip("Show the current execution state (PACKML: IDLE, EXECUTE, STOPPED, ...)")]
    public bool EXST = true;
    [Tooltip("Show the current operation mode, which is control component type specific. (e.g. FPASS, FTAKE, ...)")]
    public bool OPMODE = true;
    [Tooltip("Show the current working state. (Additional non normative information of the current step in the operation mode)")]
    public bool WORKST = true;

    private ControlComponent cc;
    private Text text;

    public void Start()
    {
        cc = GetComponentInParent<ControlComponent>();
        text = GetComponent<Text>();
        if (cc != null && text)
            InvokeRepeating(nameof(UpdateText), 0, UpdateTime);
        else if (text)
            text.text = "Didn't find ControlComponent component in game object.";
    }

    private void UpdateText()
    {
        text.text = "States:";
        if (OCCUPIER)
            text.text += "\nOCCUPIER: " + cc.OCCUPIER;
        if (OCCST)
            text.text += "\nOCCST: " + cc.OCCST;
        if (EXMODE)
            text.text += "\nEXMODE: " + cc.EXMODE;
        if (EXST)
            text.text += "\nEXST: " + cc.EXST;
        if (OPMODE)
            text.text += "\nOPMODE: " + ((cc.OPMODE != null) ? cc.OPMODE.OpModeName : "NONE");
        if (WORKST)
            text.text += "\nWORKST: " + cc.WORKST;
    }
}
