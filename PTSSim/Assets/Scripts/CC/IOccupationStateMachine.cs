﻿using System;
using UnityEngine;

public enum OccupationState
{
    FREE, OCCUPIED, PRIO, LOCAL
}
[Flags]
public enum OccupationFacet
{
    UNKNOWN = 0,
    NONE = 1,
    OCCUPIED = 2,
    PRIO = 4,
    LOCAL = 8
}

public interface IOccupationStateMachine
{
    string OCCUPIER { get; }
    OccupationState OCCST { get; }
    void Free(string sender);
    void Occupy(string sender);
    void Prio(string sender);
    OccupationFacet OC { get; }
}

public partial class ControlComponent : IOccupationStateMachine
{
    public const string OCCUPIER_NONE = "NONE";
    public string OCCUPIER { get; private set; } = OCCUPIER_NONE;
    public OccupationState OCCST { get; private set; } = OccupationState.FREE;
    //TODO Default to NONE and make konfigurable from unity editor
    public OccupationFacet OC { get; private set; } = OccupationFacet.OCCUPIED | OccupationFacet.PRIO;

    public void Free(string sender)
    {
        if (IsOccupied(sender))
        {
            OCCUPIER = OCCUPIER_NONE;
            OCCST = OccupationState.FREE;
        }
    }
    public void Free(ControlComponent sender) {
        Free(sender.name);
    }

    public void Occupy(string sender)
    {
        if((OC & OccupationFacet.OCCUPIED) > 0 && IsFree())
        {
            OCCUPIER = sender;
            OCCST = OccupationState.OCCUPIED;
        }
    }
    public void Occupy(ControlComponent sender)
    {
        Occupy(sender.name);
    }

    public void Prio(string sender)
    {
        if((OC & OccupationFacet.PRIO) > 0)
        {
            if (IsOccupied(sender))
                OCCST = OccupationState.PRIO;
            else if (OCCST < OccupationState.PRIO)
            {
                OCCUPIER = sender;
                OCCST = OccupationState.PRIO;
            }
        }
    }
    public void Prio(ControlComponent sender)
    {
        Prio(sender.name);
    }

    // Checks if control component is free
    public bool IsFree()
    {
        return OCCUPIER == OCCUPIER_NONE;
    }

    // Checks if control component is free or occupied by specific sender
    public bool IsFree(ControlComponent sender)
    {
        return IsFree(sender.name);
    }
    public bool IsFree(string sender)
    {
        return OCCUPIER.Equals(sender) || OCCUPIER.Equals(OCCUPIER_NONE);
    }

    // Checks if control component is occupied at all
    public bool IsOccupied()
    {
        return !OCCUPIER.Equals(OCCUPIER_NONE);
    }

    // Checks if control component is occupied by the sender except for OCCUPIER_NONE ("NONE")
    public bool IsOccupied(ControlComponent sender)
    {
        return IsOccupied(sender.name);
    }
    public bool IsOccupied(string sender)
    {
        return OCCUPIER.Equals(sender) && !sender.Equals(OCCUPIER_NONE);
    }

    // Internal check, before other orders are executed
    private bool CheckOccupation(string sender)
    {
        if (OC == OccupationFacet.NONE)
            return true; // Skip occupation check
        if (IsOccupied(sender))
            return true; 
        Debug.Log("Order from " + sender + " denied. Current OCCUPIER = " + OCCUPIER);
        return false;
    }
}