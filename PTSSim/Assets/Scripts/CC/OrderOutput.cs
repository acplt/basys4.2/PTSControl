﻿using System.Collections;
using UnityEngine;

public class OrderOutput : MonoBehaviour
{
    public enum OrderOutputState { OK, Completed, Stopped, NotExisting, NullRequested, NotExecuting, NotAccepted, Occupied };

    [Tooltip("The technical role of this order output (e.g. motor, entryPump, exitPump, currentConveyor, ...)")]
    public string Role;

    [Tooltip("Realisation unit, which is currently assigned to the role (= actor)")]
    // TODO discuss, whether to use interface definition like IMotor here (and in SCC)
    [SerializeField]
    public ControlComponent CC;

    public OrderOutputState State;
    // Used to manage coroutines in ExecuteOpMode macro
    public IEnumerator Coroutine;
}
