﻿using UnityEngine;

public enum ExecutionState
{
    ABORTED, ABORTING, CLEARING,
    STOPPING, STOPPED, RESETTING,
    IDLE, STARTING,
    HOLDING, HELD, UNHOLDING,
    SUSPENDING, SUSPENDED, UNSUSPENDING,
    EXECUTE, COMPLETING, COMPLETED
};

public interface IExecutionStateMachine
{
    ExecutionState EXST { get; }

    void Start(string sender);
    void Stop(string sender);
    void Reset(string sender);
    void Abort(string sender);
    void Clear(string sender);
    void Hold(string sender);
    void Unhold(string sender);
}

// Handle execution state orders from outside
// Request are handed over to current operation mode, except no operation mode is currently selected.
// TODO log ignored requests
public partial class ControlComponent : IExecutionStateMachine
{
    public ExecutionState EXST { get => _EXST; private set { _EXST = value; } }
    private ExecutionState _EXST = ExecutionState.STOPPED;

    public void Abort(string sender)
    {
        if (!CheckOccupation(sender)) return;
        if(OPMODE == null)
            EXST = ExecutionState.ABORTED;
        else
        {
            EXST = ExecutionState.ABORTING;
            OPMODE.OnAbort();
        }
    }

    public void Clear(string sender)
    {
        if (!CheckOccupation(sender)) return;
        if (EXST == ExecutionState.ABORTED)
        {
            if (OPMODE == null)
                EXST = ExecutionState.STOPPED;
            else
            {
                EXST = ExecutionState.CLEARING;
                OPMODE.OnClear();
            }
        }
    }

    public void Stop(string sender)
    {
        if (!CheckOccupation(sender)) return;
        if (EXST != ExecutionState.ABORTED &&
            EXST != ExecutionState.ABORTING &&
            EXST != ExecutionState.CLEARING)
        {
            if (OPMODE == null)
                EXST = ExecutionState.STOPPED;
            else
            {
                EXST = ExecutionState.STOPPING;
                OPMODE.OnStop();
            }
        }
    }

    public void Reset(string sender)
    {
        if (!CheckOccupation(sender)) return;
        if (EXST == ExecutionState.COMPLETED ||
            EXST == ExecutionState.STOPPED)
        {
            if (OPMODE == null)
                EXST = ExecutionState.IDLE;
            else
            {
                OPMODE.OnReset();
                OPMODE = null;
            }
            _WORKST = "BSTATE";
        }

    }
    
    public void Start(string sender)
    {
        if (!CheckOccupation(sender)) return;
        if (EXST == ExecutionState.IDLE && OPMODE != null && WORKST.Equals(WORKST_READY))
        {
            EXST = ExecutionState.STARTING;
            OPMODE.OnStart();
        }

    }

    public void Hold(string sender)
    {
        if (!CheckOccupation(sender)) return;
        if (EXST == ExecutionState.EXECUTE)
            OPMODE.OnHold();
    }

    public void Unhold(string sender)
    {
        if (!CheckOccupation(sender)) return;
        if (EXST == ExecutionState.HELD)
            OPMODE.OnUnhold();
    }
}