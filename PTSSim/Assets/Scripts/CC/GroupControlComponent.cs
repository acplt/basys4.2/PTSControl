﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static OrderOutput;

public partial class ControlComponent
{
    //TODO Stop and Reset all occupied order outputs automatically

    /// Order outputs for group control components
    private Dictionary<string, OrderOutput> OrderOutputs;

    /// <summary>
    /// Macro function to let an underlying control component execute an specific
    /// operation mode.
    /// </summary>
    /// The execution of an operation mode includes the occupation(OCCUPY),
    /// resetting from STOPPED or COMPLETED execution state(RESET),
    /// setting of <see cref="OPMODE"/>(), and the start(START).
    /// After completion the order output is not freed(FREE).
    /// An opMode value of null stops the execution of the order output(STOP)
    /// and frees it(FREE).
    /// <param name="output">Name ot the order output (role).</param>
    /// <param name="opMode">Name of the operation mode that should be executed.
    /// opMode == null means stop current execution.</param>
    /// <param name="start">Starts the operation mode if desired, otherwise it is just prepared (ready).</param>
    /// <returns>Unity co routine iterator</returns>
    /// TODO inconsistend free after done and stop --> Don't free automatically
    private IEnumerator ExecuteOpMode(string output, string opMode, bool start)
    {
        OrderOutputs[output].State = OrderOutputState.OK;

        if (opMode != null && !OrderOutputs.ContainsKey(output))
        {
            OrderOutputs[output].State = OrderOutputState.NotExisting;
            yield break;
        }

        ControlComponent cc = OrderOutputs[output].CC;
        // Check if order output is set to execute an OpMode.
        if (cc == null)
        {
            OrderOutputs[output].State = OrderOutputState.NullRequested;
            yield break;
        }

        // Check if order output should be stopped or changed --> Bring cc to STOPPED/IDLE state
        if (cc.IsOccupied(this))
        {
            // Stop Execution
            if (opMode == null)
            {
                cc.Stop(name);
                cc.Free(this);
                OrderOutputs[output].State = OrderOutputState.Stopped;
                yield break;
            }

            // Determine current OpMode if not in NONE
            if (cc.OPMODE != null)
            {
                // Detect OpMode change request
                if (!cc.OPMODE.Equals(opMode))
                {
                    cc.Stop(name);
                    yield return new WaitWhile(() => cc.EXST == ExecutionState.STOPPING);
                    cc.Reset(name);
                    yield return new WaitWhile(() => cc.EXST == ExecutionState.RESETTING);
                    //TODO move reward to agent --> Use out parameter with errors in this 
                    //AddReward(ROrderOutputOpModeChange);
                }
                else
                {
                    if (cc.EXST == ExecutionState.STOPPED || cc.EXST == ExecutionState.COMPLETED)
                    {
                        // Is automatically reset on next step
                    }
                    else if (cc.EXST != ExecutionState.EXECUTE)
                    {
                        // What shell we do, when the OpMode is active but held, suspended, aborted or transient states?
                        OrderOutputs[output].State = OrderOutputState.NotExecuting;
                        yield break;
                    }
                    else
                    {
                        // Noting to do, OpMode is already executing (No restart?)
                        // Check for completion
                        yield return new WaitUntil(() => cc.EXST == ExecutionState.COMPLETED);
                        OrderOutputs[output].State = OrderOutputState.Completed;
                        yield break;
                    }
                }
            }
        }

        // Check occupation, select and execute opMode
        if (cc.IsFree(this) && opMode != null)
        {
            // Occupy component
            cc.Occupy(this);

            // Reset component if necessary
            if (cc.EXST == ExecutionState.COMPLETED || cc.EXST == ExecutionState.STOPPED)
            {
                cc.Reset(name);
                yield return new WaitWhile(() => cc.EXST == ExecutionState.RESETTING);
            }

            // Select operation mode
            if (!cc.SelectOperationMode(name, opMode))
            {
                OrderOutputs[output].State = OrderOutputState.NotAccepted;
                yield break;
            }
            yield return new WaitUntil(() => cc.WORKST.Equals(WORKST_READY));

            if (start)
            {
                // Start execution
                cc.Start(name);
                // Check for completion
                yield return new WaitUntil(() => cc.EXST == ExecutionState.COMPLETED);
            }
            OrderOutputs[output].State = OrderOutputState.Completed;
        }
        else
        {
            OrderOutputs[output].State = OrderOutputState.Occupied;
        }
    }

    private void OnDestroy()
    {
        foreach (var oo in OrderOutputs)
        {
            if (oo.Value != null && oo.Value.CC != null && oo.Value.CC.IsOccupied(name))
            {
                oo.Value.CC.Stop(name);
                oo.Value.CC.Free(name);
            }   
        }
    }
}
