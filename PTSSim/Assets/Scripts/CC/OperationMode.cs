﻿using System.Collections;
using UnityEngine;

public abstract class OperationMode : MonoBehaviour
{
    [Tooltip("The name that is used to call this operation mode.")]
    public string OpModeName;

    /// <summary>
    /// Is set from the control component while the operation mode is selected;
    /// Allows the operation mode to set EXST and WORKST.
    /// Allows the operation mode to get all component states.
    /// </summary>
    protected ControlComponent.OperationModeControl control;

    // TODO How to set operation mode parameter?
    public void Select(ControlComponent.OperationModeControl control)
    {
        this.control = control;
        StartCoroutine("OnSelected");
    }

    public void Deselect()
    {
        StopCoroutine("OnSelected");
        control = null;
    }

    //TODO add config for HasHoldCycle --> Don't allow HOLD command (use Stop instead?)

    /// <summary>
    /// This coroutine is executed while the operation mode is selected.
    /// Typically this is between a operation mode selection and the Resetting
    /// execution state.
    /// The operation mode needs to set the WORKST to WORKST_READY("READY"),
    /// to allow the Start command to be triggered.
    /// The control flow is handed over to the operation mode. Hence the EXST
    /// and WORKST variables need to be set according to the operation mode
    /// behavior or based on the function calls below.
    /// </summary>
    /// <returns>Enumerator for Unity Coroutine</returns>
    // TODO make a sketch / sequence diagram of the operation mode selection cycle.
    protected virtual IEnumerator OnSelected()
    {
        control.WORKST = ControlComponent.WORKST_READY;
        yield return new WaitUntil( () => control.EXST == ExecutionState.EXECUTE);
        while (true)
        {
            if (control.EXST == ExecutionState.EXECUTE)
                OnExecute();
            else if (control.EXST == ExecutionState.COMPLETING)
                OnCompleting();
            else if (control.EXST == ExecutionState.STOPPING
                || control.EXST == ExecutionState.STOPPED
                || control.EXST == ExecutionState.ABORTING
                || control.EXST == ExecutionState.ABORTED
                || control.EXST == ExecutionState.CLEARING
                || control.EXST == ExecutionState.RESETTING
                || control.EXST == ExecutionState.IDLE
                || control.EXST == ExecutionState.STARTING
                || control.EXST == ExecutionState.COMPLETING)
                break;
            yield return null;
        }
    }

    /* 
     * Instead or additionaly you can use the following operations to react to
     * orders from outside.
     * You need to make sure, that the EXST is set accordingly after completing
     * the required actions.
     * TODO wait vor underlying units to reach requested states.
     */
    // Start has to set EXST to EXECUTE after completion.
    public virtual void OnStart()
    {
        control.EXST = ExecutionState.EXECUTE;
    }
    // Stop has to set EXST to STOPPED after completion.
    public virtual void OnStop()
    {
        foreach (var output in control.OrderOutputs)
        {
            if (output.Value.CC && output.Value.CC.IsOccupied(control.cc))
                output.Value.CC.Stop(control.cc.name);
        }
        control.EXST = ExecutionState.STOPPED;
    }
    // Reset has to set EXST to IDLE after completion.
    public virtual void OnReset()
    {
        foreach (var output in control.OrderOutputs)
        {
            if (output.Value.CC && output.Value.CC.IsOccupied(control.cc))
                output.Value.CC.Reset(control.cc.name);
        }
        control.EXST = ExecutionState.IDLE;
    }
    // Abort has to set EXST to ABORTED after completion.
    public virtual void OnAbort()
    {
        foreach (var output in control.OrderOutputs)
        {
            if (output.Value.CC && output.Value.CC.IsOccupied(control.cc))
                output.Value.CC.Abort(control.cc.name);
        }
        control.EXST = ExecutionState.ABORTED;
    }
    // Clear has to set EXST to STOPPED after completion.
    public virtual void OnClear()
    {
        foreach (var output in control.OrderOutputs)
        {
            if (output.Value.CC && output.Value.CC.IsOccupied(control.cc))
                output.Value.CC.Clear(control.cc.name);
        }
        control.EXST = ExecutionState.STOPPED;
    }
    // Hold has to set EXST to HELD after completion.
    public virtual void OnHold()
    {
        foreach (var output in control.OrderOutputs)
        {
            if (output.Value.CC && output.Value.CC.IsOccupied(control.cc))
                output.Value.CC.Hold(control.cc.name);
        }
        control.EXST = ExecutionState.HELD;
    }
    // Unhold has to set EXST to EXECUTE after completion.
    public virtual void OnUnhold()
    {
        foreach (var output in control.OrderOutputs)
        {
            if (output.Value.CC && output.Value.CC.IsOccupied(control.cc))
                output.Value.CC.Unhold(control.cc.name);
        }
        control.EXST = ExecutionState.EXECUTE;
    }

    /// <summary>
    /// OnExecute is called cyclic, if OnSelected isn't overriden.
    /// If execution is finished set EXST to COMPLETING and call OnCompleting().
    /// <code>
    /// control.EXST = ExecutionState.COMPLETING;
    /// OnCompleting();
    /// </code>
    /// </summary>
    /// TODO maybe its better to use this as a coroutine ?
    protected virtual void OnExecute()
    {
    }
    /*
    * The following methods can be called from inside the operation mode (in an own OnSelect method or from OnExecute())
    */
    protected virtual void OnSuspend()
    {
        control.EXST = ExecutionState.SUSPENDED;
    }
    protected virtual void OnSuspended()
    {
        control.EXST = ExecutionState.UNSUSPENDING;
    }
    protected virtual void OnUnsuspend()
    {
        control.EXST = ExecutionState.EXECUTE;
    }
    protected virtual void OnCompleting()
    {
        control.EXST = ExecutionState.COMPLETED;
    }
}