﻿public enum ExecutionMode
{
    AUTO, SEMIAUTO, MANUAL, SIMULATE, TRAIN, LEARN
}

public interface IExecutionModeStateMachine
{
    ExecutionMode EXMODE { get; }
    void Auto(string sender);
    void SemiAuto(string sender);
    void Manual(string sender);
    void Simulate(string sender);
    void Train(string sender);
    void Learn(string sender);
}

// Handle execution mode orders from outside
public partial class ControlComponent : IExecutionModeStateMachine
{
    public ExecutionMode EXMODE { get; private set; } = ExecutionMode.AUTO;

    public void Auto(string sender)
    {
        if (!CheckOccupation(sender)) return;
        EXMODE = ExecutionMode.AUTO;
    }

    public void Learn(string sender)
    {
        throw new System.NotImplementedException();
    }

    public void Manual(string sender)
    {
        throw new System.NotImplementedException();
    }

    public void SemiAuto(string sender)
    {
        throw new System.NotImplementedException();
    }

    public void Simulate(string sender)
    {
        throw new System.NotImplementedException();
    }

    public void Train(string sender)
    {
        throw new System.NotImplementedException();
    }
}
