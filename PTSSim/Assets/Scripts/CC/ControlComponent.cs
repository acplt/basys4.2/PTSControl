﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



public partial class ControlComponent : MonoBehaviour
{
    public ControlComponent() : base()
    {
        opModeControl = new OperationModeControl(this);
    }

    private OperationMode _OPMODE;
    public OperationMode OPMODE {
        get => _OPMODE;
        private set
        {
            _OPMODE?.Deselect();
            _OPMODE = value;
            _OPMODE?.Select(opModeControl);
        }
    }
    public Dictionary<string, OperationMode> OperationModes;

    public string WORKST { get => _WORKST; }
    private string _WORKST = "INIT";
    public const string WORKST_READY = "READY";

    //TODO cyclic check interlocks and skills (e.g. in Update())
    //TODO Interlocks: List of callbacks, that get PVs and returns SPs

    protected OperationModeControl opModeControl;
    public class OperationModeControl
    {
        public OperationModeControl(ControlComponent cc)
        {
            this.cc = cc;
        }
        // Interface of the control component, for interaction from outside (e.g. read states or trigger a hold)
        public ControlComponent cc;

        /*
         * States, that can be updated by an selected operation mode (= when it is in control)
         */
        public ExecutionState EXST
        {
            get => cc.EXST;
            set => cc._EXST = value;
        }

        public string WORKST
        {
            get => cc.WORKST;
            set => cc._WORKST = value;
        }
        //TODO define other states?

        /* 
         * Actions that a selected operation mode may choose
         */
        // Direct interaction with order outputs
        public Dictionary<string, OrderOutput> OrderOutputs { get => cc.OrderOutputs; }

        public void PrepareOpMode(string output, string opMode)
        {
            StopExecuteOpMode(output);
            OrderOutputs[output].Coroutine = cc.ExecuteOpMode(output, opMode, false);
            cc.StartCoroutine(OrderOutputs[output].Coroutine);
        }

        /// <summary>
        /// Macro to exececute an operation mode at a specific order output.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="opMode"></param>
        public void ExecuteOpMode(string output, string opMode)
        {
            // Stop existing coroutine first
            StopExecuteOpMode(output);
            OrderOutputs[output].Coroutine = cc.ExecuteOpMode(output, opMode, true);
            cc.StartCoroutine(OrderOutputs[output].Coroutine);
        }

        public void StopExecuteOpMode(string output)
        {
            if (OrderOutputs[output].Coroutine != null)
                cc.StopCoroutine(OrderOutputs[output].Coroutine);
        }

        public void UpdateOrderOutputs()
        {
            cc.OrderOutputs.Clear();
            // Get all order outputs, that are connected to the game object
            cc.OrderOutputs = cc.GetComponents<OrderOutput>().ToDictionary(output => output.Role);
        }
        //TODO Skills: analog to interlocks --> can be called by Operation Mode
        //TODO add direct I/Os or Sensor / Actor interface for SingleControlComponents?
    }

    // TODO add polymorph for ..(string name, string parameter)
    public bool SelectOperationMode(string sender, string opModeName)
    {
        if (!CheckOccupation(sender))
        {
            return false;
        }
        // Check if operation mode is available
        if (OperationModes.ContainsKey(opModeName))
        {
            if (OPMODE == null && EXST == ExecutionState.IDLE)
            {
                OPMODE = OperationModes[opModeName];
                return true;
            }
            Debug.LogWarning("Operation mode " + opModeName + " not selected, because EXST of " + name + " is not IDLE.");
        }
        else
        {
            // TODO implement error state machine?
            Debug.LogWarning("Operation mode " + opModeName + " unknown in " + name);
        }
        return false;
    }

    public virtual void Start()
    {
        // Get all operation modes, that are connected to the game object
        OperationModes = GetComponents<OperationMode>().ToDictionary(opMode => opMode.OpModeName);
        // Get all order outputs, that are connected to the game object
        OrderOutputs = GetComponents<OrderOutput>().ToDictionary(output => output.Role);
    }

    /// <summary>
    /// Tries to bring the CC to IDLE execution state, no matter what the current state is.
    /// </summary>
    /// <param name="occupier">Name of the operator or control unit, that wants to reset the CC.</param>
    /// <param name="freeAfterReset">If true, the CC is freed afterwards</param>
    /// <returns></returns>
    public IEnumerator FullReset(string occupier, bool freeAfterReset)
    {
        // Bring control component to IDLE
        if (EXST != ExecutionState.IDLE || OPMODE != null)
        {
            // Occupy with higher priority to overwrite exisiting occupations
            Prio(occupier);
            // Wait if the control component is in aborting state, as this needs to be fixed by the component itself
            yield return new WaitWhile(() => EXST == ExecutionState.ABORTING);

            // Bring to STOPPED state
            if (EXST == ExecutionState.ABORTED)
                Clear(occupier);
            else
                Stop(occupier);
            yield return new WaitUntil(() => EXST == ExecutionState.STOPPED);

            // Reset to IDLE state
            Reset(occupier);
            yield return new WaitUntil(() => EXST == ExecutionState.IDLE);
        }
        // Free component if desired or leave occupied by the environment
        if (freeAfterReset)
        {
            if (IsOccupied())
                Free(occupier);
        }
        else
        {
            // Downgrade occupation from PRIO to OCCUPIED if necessary
            Occupy(occupier);
        }
    }
}