﻿using System.Collections;
using UnityEngine;
using Unity.MLAgents;
using System.Linq;
using System;
using System.Collections.Generic;

/// <summary>
/// This class defines the "Pallete Transport System" as a training environment for the palette agents.
/// It manages its conveyor and palette control components.
/// Resets the environment on episode changes: palette positions, palette targets, shift positions.
/// </summary>
/// <remarks>
/// The reset may also be started by pressing key "r".
/// Resets all conveyor control components and starts the palette control components.
/// The PTS observes the palette control components execution states for aborted, completed, stopped or idle state to determine episode ends and reset the environment.
/// May be used to read curriculum learning parameters and change the environment accordingly in the future.
/// </remarks>
public class PTS : MonoBehaviour
{
    [Tooltip("Prefab for the palette, which is used to populate the environment based on PaletteCount value.")]
    public GameObject PalettePrefab;
    [Tooltip("Array of all conveyor control components belonging to this PTS. (Is automatically filled during startup.)")]
    public ControlComponent[] Conveyors;
    [Tooltip("Array of all shift position sensors belonging to this PTS. (Is automatically filled during startup.)")]
    public ShiftPosition[] ShiftPositions;

    [Header("Trainer Environment Variables")]
    public bool RandomShiftPosition = false;
    public bool RandomStartPosition = false;
    public bool RandomTargetPosition = false;
    public uint PaletteCount = 1;
    public bool DestroyAgentsOnSuccess = false;


    // Flag to indicate that a reset was started, so that a reset isn't started over and over
    private bool resetting = false;
    private int ready = 0;
    private bool starting = false;
    private int ccCount = 0;
    /// <summary>
    /// List of all agents (control components with enabled MLOperationMode) watched by the PTS.
    /// </summary>
    private List<ControlComponent> agents = new List<ControlComponent>();
    /// <summary>
    /// List of all palettes in the PTS instanciated via EnvironmentReset based on parameters.
    /// </summary>
    public List<GameObject> Palettes = new List<GameObject>();


    public void Awake()
    {
        // Set environment reset callback to initalise the PTS correctly
        Academy.Instance.OnEnvironmentReset += EnvironmentReset;
    }

    public void Start()
    {
        // Find all conveyors
        Conveyors = Array.FindAll(GetComponentsInChildren<ControlComponent>(),
                cc => cc.CompareTag("Conveyor"));

        // Find all shift position sensors
        ShiftPositions = GetComponentsInChildren<ShiftPosition>();
    }


    /// <summary>
    /// Cyclic check of agents (control components with MLOperationMode enabled) state to initiate environment resets.
    /// Disables <see cref="agents"/> that are in COMPLETE state if <see cref="DestroyAgentsOnSuccess"/> is set.
    /// </summary>
    public void LateUpdate()
    {
        // Wait for resetting process to complete
        // TODO add a reset timeout as watchdog
        if (resetting)
        {
            // Start agents if all CCs are ready
            if (agents.Count > 0 && !starting && ready == ccCount)
            {
                agents.ForEach(a => StartCoroutine(StartMLOpMode(a)));
                ready = 0;
                starting = true;
            }
            // Wait till MLOpModes are Started (use ready counter in StarMLOpMode)
            else if (starting && agents.Count > 0 && ready == agents.Count)
                resetting = false;
            else if (Input.GetKey(KeyCode.R))
                EnvironmentReset();
        }
        else
        {
            // Check if all agents are done or at least one has a failure
            bool done = true;
            for (int i = agents.Count - 1; i >= 0; i--)
            {
                var a = agents[i];
                if (a.EXST == ExecutionState.COMPLETED
                        || a.EXST == ExecutionState.IDLE)
                {
                    // Destroy and delete agent if it is done
                    if (DestroyAgentsOnSuccess) {
                        Destroy(a.gameObject);
                        agents.Remove(a);
                    }
                }else if (a.EXST == ExecutionState.ABORTED
                    || a.EXST == ExecutionState.STOPPED) // TODO check if STOPPED counts as an error)
                {
                    done = true;
                    break;
                }
                else
                {
                    done = false;
                }
            }
            if(done || Input.GetKey(KeyCode.R))
                EnvironmentReset();
        }
    }

    /// <summary>
    /// Resets the whole PTS environment beneath this game object.
    /// </summary>
    /// <remarks>
    /// Could also be made public available to initiate a reset from outside
    /// </remarks>
    private void EnvironmentReset()
    {
        // indicate resetting process is active
        resetting = true;
        starting = false;

        // Delete remaining palettes
        StopAllCoroutines();
        if(PalettePrefab != null)
            Palettes.ForEach(p => Destroy(p));
        Palettes.Clear();

        // Set shift positions according to curriculum
        foreach (ShiftPosition shift in ShiftPositions)
        {
            if (Academy.Instance.EnvironmentParameters.GetWithDefault("RandomShiftPosition",
                    Convert.ToSingle(RandomShiftPosition)) != 0f)
                shift.SetRandomPosition();
            else
                shift.SetPosition(0);
        }

        // Use or spawn palettes according to curriculum or PaletteCount value
        int paletteCount = Convert.ToInt32(Academy.Instance.EnvironmentParameters.GetWithDefault("PaletteCount",
                    Convert.ToSingle(PaletteCount)));
        if(PalettePrefab == null)
        {
            Palette[] paletteArray = GetComponentsInChildren<Palette>();
            foreach (var palette in paletteArray)
            {
                Palettes.Add(palette.gameObject);
            }
            // Take minimum of all available palette prefabs
            paletteCount = Math.Min(paletteCount, Palettes.Count);
        }

        // Get random palette positions
        int[] randomIndex = Enumerable.Range(0, Conveyors.Length - 1).OrderBy(x => System.Guid.NewGuid()).Take(paletteCount).ToArray();
        for (int i = 0; i < paletteCount; i++)
        {
            GameObject palette;
            if (PalettePrefab == null)
                palette = Palettes[i];
            else
            {
                // Instanciate a new palette
                palette = Instantiate(PalettePrefab, gameObject.transform);
                // Add palette to watch list
                Palettes.Add(palette);
                palette.name = "PA" + i;
                palette.SetActive(true);
            }

            // Place palette random or at default position
            if (Academy.Instance.EnvironmentParameters.GetWithDefault("RandomStartPosition",
                    Convert.ToSingle(RandomStartPosition)) != 0f)
            {
                // Place palette at random position
                palette.GetComponent<Palette>().SetPosition(Conveyors[randomIndex[i]]);
            }
            else
            {
                // Place palette at default position
                palette.GetComponent<Palette>().SetPosition(Conveyors[paletteCount-1-i]);
            }

        }

        // Find all agents, start their MLOperationModes and refresh agents watch list.
        agents.Clear();
        ready = 0;
        ccCount = 0;
        int targetIdxCounter = 0;
        foreach (ControlComponent cc in GetComponentsInChildren<ControlComponent>())
        {
            // Check if the cc has an ML operation mode and if it is enabled --> consider it as an agent
            if(cc.TryGetComponent(out MLOperationMode opMode) && opMode.isActiveAndEnabled)
            {
                if (opMode.Goal.Length == 0)
                {
                    // TODO overwrite Goal for multiCoil case
                    // Set palette random or default target position
                    if (Academy.Instance.EnvironmentParameters.GetWithDefault("RandomTargetPosition",
                            Convert.ToSingle(RandomTargetPosition)) != 0f)
                    {
                        // Place palette at random position
                        // Set random goal position for all palettes
                        opMode.Goal = new float[] { UnityEngine.Random.Range(0, Conveyors.Length - 2)};
                    }
                    else
                    {
                        // Set exit as target for all palettes, if DestroyAgentsOnSuccess is true
                        // assume that the last conveyor is the exit
                        // Count in reverse order from last conveyor in list, if DestroyAgentsOnSuccess is false
                        opMode.Goal = new float[] { Conveyors.Length - 1 - (DestroyAgentsOnSuccess ? 0 : targetIdxCounter++) };
                    }
                }
                agents.Add(cc);
            }

            // Reset all other control components
            // TODO change in curriculum: set some conveyors to OCCUPIED or MANUAL block lanes?
            StartCoroutine(FullResetCC(cc));
            ccCount++;
        }
    }
    private IEnumerator FullResetCC(ControlComponent cc)
    {
        yield return StartCoroutine(cc.FullReset(name, true));
        ready++;
    }

    private IEnumerator StartMLOpMode(ControlComponent cc)
    {
        yield return StartCoroutine(cc.FullReset(name, false));

        string opModeName = cc.GetComponent<MLOperationMode>().OpModeName;
        cc.SelectOperationMode(name, opModeName);
        yield return new WaitUntil(() => cc.OPMODE.OpModeName.Equals(opModeName));

        cc.Start(name);
        yield return new WaitWhile(() => cc.EXST == ExecutionState.IDLE);
        ready++;
    }
}
