﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleCoilProcedure : MLOperationMode
{
    public override string MLMODEL { get => "CoilPathFinder"; }

    [Header("Application Specific settings.")]
    public PTS pts;
    public Palette palette;
    public float RCloserToTarget = 0.01f;

    private OrderOutput paletteCurrent, paletteLeft, paletteRight;
    private int targetIdx;
    private float targetDistance;


    private void Start()
    {
        OrderOutput[] outputs = palette.GetComponents<OrderOutput>();
        paletteCurrent = Array.Find(outputs, o => o.Role.Equals("Current"));
        paletteLeft = Array.Find(outputs, o => o.Role.Equals("Left"));
        paletteRight = Array.Find(outputs, o => o.Role.Equals("Right"));
        if (paletteCurrent == null || paletteLeft == null || paletteRight == null)
            Debug.LogError("Didn't find all orderoutputs of palette control component.");
    }

    public void OnPaletteCollision()
    {
        if (control != null) control.EXST = ExecutionState.ABORTING;
    }

    protected override void CollectObservations()
    {
        int conveyorCount = pts.Conveyors.Length;
        // Occupation states of conveyors
        for (int i = 0; i < conveyorCount; ++i)
        {
            // Is free (OCCUPIER == OCCUPIER_NONE)
            MLOBSERVE[i] = pts.Conveyors[i].IsFree() ? 1f : 0f;
            // Is occupied by palette (OCCUPIER == <name>)
            MLOBSERVE[i + conveyorCount] = pts.Conveyors[i].IsOccupied(palette.name) ? 1f : 0f;
            // Target position as one hot observation
            MLOBSERVE[i + 2*conveyorCount] = i == targetIdx ? 1f : 0f;
        }
    }

    protected override void SetEnabledActions()
    {
        MLENACT[0][0] = true; // Allow to do nothing
        MLENACT[0][1] = paletteLeft.CC != null
            && paletteLeft.CC.IsFree(control.cc); // Left
        MLENACT[0][2] = paletteRight.CC != null
            && paletteRight.CC.IsFree(control.cc); // Right
        MLENACT[0][3] = paletteCurrent.CC != null &&
            paletteCurrent.CC.OperationModes.ContainsKey("USHIFT"); // Up
        MLENACT[0][4] = paletteCurrent.CC != null &&
            paletteCurrent.CC.OperationModes.ContainsKey("DSHIFT"); // Down
    }

    protected override void MapDecisionToAction()
    {
        switch (MLDECIDE[0])
        {
            case 1: control.ExecuteOpMode("Palette", "LEFT"); break;
            case 2: control.ExecuteOpMode("Palette", "RIGHT");break;
            case 3: control.ExecuteOpMode("Palette", "UP"); break;
            case 4: control.ExecuteOpMode("Palette", "DOWN"); break;
        }
    }

    private float CalculateTargetDistance()
    {
        if (paletteCurrent.CC != null)
            return Vector3.Distance(paletteCurrent.CC.transform.position, pts.Conveyors[targetIdx].transform.position);
        return float.MaxValue;
    }

    public override void OnStart()
    {
        // Calculate observation space
        MLOBSERVE = new float[pts.Conveyors.Length * 3];
        // Calucalte action space
        MLENACT = new bool[1][];
        MLENACT[0] = new bool[5];

        // Set target from Goals parameter
        targetIdx = Mathf.RoundToInt(Goal[0]);
        if (targetIdx < 0 || targetIdx >= pts.Conveyors.Length)
        {
            control.WORKST = "TargetNull";
            control.EXST = ExecutionState.ABORTING;
            return;
        }

        // Try to occupy Palette CC
        if(control.OrderOutputs["Palette"].CC != null)
        {
            control.OrderOutputs["Palette"].CC.Occupy(control.cc);
            if(!control.OrderOutputs["Palette"].CC.IsOccupied(control.cc))
            {
                control.WORKST = "PaletteCCNotOccupied";
                control.EXST = ExecutionState.ABORTING;
                return;
            }
        }
        else
        {
            control.WORKST = "PaletteCCNotFound";
            control.EXST = ExecutionState.ABORTING;
            return;
        }

        // Initialize order outputs of palette
        //TODO move UpdateOrderOutputs from MLOpModePalette to Palette class and use instead?
        paletteCurrent.CC = palette.GetCurrentConveyor();
        paletteLeft.CC = palette.GetNextConveyor(false);
        paletteRight.CC = palette.GetNextConveyor(true);
        targetDistance = CalculateTargetDistance();

        // End STARTING state and go to EXECUTE
        base.OnStart();
    }

    protected override void OnSuspend()
    {
        // Check if target is reached
        if (paletteCurrent.CC == pts.Conveyors[targetIdx])
        {
            control.WORKST = "TargetReached";
            control.EXST = ExecutionState.COMPLETING;
        }
        else
        {
            // Add reward if closer to target
            float newTargetDistance = CalculateTargetDistance();
            if ((targetDistance - newTargetDistance) > 0.1f)
                MLREWARD += RCloserToTarget;
            // Add critic if farther from target
            else if ((targetDistance - newTargetDistance) < -0.1f)
                MLREWARD -= 2 * RCloserToTarget;
            // Update last target distance for next move
            targetDistance = newTargetDistance;
            control.WORKST = string.Format("Target:{0:f2}", targetDistance);

            // End suspending state --> got to SUSPENDED
            base.OnSuspend();
        }
    }

    protected override void OnCompleting()
    {
        control.OrderOutputs["Palette"].CC.Free(control.cc);
        base.OnCompleting();
    }

}
