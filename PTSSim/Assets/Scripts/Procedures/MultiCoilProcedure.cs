﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiCoilProcedure : MLOperationMode
{
    public override string MLMODEL { get => "MultiCoilPathFinder"; }

    [Header("Application Specific settings.")]
    public PTS pts;
    public float RCloserToTarget = 0.01f;

    private Palette[] palettes;
    private OrderOutput[,] paletteOutputs;
    private int[] targetIdx;
    private float[] targetDistance;

    public void OnPaletteCollision()
    {
        if (control != null) control.EXST = ExecutionState.ABORTING;
    }

    protected override void CollectObservations()
    {
        int conveyorCount = pts.Conveyors.Length;

        // Occupation states of conveyors
        for (int i = 0; i < conveyorCount; ++i)
        {
            // Is free (OCCUPIER == OCCUPIER_NONE)
            MLOBSERVE[i] = pts.Conveyors[i].IsFree() ? 1f : 0f;

            for (int j = 0; j < palettes.Length; j++)
            {
                // Is occupied by palette (OCCUPIER == <name>)
                MLOBSERVE[i + conveyorCount + 2 * conveyorCount * j] = pts.Conveyors[i].IsOccupied(palettes[j].name) ? 1f : 0f;
                // Target position as one hot observation
                MLOBSERVE[i + 2 * conveyorCount + 2 * conveyorCount * j] = i == targetIdx[j] ? 1f : 0f;
            }
        }
    }

    protected override void SetEnabledActions()
    {
        for (int i = 0; i < palettes.Length; i++)
        {
            MLENACT[i][0] = true; // Allow to do nothing
            MLENACT[i][1] = paletteOutputs[i, 1].CC != null
                && paletteOutputs[i,1].CC.IsFree(control.cc); // Left
            MLENACT[i][2] = paletteOutputs[i, 2].CC != null 
                && paletteOutputs[i, 2].CC.IsFree(control.cc); // Right
            MLENACT[i][3] = paletteOutputs[i, 0].CC != null &&
                paletteOutputs[i, 0].CC.OperationModes.ContainsKey("USHIFT"); // Up
            MLENACT[i][4] = paletteOutputs[i, 0].CC != null &&
                paletteOutputs[i, 0].CC.OperationModes.ContainsKey("DSHIFT"); // Down
        }
    }

    protected override void MapDecisionToAction()
    {
        for (int i = 0; i < palettes.Length; i++)
        {
            switch (MLDECIDE[i])
            {
                case 1: control.ExecuteOpMode("Palette" + i, "LEFT"); break;
                case 2: control.ExecuteOpMode("Palette" + i, "RIGHT"); break;
                case 3: control.ExecuteOpMode("Palette" + i, "UP"); break;
                case 4: control.ExecuteOpMode("Palette" + i, "DOWN"); break;
            }
        }
    }

    private float CalculateTargetDistance(int i)
    {
        if (paletteOutputs[i, 0].CC != null)
            return Vector3.Distance(paletteOutputs[i, 0].CC.transform.position, pts.Conveyors[targetIdx[i]].transform.position);
        return float.MaxValue;
    }

    protected override IEnumerator OnSelected()
    {
        // Delete old procedure order outputs
        foreach (var oo in gameObject.GetComponents<OrderOutput>())
            DestroyImmediate(oo);

        palettes = new Palette[pts.PaletteCount];
        paletteOutputs = new OrderOutput[pts.PaletteCount, 3];
        for (int i = 0; i < pts.PaletteCount; i++)
        {
            // Create list of palette control components --> TODO use own order outputs instead
            palettes[i] = pts.Palettes[i].GetComponent<Palette>();

            // Create an Order Output for this procedure
            OrderOutput output = gameObject.AddComponent<OrderOutput>();
            output.Role = "Palette" + i;
            output.State = OrderOutput.OrderOutputState.OK;
            output.CC = palettes[i].GetComponent<ControlComponent>();

            // Find order outputs of palettes control components
            OrderOutput[] outputs = pts.Palettes[i].GetComponents<OrderOutput>();
            paletteOutputs[i, 0] = Array.Find(outputs, o => o.Role.Equals("Current"));
            paletteOutputs[i, 1] = Array.Find(outputs, o => o.Role.Equals("Left"));
            paletteOutputs[i, 2] = Array.Find(outputs, o => o.Role.Equals("Right"));
            if (output.CC == null ||
                paletteOutputs[i, 0] == null || paletteOutputs[i, 1] == null || paletteOutputs[i, 2] == null)
            {
                Debug.LogError("Didn't find all control components or orderoutputs of palettes.");
                control.WORKST = "NoPaletteCCs";
                OnStop();
                yield break;
            }
        }
        control.UpdateOrderOutputs();
        yield return base.OnSelected();
    }

    public override void OnStart()
    {
        // Calculate observation space
        MLOBSERVE = new float[pts.Conveyors.Length * (1 + 2 * palettes.Length)];
        // Calucalte action space
        MLENACT = new bool[palettes.Length][];

        targetIdx = new int[palettes.Length];
        targetDistance = new float[palettes.Length];

        for (int i = 0; i < palettes.Length; i++)
        {
            MLENACT[i] = new bool[5];

            // Set target from Goals parameter
            targetIdx[i] = Mathf.RoundToInt(Goal[i]); //TODO check Goal length
            if (targetIdx[i] < 0 || targetIdx[i] >= pts.Conveyors.Length)
            {
                control.WORKST = "TargetNull";
                control.EXST = ExecutionState.ABORTING;
                return;
            }

            // Try to occupy Palette CC
            if (control.OrderOutputs["Palette" + i].CC != null)
            {
                control.OrderOutputs["Palette" + i].CC.Occupy(control.cc);
                if (!control.OrderOutputs["Palette" + i].CC.IsOccupied(control.cc))
                {
                    control.WORKST = "Palette" + i + "CCNotOccupied";
                    control.EXST = ExecutionState.ABORTING;
                    return;
                }
            }
            else
            {
                control.WORKST = "Palette" + i + "CCNotFound";
                control.EXST = ExecutionState.ABORTING;
                return;
            }

            // Initialize order outputs of palette
            //TODO move UpdateOrderOutputs from MLOpModePalette to Palette class and use instead?
            paletteOutputs[i, 0].CC = palettes[i].GetCurrentConveyor();
            paletteOutputs[i, 1].CC = palettes[i].GetNextConveyor(false);
            paletteOutputs[i, 2].CC = palettes[i].GetNextConveyor(true);

            targetDistance[i] = CalculateTargetDistance(i);

        }

        // End STARTING state and go to EXECUTE
        base.OnStart();
    }

    protected override void OnSuspend()
    {
        // Check individual palettes for their distance to target and apply rewards
        bool targetsReached = true;
        for (int i = 0; i < palettes.Length; i++)
        {
            float newTargetDistance = CalculateTargetDistance(i);

            if (paletteOutputs[i, 0].CC != pts.Conveyors[targetIdx[i]])
            {
                targetsReached = false;
                // Add reward if closer to target
                if ((targetDistance[i] - newTargetDistance) > 0.1f)
                    MLREWARD += RCloserToTarget / palettes.Length;
                // Add critic if farther from target
                else if ((targetDistance[i] - newTargetDistance) < -0.1f)
                    MLREWARD -= 2 * RCloserToTarget / palettes.Length;
            }
            // Update last target distance for next move
            targetDistance[i] = newTargetDistance;
        }

        // Check if targets are reached
        if (targetsReached)
        {
            control.WORKST = "TargetsReached";
            control.EXST = ExecutionState.COMPLETING;
        }
        else
        {
            //TODO calculate sum of targetDistance
            //control.WORKST = string.Format("Target:{0:f2}", targetDistance);

            // End suspending state --> got to SUSPENDED
            base.OnSuspend();
        }
    }

    protected override void OnCompleting()
    {
        foreach (var oo in control.OrderOutputs)
        {
            oo.Value.CC.Free(control.cc);
        }
        base.OnCompleting();
    }
}
