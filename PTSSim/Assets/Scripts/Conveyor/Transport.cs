﻿using System;
using System.Collections;
using UnityEngine;

// TODO make abstract class and inherit Transport and Shift
public class Transport : OperationMode
{
    [Tooltip("Speed of the shift movement.")]
    public float Speed = 1f;
    [Tooltip("Reduction factor that is used to determine the speed in the FTAKE / BTAKE positioning working state.")]
    public float positioningSpeed = 0.2f;
    public bool take, fwd = true;

    // I/Os
    [Tooltip("Belt motor is automatically assigned during Start if null.")]
    public IMotor motor;
    [Tooltip("Light barriers are automatically assigned during Start if null.")]
    public LightBarrier[] lightBarriers;

    public void Start()
    {
        // Get motor
        if (motor == null)
            motor = GetComponent<BeltMotor>();

        // Get light barriers
        if (lightBarriers.Length == 0)
            lightBarriers = GetComponents<LightBarrier>();

        if (lightBarriers.Length < 4)
            Debug.Log("Light Barriers are missing!");
    }

    // Execute operation mode
    // TODO configure belt speed as operation mode parameter
    override protected IEnumerator OnSelected()
    {
        control.WORKST = ControlComponent.WORKST_READY;
        // Wait till STARTING is done
        yield return new WaitUntil(() => control.EXST == ExecutionState.EXECUTE);

        // Set direction and speed
        motor.Direction = fwd ? 1 : -1;
        motor.Speed = Speed;
        yield return new WaitUntil(() => motor.Speed == Speed);
        LightBarrier stop = lightBarriers[fwd ? 0 : 3];

        // Check light barriers and control motorspeed
        if (take)
        {
            LightBarrier slow = lightBarriers[fwd ? 1 : 2];
            control.WORKST = "TakeIn";
            yield return new WaitUntil(() => slow.Hit);

            // 3rd light barrier reached --> Start positioning
            control.WORKST = "Positioning";
            motor.Speed = positioningSpeed;
            yield return new WaitUntil(() => stop.Hit);

            // Last light barrier reached --> Stop motor
        }
        else
        {
            control.WORKST = "PassIn";
            yield return new WaitUntil(() => stop.Hit);

            // Last light barrier hit, wait till palette has passed
            control.WORKST = "PassOut";
            yield return new WaitWhile(() => stop.Hit);

            // Wait till palette has reached end
            //TODO integrate over motor speed to get wait time
            yield return new WaitForSeconds(0.5f);
        }
        control.EXST = ExecutionState.COMPLETING;
        control.WORKST = "Stopping";
        motor.Speed = 0;
        // TODO read measured speed value instead
        //yield return new WaitUntil(() => motor.Speed == 0);
        motor.Direction = 0;
        control.WORKST = "DONE";
        OnCompleting();
    }

    public override void OnAbort()
    {
        motor.Direction = 0;
        base.OnAbort();
    }

    public override void OnClear()
    {
        motor.Speed = 0;
        base.OnClear();
    }

    public override void OnStop()
    {
        motor.Speed = 0;
        base.OnStop();
    }

    float holdSpeed = 0;
    public override void OnHold()
    {
        holdSpeed = motor.Speed;
        motor.Speed = 0;
        base.OnHold();
    }
    public override void OnUnhold()
    {
        motor.Speed = holdSpeed;
        base.OnUnhold();
    }
}