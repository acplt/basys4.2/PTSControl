﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMotor
{
    float Speed { get; set; }
    int Direction { get; set; }
}

public class BeltMotor : MonoBehaviour, IMotor
{
    [Tooltip("Speed setpoint of conveyor belt: [0,1]")]
    public float speedSP = 1f;
    // TODO use ramp or sinoid function --> add speedMV value
    public float Speed { get => speedSP; set => speedSP = value; }
    [Tooltip("Direction of conveyor belt. 0 means of: [-1,0,1]")]
    public int direction = 0;
    public int Direction { get => direction; set => direction = value; }
    private Rigidbody body;

    /* Kinematic version: not tested
    void OnTriggerStay(Collider other)
    {
        if (motor != 0)
        {
            other.transform.position = Vector3.MoveTowards(other.transform.position, other.transform.position + Vector3.right * motor, ConveyorSpeed * Time.deltaTime);
        }
    }
    */

    public void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    public void FixedUpdate()
    {
        // NEVER set position to current position (e.g. speed = 0).
        // Otherwise some strange side effects occur (MovePosition in ShiftMotor didn't move the conveyor)
        if (direction != 0 && speedSP != 0)
        {
            Vector3 currentPosition = body.position;
            // Move up a litle bit with product on belt
            //body.MovePosition(currentPosition + Vector3.up * Time.fixedDeltaTime);
            // Teleport forward
            body.position += transform.right * direction * speedSP * Time.fixedDeltaTime;
            // Transport object back
            body.MovePosition(currentPosition);
        }
    }
}
