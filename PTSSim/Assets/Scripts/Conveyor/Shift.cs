﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shift : OperationMode
{
    [Tooltip("Speed of the shift movement.")]
    public float Speed = 1f;
    [Tooltip("Distance at which the conveyor is considered close enough to change to positioning speed.")]
    public float SlowDistance = 0.2f;
    [Tooltip("Reduction factor that is used to determine the speed after reaching SlowDistance.")]
    public float PositioningSpeed = 0.2f;
    [Tooltip("Direction to shift. (USHIFT / DSHIFT)")]
    public bool Fwd = true;
    // I/Os
    [Tooltip("Shift motor is automatically assigned during Start if null.")]
    public IMotor motor;
    [Tooltip("Shift position sensor is automatically assigned during Start if null.")]
    public ShiftPosition pos;

    public void Start()
    {
        // Get motor
        if (motor == null)
            motor = GetComponentInParent<ShiftMotor>();
        if (pos == null)
            pos = GetComponentInParent<ShiftPosition>();
    }

    private int targetPosition;

    private float Distance(int targetPosition)
    {
        return (pos.Stops[targetPosition] - transform.localPosition.z) * (Fwd ? 1 : -1);
    }

    public override void OnStart()
    {
        // Calulate target position
        targetPosition = pos.Position + (Fwd ? 1 : -1);
        // Crop value to possible range
        targetPosition = (targetPosition < 0) ? 0 : (targetPosition > pos.Stops.Count - 1) ? pos.Stops.Count - 1 : targetPosition;

        // Set motor direction (release brake) 
        motor.Direction = (Fwd ? 1 : -1);
        base.OnStart();
    }

    protected override void OnExecute()
    {
        float distance = Distance(targetPosition);
        // Check for slow position range
        if (distance < SlowDistance)
        {
            control.WORKST = "Positioning: " + Distance(targetPosition);
            // Check if target position reached
            if (distance <= Time.deltaTime)
            {
                motor.Speed = 0;
                control.EXST = ExecutionState.COMPLETING;
            }
            else
            {
                motor.Speed = PositioningSpeed;
            }
        }
        else
        {
            control.WORKST = string.Format("Moving:{0:f2}", Distance(targetPosition));
            motor.Speed = Speed;
        }
        base.OnExecute();
    }

    protected override void OnCompleting()
    {
        control.EXST = ExecutionState.COMPLETING;
        pos.SetPosition(targetPosition);
        control.WORKST = "DONE";
        base.OnCompleting();
    }

    public override void OnAbort()
    {
        motor.Direction = 0;
        base.OnAbort();
    }

    public override void OnClear()
    {
        motor.Speed = 0;
        base.OnClear();
    }

    public override void OnStop()
    {
        motor.Speed = 0;
        base.OnStop();
    }

    float holdSpeed = 0;
    public override void OnHold()
    {
        holdSpeed = motor.Speed;
        motor.Speed = 0;
        base.OnHold();
    }
    public override void OnUnhold()
    {
        motor.Speed = holdSpeed;
        base.OnUnhold();
    }
}
