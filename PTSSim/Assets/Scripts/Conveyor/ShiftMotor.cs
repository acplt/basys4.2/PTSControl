﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftMotor : MonoBehaviour, IMotor
{
    [Tooltip("Speed of shift conveyor: [0,1]")]
    public float Speed = 1f;
    [Tooltip("Direction of shift conveyor. 0 means of: [-1,0,1]")]
    public int Direction = 0;
    private Rigidbody body;

    // IMotor Interface
    float IMotor.Speed { get => Speed; set => Speed = value; }
    int IMotor.Direction { get => Direction; set => Direction = value; }

    // Start is called before the first frame update
    public void Start()
    {
        body = GetComponentInChildren<Rigidbody>();   
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        if(Direction != 0 && Speed != 0)
        {
            body.MovePosition(body.gameObject.transform.position + (Vector3.forward * Time.fixedDeltaTime * Speed * Direction));
        }
    }
}
