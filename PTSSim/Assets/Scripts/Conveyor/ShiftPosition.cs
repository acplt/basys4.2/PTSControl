﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftPosition : MonoBehaviour
{
    [Tooltip("Z value for each possible shift stop position (have to be ordered by size).")]
    // Todo order on set accessor
    public List<float> Stops;
    [SerializeField]
    //[Tooltip("Current position (index of possiblePositions starting from 0. -1 if not close enough to a position).")]
    public int Position { get => Stops.FindIndex(p => Mathf.Abs(conveyor.transform.localPosition.z - p) < Time.deltaTime); }

    private GameObject conveyor;


    public void SetPosition(int value)
    {
        value = Mathf.Max(value, 0);
        value = Mathf.Min(value, Stops.Count);
        conveyor.transform.localPosition = new Vector3(
            conveyor.transform.localPosition.x,
            conveyor.transform.localPosition.y,
            Stops[value]);
    }
    public void SetRandomPosition()
    {
        SetPosition(Random.Range(0, Stops.Count));
    }

    public void Start()
    {
        conveyor = GetComponentInChildren<ControlComponent>().gameObject;
        SetPosition(Position);
    }
}
