﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBarrier : MonoBehaviour
{
    public Vector3 Origin;
    public Vector3 Direction;
    public bool Hit { get; private set; }


    // Update is called once per frame
    void Update()
    {
        //TODO also use transform.rotation to localize Direction!
        Hit = Physics.Raycast(transform.position + Origin, Direction, Direction.magnitude);
    }

    // Draw light barrier only when selected
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.75F);
        Gizmos.DrawLine(transform.position + Origin, transform.position + Origin + Direction);
    }
}
