﻿using System;
using UnityEngine;

/// <summary>
/// Implements the ML operation mode for the PalettePathFinder behaviour.
/// Therefore the actions and observations are mapped.
/// The goal is defined as reaching the <see cref="target"/>.
/// <para/>
/// Additionally the operation mode handles position updates of the palette via
/// <see cref="UpdateOrderOutputs"/>.
/// Checks for collisions with other palettes and the ground.
/// Calculates distance based and terminal rewards.
/// </summary>
/// <remarks>
/// The operation mode is placed inside a group control component attached to a palette game object.
/// The <see cref="target"/> variable could be used as operation mode parameter for the ML operation mode.
/// More parameters will be added in the future: targetRotation, targetTemperature
/// </remarks>
public class MLOpModePalette : MLOperationMode
{
    public override string MLMODEL { get => "PalettePathFinder"; }

    [Header("Application Specific Rewards")]
    // positive:
    public float RCloserToTarget = 0.01f;
    public float RPositionChanged = 0.005f;

    [Header("Application Specific Settings")]
    // Fields for use in unity //TODO save to Tensorflow stats
    // Position and target
    [Tooltip("Target conveyor game object. (is filled via generic Goals field of MLOperationMode)")]
    public GameObject target;
    public ControlComponent currentConveyor
    {
        get => control.OrderOutputs["Current"].CC;
        set
        {
            if(control != null && value != null)
            {
                value.Occupy(control.cc);
                control.OrderOutputs["Current"].CC = value;
            }
        }
    }
    [Tooltip("Triggers decision and actions requests if current conveyor isn't executing.")]
    public bool AllowActionOnCurrentIdle = false;

    // Privat variables
    private float targetDistance;
    private PTS pts;

    #region Unity specific overrides
    /// <summary>
    /// Get the corresponding environment to access <see cref="PTS"/> conveyors and shift elements.
    /// </summary>
    /// <remarks>
    /// If possible these information could be accesible via AssetAdminstrationShells in the "real" environment.
    /// </remarks>
    private void Start()
    {
        pts = transform.parent.GetComponent<PTS>();
    }

    /// <summary>
    /// Watch for collissions with other palettes or the ground, identified by corresponding tags.
    /// </summary>
    public void OnPaletteCollision()
    {
        if(control!=null) control.EXST = ExecutionState.ABORTING;
    }
    #endregion Unity specific overrides


    #region MLOpMode specific overrides of observations and actions
    /// <summary>
    /// Currently the <see cref="ControlComponent.OCCUPIER"/> of all conveyors and <see cref="ShiftPosition.Position"/> values are used.
    /// </summary>
    /// <remarks>
    /// State Space is 2 times conveyor count + count of shift position sensors.
    /// </remarks>
    protected override void CollectObservations()
    {
        int conveyorCount = pts.Conveyors.Length;

        // Occupation states of conveyors
        for (int i = 0; i < conveyorCount; ++i)
        {
            // Is free (OCCUPIER == OCCUPIER_NONE)
            MLOBSERVE[i] = pts.Conveyors[i].IsFree() ? 1f: 0f;
            // Is occupied by palette (OCCUPIER == <name>)
            MLOBSERVE[i + conveyorCount] = pts.Conveyors[i].IsOccupied(name) ? 1f : 0f;
        }

        // Shift positions
        for (int i = 0; i < pts.ShiftPositions.Length; ++i)
        {
            MLOBSERVE[2 * conveyorCount + i] = pts.ShiftPositions[i].Position+1;
        }

        // Target position <-- Get as operation mode parameter
        //sensor.AddOneHotObservation(Array.IndexOf(pts.conveyors, target), pts.conveyors.Length);
    }


    /// <summary>
    /// Only enable left (1) and right (2) and shift (3-8) actions if conveyors
    /// and necessary operation modes are available.
    /// </summary>
    /// <remarks>
    /// TODO Disable operation modes in control components dynamically, if they are not available. E.g. don't allow USHIFT or DSHIFT if shift is at endposition.
    /// TODO In the "real" environment operation modes are (currently) available even if not desireable. Adapt "real" environment or train differentyl or ignore?
    /// </remarks>
    protected override void SetEnabledActions()
    {
        ControlComponent c = control.OrderOutputs["Current"].CC;
        ControlComponent l = control.OrderOutputs["Left"].CC;
        ControlComponent r = control.OrderOutputs["Right"].CC;

        // Allow to do nothing //TODO create variable for unity editor, throw error if no action is available?
        MLENACT[0][0] = true;
        // Left
        MLENACT[0][1] = c.OperationModes.ContainsKey("BPASS")
            && l != null && l.OperationModes.ContainsKey("BTAKE")
            && l.IsFree(control.cc);
        //Right
        MLENACT[0][2] = c.OperationModes.ContainsKey("FPASS")
            && r != null && r.OperationModes.ContainsKey("FTAKE")
            && r.IsFree(control.cc);
        //Shift
        MLENACT[0][3] = c.OperationModes.ContainsKey("USHIFT");
        MLENACT[0][4] = c.OperationModes.ContainsKey("DSHIFT");
        MLENACT[0][5] = l != null && l.OperationModes.ContainsKey("USHIFT");
        MLENACT[0][6] = l != null && l.OperationModes.ContainsKey("DSHIFT");
        MLENACT[0][7] = r != null && r.OperationModes.ContainsKey("USHIFT");
        MLENACT[0][8] = r != null && r.OperationModes.ContainsKey("DSHIFT");
    }

    /// <summary>
    /// Current action mapping:
    /// <list type="bullet">
    /// <item>0: Do nothing</item>
    /// <item>1: Move left       - Current = BPASS, Left = BTAKE</item>
    /// <item>2: Move right      - Current = FPASS, Right = FTAKE</item>
    /// <item>3-4: Shift Current - Current = USHIFT, DSHIFT</item>
    /// <item>5-6: Shift Left    - Left = USHIFT, DSHIFT</item>
    /// <item>7-8: Shift Right   - Right = USHIFT, DSHIFT</item>
    /// </list>
    /// </summary>
    /// <remarks>
    /// TODO Select NONE or STOP in case of null or 0 --> selectable from Unity editor
    /// TODO Implement other mappings? --> selectable from Unity editor
    /// TODO Read mapping from a configuration file?
    /// </remarks>
    protected override void MapDecisionToAction()
    {
        /*
         * Option 1a: Generic mapping:
         * branch = ElementAt of order ouput
         * branch's action value = ElementAt of OrderOutput[branch].cc.OperationModes 
         */
        //base.MapDecisionToAction();

        /*
         * Option 1b: Generic mapping, but ordered, so heuristic functions properly
         * - branch 0: Current
         *    0-6: Current = NONE, FPASS, FTAKE, BPASS, BTAKE, USHIFT, DSHIFT
         * - branch 1:
         *    0-2: Left = NONE, FPASS, FTAKE, BPASS, BTAKE, USHIFT, DSHIFT
         * - branch 2:
         *    0-2: Right = NONE, FPASS, FTAKE, BPASS, BTAKE, USHIFT, DSHIFT
         */
        //TODO implement?

        /* 
         * Option 2: Simplified actions for moving the palett and parallel shifting (branches)
         * TODO Needs 2 aditional order outputs: Shift1, Shift2
         * --> Here "Skills" of the control component could be used if implemented.
         * - branch 0:
         *    0: Current = NONE, Left = NONE, RIGHT = NONE;
         *    1: Current = BPASS, Left = BTAKE
         *    2: Current = FPASS, Right = FTAKE
         * - branch 1:
         *    0-2: Shift1 = NONE, USHIFT, DSHIFT
         * - branch 2:
         *    0-2: Shift2 = NONE, USHIFT, DSHIFT
         *
        if (MLDECIDE[0] == 1)
        {
            control.ExecuteOpMode("Current", "BPASS");
            control.ExecuteOpMode("Left", "BTAKE");
        }
        else if(MLDECIDE[0] == 2)
        {
            control.ExecuteOpMode("Current", "FPASS");
            control.ExecuteOpMode("Right", "FTAKE");
        }
        if(MLDECIDE.Length > 1)
        {
            if (MLDECIDE[1] == 1)
                control.ExecuteOpMode("Shift1", "USHIFT");
            else if (MLDECIDE[1] == 2)
                control.ExecuteOpMode("Shift1", "DSHIFT");
        }
        if(MLDECIDE.Length > 2)
        {
            if (MLDECIDE[2] == 1)
                control.ExecuteOpMode("Shift2", "USHIFT");
            else if (MLDECIDE[2] == 2)
                control.ExecuteOpMode("Shift2", "DSHIFT");
        }

        /* Option 3: Flat action space and simplified actions
         * - branch 0:
         *    0: Do nothing
         *    1: Move left       - Current = BPASS, Left = BTAKE
         *    2: Move right      - Current = FPASS, Right = FTAKE
         *    3-4: Shift Current - Current = USHIFT, DSHIFT
         *    5-6: Shift Left    - Left = USHIFT, DSHIFT
         *    7-8: Shift Right   - Right = USHIFT, DSHIFT
         */
        switch (MLDECIDE[0])
        {
            case 1:
                // Left
                control.ExecuteOpMode("Current", "BPASS");
                control.ExecuteOpMode("Left", "BTAKE");
                break;
            case 2:
                // Right
                control.ExecuteOpMode("Current", "FPASS");
                control.ExecuteOpMode("Right", "FTAKE");
                break;
            case 3: control.ExecuteOpMode("Current", "USHIFT"); break;
            case 4: control.ExecuteOpMode("Current", "DSHIFT"); break;
            case 5: control.ExecuteOpMode("Left", "USHIFT"); break;
            case 6: control.ExecuteOpMode("Left", "DSHIFT"); break;
            case 7: control.ExecuteOpMode("Right", "USHIFT"); break;
            case 8: control.ExecuteOpMode("Right", "DSHIFT"); break;
        }

    }
    #endregion MLOpMode specific overrides of observations and actions

    #region Change palette position
    /// <summary>
    /// Moves the palettets "logical" position.
    /// Maps the Current, Left, Right order outputs of the corresponding control component
    /// to the control components that are found via <see cref="GetConveyorUnderPosition(Vector3)"/>.
    /// Stops and frees control components that are out of reach.
    /// Can add a reward if <see cref="RPositionChanged"/> is set.
    /// </summary>
    /// <remarks>
    /// Is executed by topology app from outside in "real" environment in
    /// combination with an update at the end of corresponding operation mode:
    /// - Current is set by operation mode, e.g. copying Left value to Current after
    /// completing FPASS.
    /// - Left and Right are set periodically by topology app reading the Current
    /// value and evaluating a simple topology model.
    /// </remarks>
    /// TODO --> Add topology component, that acts like in real environment?
    private void UpdateOrderOutputs()
    {
        // Get new control components based on current physical postion
        Palette palette = GetComponent<Palette>();
        ControlComponent[] newOutputs =
            { palette.GetCurrentConveyor(), palette.GetNextConveyor(false), palette.GetNextConveyor(true)};

        // Check if current position changed
        if (newOutputs[0] != control.OrderOutputs["Current"].CC)
            MLREWARD += RPositionChanged;

        // Check if old conveyor isn't available any more
        foreach (var entry in control.OrderOutputs)
        {
            ControlComponent cc = entry.Value.CC;
            if (cc != null && !Array.Exists(newOutputs, value => value == cc))
            {
                // Stop and free old output if necessary //TODO Reset? Reward?
                if (cc.IsOccupied(control.cc))
                {
                    cc.Stop(control.cc.name);
                    cc.Free(control.cc.name);
                }
            }
        }
        // Set new order outputs
        currentConveyor = newOutputs[0];
        control.OrderOutputs["Left"].CC = newOutputs[1];
        control.OrderOutputs["Right"].CC = newOutputs[2];
    }

    /// <summary>
    /// Calculates the physical distance between the two transform objects of the
    /// <see cref="currentConveyor"/> and <see cref="target"/>.
    /// </summary>
    /// <returns>Distance in Unitys arbitrary units.</returns>
    private float CalculateTargetDistance()
    {
        if (currentConveyor != null && target != null)
            return Vector3.Distance(currentConveyor.transform.position, target.transform.position);
        return float.MaxValue;
    }
    #endregion Change palette position

    #region Operation mode execution cycle
    /// <summary>
    /// Allows to go to SUSPENDING before actions are finished if <see cref="AllowActionOnCurrentIdle"/>
    /// is true and Current order output isn't in use (EXECUTING), e.g. move right, while shifting.
    /// <inheritdoc/>
    /// </summary>
    /// <remarks>
    /// Not tested yet.
    /// </remarks>
    protected override void OnExecute()
    {
        if (AllowActionOnCurrentIdle && currentConveyor.EXST != ExecutionState.EXECUTE)
        {
            control.EXST = ExecutionState.SUSPENDING;
        }
    }

    /// <summary>
    /// Initializes the action and observation space.
    /// Initializes the order output mapping and target distance.
    /// </summary>
    public override void OnStart()
    {
        // Calculate observation space
        MLOBSERVE = new float[pts.Conveyors.Length * 2 + pts.ShiftPositions.Length];
        // Calucalte action space
        MLENACT = new bool[1][];
        MLENACT[0] = new bool[9];
        //TODO also fill MLDECIDE?
        //TODO get from or compare with MLMODEL (from MLAgent. ...) --> Add new "Configuration" variable to interface?

        //Set target from Goals parameter
        if (Goal.Length > 0 && Mathf.RoundToInt(Goal[0]) >= 0 && Mathf.RoundToInt(Goal[0]) < pts.Conveyors.Length)
            target = pts.Conveyors[Mathf.RoundToInt(Goal[0])].gameObject;
        else if (target == null)
        {
            control.WORKST = "TargetNull";
            control.EXST = ExecutionState.ABORTING;
            return;
        }

        // Initialize order outputs and distance
        UpdateOrderOutputs();
        targetDistance = CalculateTargetDistance();

        // End STARTING state and go to EXECUTE
        base.OnStart();
    }

    /// <summary>
    /// Is used to update order outputs of the palette and to check if goal is closer, reached or missed.
    /// </summary>
    /// <remarks>
    /// TODO maybe its better to move this to new dedicated functions in <see cref="MLOperationMode"/>
    /// or in <see cref="IMLOperationMode"/>, e.g. <c>CheckGoal()</c>
    /// </remarks>
    protected override void OnSuspend()
    {
        // Let topology app update outputs
        UpdateOrderOutputs();

        // Check if not on conveyor any more
        if (currentConveyor == null)
        {
            control.WORKST = "ConveyorNull";
            control.EXST = ExecutionState.ABORTING;
        }
        // Check if target is reached
        else if (currentConveyor.gameObject == target)
        {
            control.WORKST = "TargetReached";
            control.EXST = ExecutionState.COMPLETING;
        }
        else
        {
            // Add reward if closer to target
            float newTargetDistance = CalculateTargetDistance();
            if ((targetDistance - newTargetDistance) > 0.1f)
                MLREWARD += RCloserToTarget;
            // Add critic if farther from target
            else if ((targetDistance - newTargetDistance) < -0.1f)
                MLREWARD -=  2*RCloserToTarget;
            // Update last target distance for next move
            targetDistance = newTargetDistance;
            control.WORKST = string.Format("Target:{0:f2}", targetDistance);

            // End suspending state --> got to SUSPENDED
            StartCoroutine("CompleteSuspension");
        }
    }

    private System.Collections.IEnumerator CompleteSuspension()
    {
        while (true)
        {
            // Check if operation mode is still active and on an conveyor
            if (control == null || control.EXST != ExecutionState.SUSPENDING
                    || control.OrderOutputs["Current"].CC == null)
            {
                yield break;
            }
            if(control.OrderOutputs["Current"].CC.IsOccupied(control.cc))
            {
                base.OnSuspend();
                break;
            }
            // Try to occupy
            control.OrderOutputs["Current"].CC.Occupy(control.cc);
            yield return null;
        }
    }
    #endregion Operation mode execution cycle
}
