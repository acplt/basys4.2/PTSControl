﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


/// <summary>
/// Class for all properties and methods of the physical palette.
/// </summary>
public class Palette : MonoBehaviour
{
    public UnityEvent Collision;

    /// <summary>
    /// Moves the palettes "physical" position.
    /// Teleports the rigidbody 0.3 units above the given <paramref name="conveyor"/>.
    /// Resets velocities, rotation and position to center the palette relative to the conveyor.
    /// </summary>
    /// <remarks>
    /// The "logical" position, stated by the mapped order outputs (Current, Left, Right)
    /// needs to be adapted at the earliest next frame, which should be handled by the
    /// regular execution cycle (e.g. in STARTING).
    /// </remarks>
    /// <param name="conveyor">The control component associated with the target conveyor.</param>
    public void SetPosition(ControlComponent conveyor)
    {
        Rigidbody body = GetComponent<Rigidbody>();
        body.velocity = Vector3.zero;
        body.angularVelocity = Vector3.zero;
        body.rotation = Quaternion.identity;
        body.position = conveyor.transform.position + (Vector3.up * 0.3f);
    }

    #region Positioning helper functions aka "topology app"
    /// <summary>
    /// Checks for a conveyor of beneath this game object (probably the palette)
    /// <seealso cref="GetConveyorUnderPosition(Vector3)"/>
    /// </summary>
    public ControlComponent GetCurrentConveyor()
    {
        return GetConveyorUnderPosition(transform.position);
    }
    /// <summary>
    /// Check for one conveyor length (2 units) in direction of <paramref name="directionRight"/>.
    /// <seealso cref="GetConveyorUnderPosition(Vector3)"/>
    /// </summary>
    /// <param name="directionRight">False = check to <c>Vector3.left</c>. True = check to <c>Vector3.right</c>.</param>
    public ControlComponent GetNextConveyor(bool directionRight)
    {
        return GetConveyorUnderPosition(transform.position + (2f * (directionRight ? Vector3.left : Vector3.right)));
    }

    /// <summary>
    /// Uses a raycast with length 1 to find a conveyors control component beneath the given postion.
    /// </summary>
    /// <param name="position">Position to search beneath.</param>
    /// <returns>The control component in the game object with tag "Conveyor" or "Shift". Null otherwise.</returns>
    public ControlComponent GetConveyorUnderPosition(Vector3 position)
    {
        //Todo use layer mask to be more efficient?
        if (Physics.Raycast(position, Vector3.down, out RaycastHit hit, 1f))
        {
            if (hit.collider.CompareTag("Conveyor"))
                return hit.collider.GetComponent<ControlComponent>();
            else if (hit.collider.CompareTag("Shift"))
                return hit.collider.GetComponentInChildren<ControlComponent>();
        }
        return null;
    }
    #endregion Positioning helper functions aka "topology app"

    /// <summary>
    /// Watch for collissions with other palettes or the ground, identified by corresponding tags.
    /// </summary>
    /// <param name="collision">The collision object to check for.</param>
    private void OnCollisionEnter(Collision collision)
    {
        // Detect crashes with other palettes
        // TODO add tag for palettes (and ground)
        if (collision.gameObject.CompareTag("Palette") || collision.gameObject.CompareTag("Ground"))
        {
            Collision.Invoke();
        }
    }
}
