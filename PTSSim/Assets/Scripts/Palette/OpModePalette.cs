﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Combined implementation of regular operation modes of palette control component.
/// Type can be selected via <see cref="opModeType"/>RIGHT, LEFT, UP, DOWN
/// </summary>
public class OpModePalette : OperationMode
{
    public enum OpModeType { LEFT, RIGHT, UP, DOWN };
    public OpModeType opModeType;
    public int OccupyTimeout = -1;

    protected override IEnumerator OnSelected()
    {
        UpdateOrderOutputs();

        // Make sure, that current conveyor is available
        if (control.OrderOutputs["Current"].CC == null)
        {
            control.WORKST = "CurrentNull";
            yield break;
        }

        // Make sure, that left or right conveyor is available
        if((opModeType == OpModeType.LEFT  && control.OrderOutputs["Left"].CC  == null) ||
           (opModeType == OpModeType.RIGHT && control.OrderOutputs["Right"].CC == null))
        {
            control.WORKST = "NextConveyorNull";
            OnStop();
            yield break;
        }

        yield return base.OnSelected();
    }

    public override void OnStart()
    {
        StartCoroutine(OccupyAndSelectOpMode());
    }

    private IEnumerator OccupyAndSelectOpMode()
    {
        int counter = 0;
        // Occupy
        while (true)
        {
            // Check if operation mode is still active
            if (control == null || control.EXST != ExecutionState.STARTING)
            {
                yield break;
            }

            // Check if current and next conveyor are occupied
            if (control.OrderOutputs["Current"].CC.IsOccupied(control.cc))
            {
                if (opModeType == OpModeType.LEFT)
                {
                    if (control.OrderOutputs["Left"].CC.IsOccupied(control.cc))
                        break;
                    else
                        control.OrderOutputs["Left"].CC.Occupy(control.cc);
                }
                else if (opModeType == OpModeType.RIGHT)
                {
                    if (control.OrderOutputs["Right"].CC.IsOccupied(control.cc))
                        break;
                    else
                        control.OrderOutputs["Right"].CC.Occupy(control.cc);
                }
                else
                    break;
            }
            else
            {
                control.OrderOutputs["Current"].CC.Occupy(control.cc);
            }
            if(OccupyTimeout >= 0 && ++counter > OccupyTimeout)
            {
                control.WORKST = "OccupyTimeout";
                OnStop();
            }
            yield return null;
        }

        //Select OpMode and send start (move shift conveyors if necessary)
        switch (opModeType)
        {
            case OpModeType.LEFT:
                {
                    if(!NextShiftPosOk(control.OrderOutputs["Left"].CC))
                        yield return MoveShift(control.OrderOutputs["Left"]);
                    control.ExecuteOpMode("Left", "BTAKE");
                    control.ExecuteOpMode("Current", "BPASS");
                    control.WORKST = "Move Left";
                    break;
                }
            case OpModeType.RIGHT:
                if (!NextShiftPosOk(control.OrderOutputs["Right"].CC))
                    yield return MoveShift(control.OrderOutputs["Right"]);
                control.ExecuteOpMode("Right", "FTAKE");
                control.ExecuteOpMode("Current", "FPASS");
                control.WORKST = "Move Right";
                break;
            case OpModeType.UP:
                control.ExecuteOpMode("Current", "USHIFT");
                control.WORKST = "Up Shift";
                break;
            case OpModeType.DOWN:
                control.ExecuteOpMode("Current", "DSHIFT");
                control.WORKST = "Down Shift";
                break;
        }
        base.OnStart();
    }

    private bool NextShiftPosOk(ControlComponent next)
    {
        return Math.Abs(next.transform.position.z - control.OrderOutputs["Current"].CC.transform.position.z) < 0.01f;
    }

    private IEnumerator MoveShift(OrderOutput next)
    {
        control.ExecuteOpMode(next.Role,
            (next.CC.transform.position.z - transform.position.z) > 0 ? "USHIFT" : "DSHIFT");
        while (true)
        {
            // Check if operation mode is still active
            if (control == null || control.EXST != ExecutionState.STARTING)
            {
                next.CC.Stop(control.cc.name);
                next.CC.Free(control.cc);
                yield break;
            }

            if (next.CC.EXST == ExecutionState.COMPLETED ||
                    next.CC.EXST == ExecutionState.STOPPED ||
                    next.CC.EXST == ExecutionState.IDLE)
            {
                // Check if next conveyor is at correct position
                if (NextShiftPosOk(next.CC))
                {
                    control.StopExecuteOpMode(next.Role);
                    break;
                }else if(next.Coroutine == null || next.State != OrderOutput.OrderOutputState.OK)
                {
                    control.ExecuteOpMode(next.Role,
                        (next.CC.transform.position.z - transform.position.z) > 0 ? "USHIFT" : "DSHIFT");
                }
            }
            yield return null;
        }
    }

    protected override void OnExecute()
    {
        // Check if completed
        if(control.OrderOutputs["Current"].CC.EXST == ExecutionState.COMPLETED)
        {
            if (opModeType == OpModeType.LEFT)
            {
                if (control.OrderOutputs["Left"].CC.EXST == ExecutionState.COMPLETED)
                    OnCompleting();
            }
            else if (opModeType == OpModeType.RIGHT)
            {
                if (control.OrderOutputs["Right"].CC.EXST == ExecutionState.COMPLETED)
                    OnCompleting();
            }
            else
                OnCompleting();
        }
        base.OnExecute();
    }

    protected override void OnCompleting()
    {
        if(opModeType == OpModeType.LEFT || opModeType == OpModeType.RIGHT)
        {
            control.OrderOutputs["Current"].CC.Reset(control.cc.name);
            control.OrderOutputs["Current"].CC.Free(control.cc);
        }
        UpdateOrderOutputs();
        base.OnCompleting();
    }

    private void UpdateOrderOutputs()
    {
        // Get new control components based on current physical postion
        Palette palette = GetComponent<Palette>();
        control.OrderOutputs["Current"].CC = palette.GetCurrentConveyor();
        control.OrderOutputs["Left"].CC = palette.GetNextConveyor(false);
        control.OrderOutputs["Right"].CC = palette.GetNextConveyor(true);
    }
}
