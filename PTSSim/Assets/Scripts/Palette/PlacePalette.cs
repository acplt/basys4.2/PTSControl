﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Simple behaviour for the test scene. Places a rigidbody (palette) on conveyor
/// 0 to 2 via alpha keys 1 to 3 that were found in children of the parent game object.
/// </summary>
/// <remarks>
/// The position update is applied via setting the <c>Rigidbody.position</c> attribute
/// in different update cycles (Update, FixedUpdate, LateUpdate) to test different behaviours.
/// </remarks>
public class PlacePalette : MonoBehaviour
{
    private ControlComponent[] conveyors;
    private Rigidbody body;

    void Start()
    {
        conveyors = transform.parent.GetComponentsInChildren<ControlComponent>();
        body = GetComponent<Rigidbody>();

    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha2))
        {
            body.position = conveyors[1].transform.position + (Vector3.up * 0.3f);
        }
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            body.position = conveyors[0].transform.position + (Vector3.up * 0.3f);
        }

    }

    private void LateUpdate()
    {
        if (Input.GetKey(KeyCode.Alpha3))
        {
            body.position = conveyors[2].transform.position + (Vector3.up * 0.3f);
        }
    }
}
