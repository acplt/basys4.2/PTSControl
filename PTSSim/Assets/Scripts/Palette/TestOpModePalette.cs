﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestOpModePalette : MonoBehaviour
{
    private ControlComponent cc;
    public string SenderID = "Operator";
    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<ControlComponent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            cc.Occupy(SenderID);
            cc.Stop(SenderID);
            cc.Reset(SenderID);
            cc.SelectOperationMode(SenderID, "LEFT");
            cc.Start(SenderID);
        }else if (Input.GetKey(KeyCode.RightArrow))
        {
            cc.Occupy(SenderID);
            cc.Stop(SenderID);
            cc.Reset(SenderID);
            cc.SelectOperationMode(SenderID, "RIGHT");
            cc.Start(SenderID);
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            cc.Occupy(SenderID);
            cc.Stop(SenderID);
            cc.Reset(SenderID);
            cc.SelectOperationMode(SenderID, "UP");
            cc.Start(SenderID);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            cc.Occupy(SenderID);
            cc.Stop(SenderID);
            cc.Reset(SenderID);
            cc.SelectOperationMode(SenderID, "DOWN");
            cc.Start(SenderID);
        }
    }
}
