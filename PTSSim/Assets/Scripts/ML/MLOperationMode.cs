﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

/// <summary>
/// This is a abstract <see cref="OperationMode"/> for ML.
/// It dispatches between the <see cref="ControlComponent"/> via <see cref="OperationMode.control"/>
/// and the <see cref="MLModel"/> via <see cref="IMLOperationMode"/>.
/// </summary>
/// <remarks>
/// To implement a specific MLOperationMode one should at least override the
/// <see cref="MapDecisionToAction"/> and the <see cref="CollectObservations"/> function.
/// Additionally terminal conditions have to be checked and reported by setting
/// <c>cc.EXST = ExecutionState.COMPLETING;</c>
/// in case of success or by setting
/// <c>cc.EXST = ExecutionState.ABORTING;</c>
/// in case of failure.
/// This can be done by overriding <see cref="OperationMode.OnSuspend"/>.
/// <para/>
/// Generic rewards can be configured via parameters starting with "R".
/// Additional task (=operation mode) specific rewards can be applied by setting
/// <c>MLREWARD += value;</c>
/// </remarks>
public abstract class MLOperationMode : OperationMode, IMLOperationMode
{
    // TODO: Maybe its usefull to have another action value for that(0 =Don't change, -1 = stop, 1 = operation mode1 , ...)
    [Header("Action Settings")]
    [Tooltip("Stop the execution of the order output operation mode on action value of 0(=BSTATE).")]
    public bool StopExecutionOnAction0 = false;
    [Tooltip("Allow action every step count number of steps, even if some order output is executing." +
        " Allows agent to change operation mode or stop operation mode if StopExecutionOnAction0 is true." +
        " 0 means actions are allowed in every step." +
        " -1 or negative value means don't allow new action, before all outputs are idle.")]
    public int ActionTimeout = -1;
    [Tooltip("Maximum number of steps to wait, till action should have started after selection (during unsuspending)." +
        " Negative values may lead to locks in UNSUSPENDING, e.g. an unknown operation mode was selected.")]
    public int ActionStartTimeout = -1;

    [Header("Generic Rewards")]
    // positive:
    public float RTaskCompleted = 1f; // Ends Episode
    public float ROrderCompleted = 0.001f;
    // negative:
    public float RActionTimeout = -0.001f;
    public float ROrderRejected = -0.001f;
    public float ROutputOccupied = -0.001f;
    public float RTaskFailed = -1f; // Ends Episode

    /* IMLOperationMode Interface, that is used from the agent */
    public ExecutionState MLSC { get; set; }
    public abstract string MLMODEL { get; }
    public float[] MLOBSERVE { get; protected set; }
    public bool[][] MLENACT { get; protected set; }
    public float[] MLDECIDE { get; set; }
    public float MLREWARD { get;
        protected set; } = 0;
    public string MLSTATS { get; set; }

    [Header("Goal")]
    [Tooltip("Generic parameter to set goals for the agent as operation mode parameters")]
    public float[] Goal;
 
    /// <summary>
    /// Indicates the MLModel to start its execution via <see cref="MLSC"/>.
    /// </summary>
    public override void OnStart()
    {
        // For real application: OCCUPY, (set all to SIMULATE execution mode, depending on EXMODE)
        MLSC = ExecutionState.STARTING;
        // Check connection to MLModel and start new episode --> Handle in OnSelected cycle, so don't call base.OnStart();
        //base.OnStart();
    }

    /// <summary>
    /// Generic ML operation mode execution cycle.
    /// Handles the communication with the <see cref="MLModel"/>
    /// via the <see cref="IMLOperationMode"/> interface
    /// identified by <see cref="MLMODEL"/> string value.
    /// </summary>
    /// <remarks>
    /// At first a communication to the MLModel is build up during selection phase and STARTING state.
    /// Basicly the SUSPENDING, SUSPENDED, UNSUSPENDING, EXECUTE state
    /// cycle is used to observe, let MLModel decide, carry out actions and add rewards.
    /// The HOLDING, HELD, UNHOLDING cycle is ignored and may be overriden.
    /// The MLModel is notified about an episode end,if the execution is compledted, aborted or stopped.
    /// Necessary OnSTATEXY functions, e.g. <see cref="OperationMode.OnSuspended()"/>
    /// and virtual functions for observations and actions are called.
    /// <para/>
    /// In this generic operation mode execution some generic (task agnostic) rewards are applied.
    /// See properties starting with "R...", e.g. <see cref="RTaskCompleted"/>.
    /// TODO Check MLSC for requested changes from the MLModel (e.g. abort , stop)
    /// </remarks>
    /// <returns>Is executed as Unity Co-Routine, hence it returns an iterator.</returns>
    protected override IEnumerator OnSelected()
    {
        // Check that control component is a group control component and has order outputs.
        if(control.OrderOutputs.Count == 0)
        {
            control.WORKST = "OrderOutputError"; //TODO use ER state of CC instead
            yield break;
        }
        control.WORKST = ControlComponent.WORKST_READY;

        // Wait for start command
        yield return new WaitUntil(() => control.EXST == ExecutionState.STARTING);

        // Wait for agent to complete OnEpisodeBegin() and correlated actions
        yield return new WaitUntil(() => MLSC == ExecutionState.EXECUTE);
        control.EXST = ExecutionState.EXECUTE;

        uint actionStepCount = 0;
        uint actionStartCount = 0;
        while (true)
        {
            yield return null;
            // Execute indicates the execution of order outputs
            if (control.EXST == ExecutionState.EXECUTE)
            {
                // Go to SUSPENDING, if 
                // all occupied order outputs are completed or stopped
                if (control.OrderOutputs.All(entry =>
                    entry.Value.CC == null
                    || !entry.Value.CC.IsOccupied(control.cc)
                    || entry.Value.CC.EXST == ExecutionState.COMPLETED
                    || entry.Value.CC.EXST == ExecutionState.STOPPED
                    || entry.Value.CC.EXST == ExecutionState.IDLE))
                {
                        control.EXST = ExecutionState.SUSPENDING;
                }
                // Check if ActionTimeout is reached
                else if (ActionTimeout > 0 && actionStepCount++ > ActionTimeout)
                {
                    MLREWARD += RActionTimeout;
                    control.EXST = ExecutionState.SUSPENDING;
                }
                OnExecute();
            }
            // Suspending is used to update order outputs and check for goal reached
            else if (control.EXST == ExecutionState.SUSPENDING)
            {
                foreach (var output in control.OrderOutputs)
                {
                    // Check order outputs for errors
                    // TODO check correct reset of OrderOutputs to OrderOutputError.OK
                    switch (output.Value.State)
                    {
                        case OrderOutput.OrderOutputState.NotAccepted:
                        case OrderOutput.OrderOutputState.NotExecuting:
                        case OrderOutput.OrderOutputState.NotExisting:
                        case OrderOutput.OrderOutputState.NullRequested:
                            MLREWARD += ROrderRejected;
                            break;
                        case OrderOutput.OrderOutputState.Occupied:
                            MLREWARD += ROutputOccupied;
                            break;
                        case OrderOutput.OrderOutputState.Stopped:
                        case OrderOutput.OrderOutputState.Completed:
                            MLREWARD += ROrderCompleted;
                            break;
                    }

                    // Reset and free stopped or completed occupied outputs
                    ControlComponent cc = output.Value.CC;
                    if (cc != null && cc.IsOccupied(control.cc) &&
                        (cc.EXST == ExecutionState.COMPLETED
                        || cc.EXST == ExecutionState.STOPPED
                        || cc.EXST == ExecutionState.IDLE))
                    {
                        if (cc.EXST != ExecutionState.IDLE)
                            cc.Reset(control.cc.name);
                        // TODO maybe its better to free only in UNSUSPENDING or add a call for FreeOutputs() and overwrite it application specific
                        cc.Free(control.cc.name);
                    }
                }
                OnSuspend();
                // Reset the MLSC value, to correctly identify changes in the next phase (SUSPENDED)
                MLSC = ExecutionState.SUSPENDED;

                // Get observations and mask out currently available options
                CollectObservations();
                SetEnabledActions();
                //TODO Call a new virtual function to check for goal reached?
            }
            // Suspended signals that the palette is waiting for the brain or heuristic to select an action
            else if (control.EXST == ExecutionState.SUSPENDED)
            {
                // Wait for agents decision
                if(MLSC == ExecutionState.UNSUSPENDING && MLDECIDE != null)
                {
                    // Execute decisions as actions
                    MapDecisionToAction();
                    // Reset reward for next observation, action, reward cycle
                    MLREWARD = 0;
                    OnSuspended();
                }
            }
            // Unsuspending waits till an order output starts
            else if (control.EXST == ExecutionState.UNSUSPENDING)
            {
                // Wait till any orderOutput is started
                foreach(var output in control.OrderOutputs)
                {
                    if(output.Value.CC != null
                        && output.Value.CC.IsOccupied(control.cc)
                        && output.Value.CC.EXST == ExecutionState.EXECUTE)
                    {
                        control.EXST = ExecutionState.EXECUTE;
                        actionStepCount = 0;
                        actionStartCount = 0;
                        OnUnsuspend();
                        break;
                    }
                }
                // Alternatively check if action start timeout is reached.
                if (ActionStartTimeout > 0 && actionStartCount++ > ActionStartTimeout)
                {
                    MLREWARD += RActionTimeout;
                    control.EXST = ExecutionState.EXECUTE;
                    actionStepCount = 0;
                    actionStartCount = 0;
                    OnUnsuspend();
                }
            }
            // Allow HOLD cycle
            else if (control.EXST == ExecutionState.HOLDING
                || control.EXST == ExecutionState.HELD
                || control.EXST == ExecutionState.UNHOLDING)
            {
            }
            else
            {
                break;
            }
        }
        // Main execution is ended, check for internal ending reason and switch through waiting states
        // TODO check if this can be handled in OperationMode.OnSelected generically
        // Ask for last stats
        MLSTATS = null;
        if (control.EXST == ExecutionState.COMPLETING)
        {
            // Indicate, that completing can be done
            MLSC = ExecutionState.COMPLETING;
            MLREWARD = RTaskCompleted;
            // Wait for EndEpisode and write back MLSTATS
            yield return new WaitUntil(() => MLSC == ExecutionState.COMPLETED);
            OnCompleting();
        }
        else if (control.EXST == ExecutionState.STOPPING)
        {
            OnStop();
        }
        else if (control.EXST == ExecutionState.ABORTING)
        {
            OnAbort();
        }
    }

    /// <summary>
    /// Apply <see cref="RTaskFailed"/> reward on external or internal stop.
    /// </summary>
    public override void OnStop()
    {
        //TODO distinguish between STOP and ABORT? (e.g. don't apply RTaskFailed on STOP)
        MLREWARD = RTaskFailed;
        base.OnStop();
    }

    /// <summary>
    /// Apply <see cref="RTaskFailed"/> reward on external or internal abort.
    /// </summary>
    public override void OnAbort()
    {
        MLREWARD = RTaskFailed;
        base.OnAbort();
    }

    /// <summary>
    /// Reset reward to 0.
    /// </summary>
    public override void OnReset()
    {
        MLREWARD = 0;
        base.OnReset();
    }

    #region Observation and action functions
    /// <summary>
    /// Triggers the collection of observations to update <see cref="MLOBSERVE"/> variable.
    /// Resets all observations if not overwritten via <see cref="CollectObservations"/> or <see cref="OperationMode.OnSuspend"/>.
    /// </summary>
    /// <remarks>
    /// <c>MLOBSERVE = null</c> tells the <see cref="MLModel"/> that no observations where made.
    /// TODO Discuss whether more (generic) information (of OrderOutputs) should be added, e.g. OPMODE, EXST.
    /// </remarks>
    protected virtual void CollectObservations()
    {
        MLOBSERVE = null;
    }

    /// <summary>
    /// Initiates the requested decision as new actions.
    /// This function is called if the decisions in <see cref="MLDECIDE"/> should be carried 
    /// out as actions. It corresponds to <see cref="Unity.MLAgents.Agent.OnActionReceived(float[])"/>
    /// and can be overriden in ML operation modes to use application specific
    /// decision (action) spaces.
    /// <para/>
    /// If it is not overriden a group control component is assumed.
    /// Then every action branch is mapped to an order output and 
    /// each action value to an operation mode of the corresponding control
    /// component that is currently mapped to the order output.
    /// </summary>
    /// <remarks>
    /// Group control components may
    /// <list type="bullet">
    /// <item>use the ExecuteOpMode macro </item>
    /// <item>directly call orders on the order outputs</item>
    /// <item>use control component specific skills(NotImplemented)</item>
    /// <item>use other operation modes of this control component</item>
    /// </list>
    /// Single control components may use their field devices directly, e.g. read 
    /// sensors or change set points.
    /// </remarks>
    protected virtual void MapDecisionToAction()
    {
        for (int branch = 0; branch < MLDECIDE.Length; branch++)
        {
            if (branch >= 0 && branch < control.OrderOutputs.Count)
            {
                int decision = (int)Math.Round(MLDECIDE[branch]);
                ControlComponent cc = control.OrderOutputs.ElementAt(branch).Value.CC;
                if (cc != null && decision >= 0 && decision < cc.OperationModes.Count)
                    control.ExecuteOpMode(
                        control.OrderOutputs.ElementAt(branch).Key,
                        cc.OperationModes.ElementAt(decision).Key);
            }
        }
    }

    /// <summary>
    /// Is called while SUSPENDING to indicate the currently available actions
    /// to the <see cref="MLModel"/> object before it chooses the next actions
    /// via the <see cref="IMLOperationMode.MLENACT"/> array.
    /// Enables all actions if not overwritten via <see cref="SetEnabledActions"/>
    /// or <see cref="OperationMode.OnSuspend"/>.
    /// </summary>
    /// <remarks>
    /// <c>MLENACT = null</c> tells the <see cref="MLModel"/> that no action (of
    /// its configured action space) is forbidden.
    /// </remarks>
    protected virtual void SetEnabledActions()
    {
        MLENACT = null;
    }
    #endregion Observation and action functions
}
