﻿using System;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using UnityEngine;

/// <summary>
/// The machine learning model, which is implemented as an <see cref="Agent"/> in Unity.
/// This object acts as an asynchronous broker between an <see cref="IMLOperationMode"/> and the <see cref="Agent"/>.
/// </summary>
/// <remarks>
/// In general, the <see cref="MLOperationMode"/> could be placed in another runtime environment.
/// TODO implement an abstract MLModel class with an specialised InternalMLModel and
/// ExternalMLModel to communicate with an external MLOperationMode via network.
/// </remarks>
public class MLModel : Agent
{
    [Tooltip("The operation mode to whom this model belongs.")]
    public string OpModeName = "NONE";
    [Tooltip("Apply a tiny negative reward every step to encourage action. Is divided by MaxStep and applied every step.")]
    public float RStep = -1f;

    private static readonly KeyCode[] AlphaNumber = {
            KeyCode.Alpha0,
            KeyCode.Alpha1,
            KeyCode.Alpha2,
            KeyCode.Alpha3,
            KeyCode.Alpha4,
            KeyCode.Alpha5,
            KeyCode.Alpha6,
            KeyCode.Alpha7,
            KeyCode.Alpha8,
            KeyCode.Alpha9,
    };

    // TODO create and use IControlComponentState instead or use opMode.cc where applicable --> extend IMLOperationMode with EXST, OCCUPIER, OPMODE?
    private ControlComponent cc;
    private IMLOperationMode opMode;

    /// <summary>
    /// Connects the model to the <see cref="ControlComponent"/> and its <see cref="MLOperationMode"/> on start.
    /// </summary>
    public void Awake()
    {
        cc = GetComponent<ControlComponent>();
        opMode = GetComponent<IMLOperationMode>();
    }

    private bool active = false;
    private bool requestedDecision = false;
    [Tooltip("Keep requesting decisions while control component is in SUSPENDED state. Helpful for Heuristic mode. Otherwise only one decision is requested, once the MLMODEL detects the SUSPENDED state.")]
    public bool RepeatDecisionRequests = true;

    /// <summary>
    /// Update frame is used to asynchronous watch the execution of the <see cref="MLOperationMode"/>.
    /// Checks <see cref="ControlComponent.OPMODE"/> and <see cref="ControlComponent.EXST"/>
    /// to activate the Agents behaviour.
    /// Controls the request for decisions and end of episodes via <see cref="IMLOperationMode.MLSC"/>.
    /// Reads and set the rewards from <see cref="IMLOperationMode.MLREWARD"/>.
    /// Additionally current stats are written to <see cref="IMLOperationMode.MLSTATS"/>,
    /// if request.
    /// </summary>
    public void Update()
    {
        if(active)
        {
            // Write current stats if requested
            if(opMode.MLSTATS == null)
                opMode.MLSTATS = string.Format(
                    "Steps: %d, CompletedEpisodes: %d, CumulativeReward: %f",
                    StepCount, CompletedEpisodes, GetCumulativeReward());

            // Check if the agent should make a decision
            if (cc.EXST == ExecutionState.SUSPENDING)
            {
                requestedDecision = false;
            }
            else if (cc.EXST == ExecutionState.SUSPENDED)
            {
                if(!requestedDecision)
                {
                    requestedDecision = true;
                    AddReward(opMode.MLREWARD);
                    RequestDecision();
                }
                else if(RepeatDecisionRequests)
                {
                    RequestDecision();
                }
            }
            else if (cc.EXST == ExecutionState.COMPLETING)
            {
                // Wait for MLOperationMode to set correct reward
                // TODO better solution, without sync via MLSC?
                if(opMode.MLSC == ExecutionState.COMPLETING)
                {
                    AddReward(opMode.MLREWARD);
                    EndEpisode();
                    opMode.MLSC = ExecutionState.COMPLETED;
                    active = false;
                }
            }
            else if (
                   cc.EXST == ExecutionState.STARTING
                || cc.EXST == ExecutionState.EXECUTE
                || cc.EXST == ExecutionState.UNSUSPENDING
                || cc.EXST == ExecutionState.HOLDING
                || cc.EXST == ExecutionState.HELD
                || cc.EXST == ExecutionState.UNHOLDING
                || cc.EXST == ExecutionState.STOPPING
                || cc.EXST == ExecutionState.ABORTING)
            {
                // Do nothing
            }
            else
            {
                if (cc.EXST == ExecutionState.ABORTED
                 || cc.EXST == ExecutionState.STOPPED)
                {
                    AddReward(opMode.MLREWARD);
                }
                EndEpisode();
                // operation mode is not active anymore (Stopped / Aborted)
                active = false;
            }
        }
        else if(cc.EXST == ExecutionState.STARTING && cc.OPMODE.OpModeName.Equals(OpModeName))
        {
            // TODO Is there a value from the agent we can use instead? (EpisodeBegin --> EndEpisode)
            active = true;
            //TODO use opMode.MLMODEL to set behaviour and model, use cc.EXMODE to set InferenceDevice
            //SetModel(string behaviorName, NNModel model, InferenceDevice inferenceDevice)

            // Signal, that starting process is done to operation mode
            opMode.MLSC = ExecutionState.EXECUTE;
        }
    }

    /// <summary>
    /// Stops the control component and respectively its operation mode,
    /// if an episode is ended.
    /// Only necessary to detect if <see cref="Agent.MaxStep"/> is reached.
    /// </summary>
    public override void OnEpisodeBegin()
    {
        //TODO check why necessary?
        SetReward(0);
        //TODO write utility function for EXST to determine "isRunning"
        if (cc.EXST == ExecutionState.STARTING
                || cc.EXST == ExecutionState.EXECUTE
                || cc.EXST == ExecutionState.SUSPENDING
                || cc.EXST == ExecutionState.SUSPENDED
                || cc.EXST == ExecutionState.UNSUSPENDING
                || cc.EXST == ExecutionState.HOLDING
                || cc.EXST == ExecutionState.HELD
                || cc.EXST == ExecutionState.UNHOLDING)
            cc.Stop(cc.OCCUPIER);
        //TODO find better way to detect Agent.MaxStep is reached. --> Maybe override EndEpisode?
        base.OnEpisodeBegin();
    }

    /// <summary>
    /// Reads the <see cref="IMLOperationMode.MLENACT"/> from the corresponding
    /// <see cref="MLOperationMode"/> to mask possible actions.
    /// </summary>
    /// <param name="actionMask">The action mask that is to be changed</param>
    public override void WriteDiscreteActionMask(IDiscreteActionMask actionMask)
    {
        if (opMode.MLENACT == null) return;
        for (int branch = 0; branch < opMode.MLENACT.Length; branch++)
        {
            for (int actionIndex = 0; actionIndex < opMode.MLENACT[branch].Length; actionIndex++)
                if (!opMode.MLENACT[branch][actionIndex])
                    actionMask.WriteMask(branch, new int[]{actionIndex});
        }
    }

    /// <summary>
    /// Ask the user for heuristic decisions.
    /// The alpha value keys (0-9) can be used as a value for the first action branch.
    /// </summary>
    /// <remarks>
    /// TODO Add generic possibility to use multiple action branches.
    /// TODO make selectable or add own script with task specific heuristic mapping or read from a configuration
    /// </remarks>
    /// <param name="actionBuffer">The actions that should be taken</param>
    public override void Heuristic(in ActionBuffers actionBuffer)
    {
        var actions = actionBuffer.DiscreteActions.Array;
        // Reset action buffer, otherwise the last selected actions are used
        Array.Clear(actions, 0, actions.Length);

        // Generic
        for (int i = 0; i <= 9; i++)
        {
            if (Input.GetKey(AlphaNumber[i]))
            {
                actions[0] = i;
                break;
            }
        }

        // Palette specific mapping (has to comply with MLOpModePalette)
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            actions[0] = 1;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            actions[0] = 2;
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            actions[0] = 3 + (Input.GetKey(KeyCode.Y) ? 2 : 0) + (Input.GetKey(KeyCode.X) ? 4 : 0);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            actions[0] = 4 + (Input.GetKey(KeyCode.Y) ? 2 : 0) + (Input.GetKey(KeyCode.X) ? 4 : 0);
        }
    }

    /// <summary>
    /// If a decision is made, the corresponding actions to take are send to the
    /// connected <see cref="MLOperationMode"/> via <see cref="IMLOperationMode.MLOBSERVE"/>.
    /// Currently only discrete actions are implemented.
    /// </summary>
    /// <param name="actionBuffer">The actions that should be executed</param>
    public override void OnActionReceived(ActionBuffers actionBuffer)
    {
        var actions = actionBuffer.DiscreteActions.Array;

        if (!Array.TrueForAll(actions, decision => decision == 0))
        {
            opMode.MLDECIDE = Array.ConvertAll<int, float>(actions, a => a);
            opMode.MLSC = ExecutionState.UNSUSPENDING;
        }

        // Apply a tiny negative reward every step to encourage action
        if (MaxStep > 0) AddReward(RStep / MaxStep);
    }

    /// <summary>
    /// Adds observations by reading the <see cref="IMLOperationMode.MLOBSERVE"/>
    /// of the corresponding <see cref="MLOperationMode"/>.
    /// </summary>
    /// <param name="sensor">The sensor to which the data is added</param>
    public override void CollectObservations(Unity.MLAgents.Sensors.VectorSensor sensor)
    {
        if (opMode.MLOBSERVE != null)
            sensor.AddObservation(opMode.MLOBSERVE);
        else
            Debug.LogWarning("No observation available for MLMODEL of " + name);
    }
}
