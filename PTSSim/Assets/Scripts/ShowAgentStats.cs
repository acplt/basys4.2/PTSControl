﻿using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using UnityEngine;
using UnityEngine.UI;

///<summary>
/// Shows the stats of an <see cref="Unity.MLAgents.Agent"/> class.
///</summary> 
public class ShowAgentStats : MonoBehaviour
{
    [Tooltip("Time between polling new state information from the control component. Can only be changed on startup.")]
    public float UpdateTime = 0.1f;

    public Agent agent;
    private Text text;

    public void Start()
    {
        if(agent == null)
            agent = GetComponentInParent<Agent>();
        text = GetComponent<Text>();
        if (agent && text)
            InvokeRepeating("UpdateText", 0, UpdateTime);
        else if (text)
            text.text = "Didn't find Agent component.";
    }

    public void UpdateText()
    {
        text.text = "Reward: " + agent.GetCumulativeReward().ToString("0.000");
        text.text += "\nStep: " + agent.StepCount;
        text.text += "\nEpisodes: " + agent.CompletedEpisodes;
    }
}
