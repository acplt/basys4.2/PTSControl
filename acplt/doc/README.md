# ACPLT/RTE Palette Transport System Control
This is under current development. The described variables and behaviours are not fully implemented yet.

[[_TOC_]]

## ML Control Component Sequence Examples
The following UML sequence diagrams show communication patterns for some examples use cases regarding ML operation modes.

Prerequisites:
* CC-Frame, ML-OpMode and ML-Model are considered as separeted and communicate via KS/HTTP:
  * GetVar(Path) corresponds to: `http://localhost:7511/getVar?format=plain&path=/TechUnits/GSE/PA001<Path>`
    * `PA001` is the name of the control component.
    * `<Path>` is the first parameter of the GetVar function.
  * SetVar(Path, Value) corresponds to: `http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001<Path>&newvalue=<Value>`
    * `PA001` is the name of the control component.
    * `<Path>` is the first parameter of the SetVar function.
    * `<Value>` is the second parameter of the SetVar function.
  * Ports of the ov servers are documented in the [README](../README.md)
  * Alternative output formats (as response to SetVar and GetVar) can be selected with the `format=` part. Allowed values are: `ksx`,`tcl` or `plain`.
  * The sequence diagrams only show the response to GetVar requests in the plain output format.
  * Depending on the deployment, the ML-OpMode may also be realised as SFC inside the CC-Frame. Hence the communication between ML-OpMode and CC-Frame is realised via SFC actions und FB connections rather than KS/HTTP requests. Nevertheless, its possible to move the ML-OpMode an other RTE, e.g. another ACPLT/RTE server.
  * Multiple SetVar and GetVar request can be combined by adding an index like `path[0]=` and `newvalue[0]=` in one KS/HTTP request. (saves overhead)
  * Some symbols need to be encoded to URL conform versions e.g. `{` --> `%7B`
* CC-Frame, ML-OpMode and ML-Model are considered to act asynchronous with own execution cycle times. The diagram only shows requests at the 'right' time. Typically the GetVar requests are done in a loop to poll for the expected state or other states, e.g. indicating an abort, hold, stop.
* The participants can be compared to the Unity Implementation:
  * [ControlComponent](../../PTSSim/Assets/Scripts/CC)
  * [MLOperationMode](../../PTSSim/Assets/Scripts/ML/MLOperationMode.cs) 
  * [MLModel](../../PTSSim/Assets/Scripts/ML/MLModel.cs) 

### 1. Operation Mode Selection Sequence
![Operation Mode Selection Sequence](sequenz-ml-fahrweise-auswahl.svg)

### 2. Start Sequence
![Start Sequence](sequenz-ml-fahrweise-start.svg)

### 3. Execution Sequence
![Execution Sequence](sequenz-ml-fahrweise-ausführung.svg)

### 4. End Sequence
![End Sequence](sequenz-ml-fahrweise-ende.svg)

### 4. Reset Sequence
![Reset Sequence](sequenz-ml-fahrweise-reset.svg)

## How To interact with the ML operation mode manually
The ML interface (MLSC, MLOBSERVE, MLDECIDE) is currently available at the operation mode function block ML inside of the control component: PA001/Skills/ML.
![ML interface FB](ml-model-interface-at-operation-mode.png)

### Example
The following example sequence is documented in [record-acplt-ml-interface-by-hand.xlsx](record-acplt-ml-interface-by-hand.xlsx).

0. Start Simulation and acplt environment with `autostart.bat` according to [README](../README.md)
1. Occupy PA001 control component (e.g. as *Operator*)
   * Via HMI: http://loccalhost:7509/gse/
   * Via KS (also available here: http://localhost:7511): http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001.CMD&newvalue=Operator%3BOCCUPY%3B
2. Set PA001 to IDLE execution state (e.g. send ABORT, CLEAR, RESET sequence), if it is not already in IDLE state.
   * Via HMI: http://loccalhost:7509/gse/
   * Via KS (also available here: http://localhost:7511):
     * Abort: http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001.CMD&newvalue=Operator%3BABORT%3B
     * Clear: http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001.CMD&newvalue=Operator%3BCLEAR%3B
     * Reset: http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001.CMD&newvalue=Operator%3BRESET%3B
3. Select ML operation mode with Goal parameter (e.g. `Operator;ML;Goal=PE018`)
   * Via KS (also available here: http://localhost:7511): http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001.CMD&newvalue=Operator%3BML%3BGoal%3DPE018
4. Manually act as ML model: 
   1. Select sequence
      * Read OPMODE == ML: http://localhost:7511/getVar?format=plain&path=/TechUnits/GSE/PA001/OPMODE.value
      * Write MLSC = STARTING: http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001/Skills/ML/MLSC.value&newvalue=STARTING
      * Send START command to PA001 as operator
        * Via HMI: http://loccalhost:7509/gse/
        * Via KS (also available here: http://localhost:7511): http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001.CMD&newvalue=Operator%3BSTART%3B
   2. Start sequence
      * Read EXST == STARTING: http://localhost:7511/getVar?format=plain&path=/TechUnits/GSE/PA001/EXST.value
      * Write MLSC = EXECUTE: http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001/Skills/ML/MLSC.value&newvalue=EXECUTE
   3. Execution Sequence
      * Read EXST == SUSPENDED: http://localhost:7511/getVar?format=plain&path=/TechUnits/GSE/PA001/EXST.value
      * Read MLOBSERVE: http://localhost:7511/getVar?format=plain&path=/TechUnits/GSE/PA001/Skills/ML/MLOBSERVE.value
      * Write MLDECIDE (e.g. 2): http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001/Skills/ML/MLDECIDE.value&newvalue=2
        * 1 = Left, 2 = Right
        * 3 = Up, 4 = Down
        * 5 = LeftUpShift, 6 = LeftDownShift
        * 7 = RightUpShift, 8 = RightDownShift
      * Write MLSC = UNSUSPENDING: http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001/Skills/ML/MLSC.value&newvalue=UNSUSPENDING
      * ... Repeat Execution Sequence till goal is reached
   4. End Sequence
      * Read EXST == SUSPENDED: http://localhost:7511/getVar?format=plain&path=/TechUnits/GSE/PA001/EXST.value
      * Write MLSC = COMPLETE: http://localhost:7511/setVar?format=plain&path=/TechUnits/GSE/PA001/Skills/ML/MLSC.value&newvalue=COMPLETE