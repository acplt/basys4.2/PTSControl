# ACPLT/RTE - Runtime Environment for realtime PTS Simulation
This folder contains an [ACPLT/RTE](https://github.com/acplt/rte) runtime environment system for a more realistic realtime simulation of the palette transport system modeled by the SMS Group GmbH, which is currently not hosted here due to IP reasons.

Different PTS models may be used for the ML training and evaluation as depicted in the following figure.
![Scenario Model Usage](doc/scenario-model-usage.png)
The ACPLT runtime environment is shown in the center as **Soft-PLC**.
The corresponding model is denoted as **Fictional Palette Transport System**

## Start Demo
* execute `autostart.bat`

### Test Demo
* Check logfiles for errors: 
  * `acplt\servers\<<Servername>>\logfiles\log_autostart_server.txt`
* Start the simulation
...
* Simple HMI for control components:
  * http://localhost:7509/ese/
  * http://localhost:7509/gse/

## Infos on the Demo
* autostart.bat starts up 3 ACPLT/RTE server with ACPLT/KS protocol:
  * **MANAGER**: Port 7509
    * HMI: Loads a simple online engineering HMI, which can be used in the browser to inspect and change the control logic
      * Browserendpoint: http://localhost:7509/hmi/
  * **ESE**: Port 7510
    * Loads and executes the single control components for conveyors with their control logic
    * SHM: connection to simulation
      * The signal adresses for IOs are loaded from `acplt/IO_Configuration.xml`
  * **GSE**: Port 7511
    * Loads and executes the group control components for palettes with their control logic
* The ACPLT/RTE system can be found in acplt folder with following relevant folders:
  * servers: each ACPLT/RTE server has its own folder with:
    * Configuration files 
    * Database
    * Autostart script
    * Logfiles
  * system:
    * sysbin: RTE, database and loading tool executables
    * addonlibs and syslibs: libraries that can be dynamically loaded by the server
    * systools: tcl scripts to load and configure databases
  * templates:
    * Serialized engineering files of the control components
    * Serialized files for HMI and OPC UA Server
* More documentation on communication with the control components can be found in [doc](doc/README.md).

### Specification
  * The control component OPC UA specification can be found in the [BaSyx Wiki Control Component Page](https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponent#OPC-UA)
  * Other examples for OPC UA control components can be found in the [CCProfilesUA project](https://git.rwth-aachen.de/acplt/basys4.2/ccProfilesUA).
  * Variations of control components are described via facets and profiles in the [BaSyx Wiki Control Component Profiles Page](https://wiki.eclipse.org/BaSyx_/_Documentation_/_API_/_ControlComponentProfiles).