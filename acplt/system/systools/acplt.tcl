# Register the package
#TODO make a package: package provide acplt 1.0
package require http

namespace eval acplt {
    ##### variables #####
    ### system and server definition ###
    variable THISACPLTSYSTEM [pwd]/../.. ;#TODO write setter method with [file normalize newPath]
    #TODO get from ov_server.conf:
    variable SERVERNAME "MANAGER"
    variable SERVERPORT 7509
    variable SERVERIP localhost

    ### settings ###
    variable SERVERTIMEOUT 5000; #TODO better method to determine that server is running?
    variable STARTASSUBPROCESS False
    #TODO add parameter for valgrind under linux

    ### static vars ###
    variable OVENVSET False

    ##### exported functions #####
    namespace export logger ks startServer loadFBD loadTemplate loadInstance

    ##### setup minimal logger #####
    namespace eval logger {
        #TODO use package import logger instead?
        variable LOGLEVEL 2; # 0 = TRACE, 1 = DEBUG, 2 = INFO, 3 = WARN, 4 = ERROR
        variable LINE "________________________________________________________________________________"
        proc log {level msg} {
            if {[::info level] > 2} {
                #TODO concat [info level list]
                set context [namespace tail [lindex [::info level [expr [::info level]-2]] 0]]
            } else {
                set context main
            }
            set time [clock format [clock seconds] -format %H:%M:%S]
            puts "$time $level - $context : $msg"
        }
        #### exported functions ####
        namespace export trace debug info warn error separator newLine
        proc trace {msg} {
            variable LOGLEVEL
            if {$LOGLEVEL <= 0} { log "TRACE" $msg }
        }
        proc debug {msg} {
            variable LOGLEVEL
            if {$LOGLEVEL <= 1} { log "DEBUG" $msg }
        }
        proc info {msg} {
            variable LOGLEVEL
            if {$LOGLEVEL <= 2} { log "INFO " $msg }
        }
        proc warn {msg} {
            variable LOGLEVEL
            if {$LOGLEVEL <= 3} { log "WARN " $msg }
        }
        proc error {msg} {
            variable LOGLEVEL
            if {$LOGLEVEL <= 4} { log "ERROR" $msg }
        }
        proc separator {} {
            variable LINE
            puts $LINE
        }
        proc newLine {} { puts "" }
    }

    ##### Kommunikations System KS #####
    namespace eval ks {
        namespace export createObject link getVar setVar renameObject deleteObject unlink getServer getEP
        variable KSFORMAT plain
        proc ksHTTPRequest {request args {port 0}} {
            variable ::acplt::SERVERIP
            variable KSFORMAT
            if {$port == 0} {
                variable ::acplt::SERVERPORT
                set port $SERVERPORT
            }
            set url "http://$SERVERIP:$port/$request?format=$KSFORMAT&$args"
            ::acplt::logger::trace "geturl: $url"
            catch {::http::data [::http::geturl $url]} result options
            if {[dict get $options -code] != 0} {
                ::acplt::logger::warn "$request got error: $result"
            } else {
                ::acplt::logger::debug "$request got result: $result"
            }
            return $result
        }
        proc getVar {path} {
            return [ksHTTPRequest "getVar" path=$path]
        }
        proc setVar {path value} {
            return [ksHTTPRequest "setVar" path=$path&[::http::formatQuery newvalue $value]]
        }
        proc createObject {factory path} {
            return [ksHTTPRequest "createObject" factory=$factory&path=$path]
        }
        proc renameObject {path newname} {
            return [ksHTTPRequest "renameObject" path=$path&newname=$newname]
        }
        proc deleteObject {path} {
            return [ksHTTPRequest "deleteObject" path=$path]
        }
        proc link {path element} {
            return [ksHTTPRequest "link" path=$path&element=$element]
        }
        proc unlink {path element} {
            return [ksHTTPRequest "unlink" path=$path&element=$element]
        }
        proc getServer {{name ""}} {
            if {$name == ""} {
                variable ::acplt::SERVERNAME
                set name $SERVERNAME
            } 
            return [ksHTTPRequest "getServer" servername=$name 7509]
        }
        proc getEP {path requestType requestOutput} {
            return [ksHTTPRequest "getEP" path=$path&requestType=$requestType&requestOutput=$requestOutput]
        }
    }
    ##### Server startup  #####

    # Check wheter something is already listening
    proc checkPortFree {} {
        variable SERVERIP
        variable SERVERPORT
        variable SERVERTIMEOUT
        logger::info "Checking port $SERVERIP:$SERVERPORT by connecting. Will wait $SERVERTIMEOUT ms."
        # setup socket
        set sock [socket -async $SERVERIP $SERVERPORT]
        # test if socket is writeable
        fileevent $sock writable
        after $SERVERTIMEOUT
        # evaluate error to determine whether something is listening
        set err [fconfigure $sock -error]
        logger::trace "Socket connect returned error: $err"
        # shutdown socket
        catch {close $sock}
        if { $err ne "" } {
            logger::debug "Port $SERVERPORT is not in use." 
            return True
        }
        logger::debug "Something is listening on port $SERVERIP:$SERVERPORT."
        return False
    }

    proc checkPIDAlive {pid} {
        if {$pid == 0} {
            logger::trace "Skipping PID check, because pid is 0."
            return True
        }
        if {[lsearch $::tcl_platform(os) "Windows"] >= 0} then {
			catch {exec tasklist } test
            if { [string match *$pid* $test] == 1 } {
                return True
            } else {
                logger::debug "PID $pid not in tasklist."
                return False
		    }
        } else { 	
            #probing if the process is still alive
            set running [exec ps -p $pid | grep $pid]
            if { $running == "" || [regexp "(.*)defunct(.*)" $running]} {
                logger::debug "PID $pid not in process list."
                return False
            } else {
                return True
            }
        }
    }

    proc setOVEnvironment {} {
        variable OVENVSET
        if {$OVENVSET} {return}
        variable THISACPLTSYSTEM
        # set environment variables for ov_runtimeserver
        set ::env(ACPLT_HOME) ${THISACPLTSYSTEM}
        if {![info exists ::env(PATH)]} { set ::env(PATH) "" }
        set ::env(PATH) "${THISACPLTSYSTEM}/system/sysbin;${THISACPLTSYSTEM}/system/addonlibs;${THISACPLTSYSTEM}/system/syslibs;$::env(PATH)"
        logger::trace "PATH environment set to $::env(PATH)"
        #  set LD_LIBRATY_PATH environment in linux
        if {$::tcl_platform(os) == "Linux"} then {
            if {![info exists ::env(LD_LIBRARY_PATH)]} { set ::env(LD_LIBRARY_PATH) "" }
            set ::env(LD_LIBRARY_PATH) "${THISACPLTSYSTEM}/system/addonlibs;${THISACPLTSYSTEM}/system/sysbin;${THISACPLTSYSTEM}/system/syslibs;$::env(LD_LIBRARY_PATH)"
        }
        set OVENVSET True
        return
    }

    # returns 1 if already running and no restart is forced, 0 on correct start/restart, and -1 on error #TODO make enum?
    proc startServer {{restart False}} {
        variable THISACPLTSYSTEM
        variable SERVERNAME
        variable SERVERPORT
        variable SERVERTIMEOUT
        variable STARTASSUBPROCESS

        #TODO only start if serverip  = localhost / 127.0.0.1 / hostname
        
        # check that server is not already running
        if {![checkPortFree]} {
            if {$restart} {
                logger::info "$SERVERNAME is probably running, as port $SERVERPORT is in use. Will shutdown the server and restart."
                if {![stopServer]} {
                    logger::error "Error during shutdown of $SERVERNAME on port $SERVERPORT for restart."
                    return -1
                }
                logger::info "$SERVERNAME stopped for restart."
            } else {
                logger::warn "$SERVERNAME is probably running, as port $SERVERPORT is in use. Skipping startup."
                return 1
            }
        }

        #TODO add parameter for cleaning database and clean db
        ### start server ###
        setOVEnvironment
        # set directorys and execute
        set serverDir $THISACPLTSYSTEM/servers/$SERVERNAME
        logger::debug "Starting from working directory: [pwd]"
        if {$STARTASSUBPROCESS} {
            set logfile $serverDir/logfiles/log_start_server.txt
            set pid [exec $THISACPLTSYSTEM/system/sysbin/ov_runtimeserver -c [file nativename $serverDir/ov_server.conf] -l $logfile &]
        } else {
            #TODO check OS, check VALGRIND
            set pid [eval exec [auto_execok start] {/min [file nativename $THISACPLTSYSTEM/system/sysbin/ov_runtimeserver.exe] -c [file nativename $serverDir/ov_server.conf] &}]
            set pid 0  ;#TODO get right PID from ov_runtimeserver.exe --> ks::getVar /vendor/serverPID
        }
        # check that server is alive
        if {[checkPIDAlive $pid]} {
            logger::info "$SERVERNAME started with PID $pid on port $SERVERPORT."
            if {$SERVERNAME != "MANAGER"} {
                for {set i 0} {$i < 5} {incr i} {
                    set result [lindex [split [ks::getServer] ";"] 0]
                    if { $result == $SERVERPORT} {
                        return 0
                    }
                    after $SERVERTIMEOUT
                }
                logger::trace "KS getServer returned: $result"
                logger::warn "Server started, but not registered at MANAGER after 5 attempts of $SERVERTIMEOUT ms."
            }
            return 0
        } else {
            logger::error "Error during startup of $SERVERNAME on port $SERVERPORT."
            return -1
        }
    }

    proc stopServer {} {
        ks::setVar /vendor.server_run FALSE
    }

    ##### FBD loading #####

    proc loadFBD {path} {
        variable THISACPLTSYSTEM
        variable SERVERIP
        variable SERVERNAME
        variable SERVERPORT
        setOVEnvironment
        set fbdPath "$THISACPLTSYSTEM/$path"
        set logPath [file dirname $fbdPath]/[file rootname [file tail $path]].log
        file delete $logPath
        # execute fb_dbcommands
        #TODO check how to change manager port to serverport ($SERVERPORT) is necessary (7509 in next line) --> direct loading to server
		#TODO check why myFBD.test loads myFBD.fbd
        #TODO check wheter logfile can be saved to servers/$SERVERNAME/logfiles/
        set cmd "$THISACPLTSYSTEM/system/sysbin/fb_dbcommands -s $SERVERIP:7509/$SERVERNAME -load -f $fbdPath"
        logger::trace "Load cmd: $cmd"
        # read answer
        set fd [open "|$cmd" "RDONLY"]
        catch {close $fd} loadResult loadOption;# TODO in fb_dbcommands exit with 0 if no error --> use loadOption instead
        # move log to server logfile dir
        set newLogPath $THISACPLTSYSTEM/servers/$SERVERNAME/logfiles/[file tail $logPath]
        file rename -force $logPath $newLogPath
        if {[string match "* geladen." $loadResult]} {
            logger::info "Loaded FBD $path into $SERVERIP/$SERVERNAME"
            return True
        } else {
            logger::warn "Loading FBD $path into $SERVERIP/$SERVERNAME returned:\n$loadResult\nSee log for details: $newLogPath"
            return False
        }
    }

    proc loadTemplate {path} {
        return [loadFBD templates/$path]
    }

    proc loadInstance {path} {
        variable SERVERNAME
        return [loadFBD servers/$SERVERNAME/modelinstances/$path]
    }    
}