#!/bin/tclsh
# No parameters.
# Starts every server in acplt/servers/../folder via its autostart_server.tcl script
# Has to be placed in acplt/system/systools/

proc startServer {serverDir} {
    set serverName [file tail $serverDir]
    if { [file exists $serverDir/autostart_server.tcl] == 1} {
        puts "________________________________________________________________________________"
        puts "\nStarting Server: $serverName"
        set oldDir [pwd]
        cd $serverDir
        catch { exec tclsh autostart_server.tcl & > logfiles/log_autostart_server.txt} result option
        if {[dict get $option -code] == 0} {
            puts "- Done. See logfile for details:"
        } else {
            puts "- Error. servers/$serverName/autostart_server.tcl returned:"
            puts $result
            puts "- See logfile for details:"
        }
        puts "servers/$serverName/logfiles/log_autostart_server.txt"
        cd $oldDir
    } else {
        puts "\nServer has no autostart_server.tcl script. Skipping: $serverName"
    }
}

### Main script ###

puts "Starting all servers in autostart mode from acplt system dir:"
puts [file normalize [pwd]/../../]

# Start MANAGER server first
startServer ../../servers/MANAGER

# Start other servers
foreach serverDir [glob -type d ../../servers/*] {
    if { $serverDir == "../../servers/MANAGER" } {continue}
    startServer $serverDir
}

puts "________________________________________________________________________________"
puts "Done."