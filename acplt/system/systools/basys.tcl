#!/bin/tclsh
# no parameter.

##### load requried libs #####
#Assume that acplt.tcl is in same directory
source [ file dirname [ file normalize [ info script ] ] ]/acplt.tcl

namespace eval basys {
    variable FOLDERNAME ESE ;# Path behind /TechnUnits/ only works for one folder right now
    variable COMPONENT "" ;# all procedures work on the current component name e.g. KR01, PE004, ...
    variable CYCLETIME 1000 ;# Miliseconds. Used to wait during configuration steps
    variable PROGRESSBAR False ;# Used to display fancy progress bar for setVarList --> only useful when output goes to stdout

    namespace export setVar setVarList configureComponent loadComponent sendCMD resetToIdle

    proc progres {cur tot} {
        variable PROGRESSBAR
        if { !$PROGRESSBAR } {
            #puts -nonewline "."
            return
        }
        # if you don't want to redraw all the time, uncomment and change ferquency
        #if {$cur % ($tot/300)} { return }
        # set to total width of progress bar
        set total 80
    
        set half [expr {$total/2}]
        set percent [expr {100.*$cur/$tot}]
        set val (\ [format "%6.2f%%" $percent]\ )
        set str "\r|[string repeat = [
            expr {round($percent*$total/100)}]][
                string repeat { } [expr {$total-round($percent*$total/100)}]]|"
        set str "[string range $str 0 $half]$val[string range $str [expr {$half+[string length $val]-1}] end]"
        puts -nonewline $stdout $str
    }

    proc setVar {path value} {
        variable FOLDERNAME
        variable COMPONENT
        set fullPath /TechUnits/$FOLDERNAME/$COMPONENT$path
        set result [acplt::ks::setVar $fullPath $value]
        if {$result != ";"} {
            acplt::logger::warn "Could't set value $value for path: $fullPath"
            return False
        }
        return True
    }

    proc setVarList {{infoTxt ""} pathValueList {waitCycles 0}} {
        variable CYCLETIME
        if {$infoTxt != ""} {acplt::logger::info $infoTxt}
        acplt::logger::trace "Setting $pathValueList"
        set waitTime [expr $CYCLETIME * $waitCycles]
        set length [expr [llength $pathValueList]/2]
        set counter 0
        foreach {path value} $pathValueList {
            setVar $path $value
            incr counter
            progres $counter $length
            if {$waitTime >0} {after $waitTime}
        }
        #acplt::logger::newLine
    }

    proc renameObject {path newname} {
        variable FOLDERNAME
        variable COMPONENT
        set fullPath /TechUnits/$FOLDERNAME/$COMPONENT$path
        set result [acplt::ks::renameObject $fullPath $newname]
        if {$result != ";"} {
            acplt::logger::warn "Could't rename object to $newname with path: $fullPath"
            return False
        }
        return True
    }

    proc deleteObject {path} {
        variable FOLDERNAME
        variable COMPONENT
        set fullPath /TechUnits/$FOLDERNAME/$COMPONENT$path
        set result [acplt::ks::deleteObject $fullPath]
        if {$result != ";"} {
            acplt::logger::warn "Could't delete object with path: $fullPath"
            return False
        }
        return True
    }

    proc link {path element} {
        variable FOLDERNAME
        variable COMPONENT
        set fullPath /TechUnits/$FOLDERNAME/$COMPONENT$path
        set result [acplt::ks::link $fullPath $element]
        if {$result != ";"} {
            acplt::logger::warn "Could't link element $element to object with path: $fullPath"
            return False
        }
        return True
    }

    proc unlink {path element} {
        variable FOLDERNAME
        variable COMPONENT
        set fullPath /TechUnits/$FOLDERNAME/$COMPONENT$path
        set result [acplt::ks::unlink $fullPath $element]
        if {$result != ";"} {
            acplt::logger::warn "Could't unlink element $element from object with path: $fullPath"
            return False
        }
        return True
    }

    # suffix = "" leads to an inplace edit
    proc replaceName {fbd oldName newName {suffix .temp}} {
        if {$oldName != $newName} {
            set fd [open $fbd r]
            set newFd [open $fbd.temp w]
            acplt::logger::trace "Replacing $oldName with $newName in $fbd"
            while {[gets $fd line] >= 0} {
                set mapping [list $oldName $newName]
                set newline [string map $mapping $line]
                puts $newFd $newline
            }
            close $fd
            close $newFd
            file rename -force $fbd.temp $fbd$suffix
        } elseif {$suffix != ""} {
            file copy -force $fbd $fbd$suffix
            #TODO catch error
        }

    }

    proc configureComponent {} {
        variable CYCLETIME
        # Resetting SSCs
        setVarList "Resetting SSCs (EN = 3)" {
            "/OrIn/Occ.EN" 3
            "/ExMode/Mode.EN" 3
            "/ExMode/Clearing.EN" 3
            "/ExMode.EN" 3
            "/Interlock/Clear.EN" 3
            "/Interlock/Abort.EN" 3
            "/Skills/Stop.EN" 3
            "/Skills/Reset.EN" 3
        }
        #Enable SSCs after reset
        after [expr $CYCLETIME * 3]
        setVarList "Enable SSC (EN = 1)" {
            "/OrIn/Occ.EN" 1
            "/ExMode/Mode.EN" 1
            "/ExMode/Clearing.EN" 1
            "/ExMode.EN" 1
        }
    }

    proc loadComponent {{templateName ""}} {
        #TODO create own log for component
        variable COMPONENT
        variable FOLDERNAME
        variable CYCLETIME
        if {$templateName == ""} { set templateName $COMPONENT}
        acplt::logger::info "Loading component $COMPONENT from template $templateName"
        set templateFBD [file normalize $::acplt::THISACPLTSYSTEM/templates/basys/TechUnits_${FOLDERNAME}_$templateName.fbd]
        set targetFBD [file normalize $::acplt::THISACPLTSYSTEM/servers/$::acplt::SERVERNAME/modelinstances/$COMPONENT.fbd]
        catch {file copy -force $templateFBD $targetFBD} result
        #TODO evaluate resulte, warn and exit in case of error
        acplt::logger::trace "Copying template to modelinstance: \n$templateFBD\n$targetFBD"
        #TODO old version was more specific, but left out variable renaming: replaceName $targetFBD /TechUnits/$FOLDERNAME/$templateName /TechUnits/$FOLDERNAME/$COMPONENT ""
        replaceName $targetFBD $templateName $COMPONENT ""
        if {![acplt::loadInstance $COMPONENT.fbd]} {return False}
        after [expr $CYCLETIME * 3]
        configureComponent
        return True
    }

    proc sendCMD {order {parameter ""} {sender "Anonymous"}} {
        acplt::logger::debug "Sending CMD: $sender;$order;$parameter"
        setVar .CMD "$sender;$order;$parameter"
    }

    proc getCCList {} {
        variable FOLDERNAME
        return [acplt::ks::getEP /TechUnits/$FOLDERNAME OT_DOMAIN OP_NAME]
    }

    proc sendCMDToAll {CCList order {sender "Anonymous"} {waitCycles 0}} {
        variable CYCLETIME
        foreach {cc} $CCList {
            set cc [string trimright $cc ";"]
            set basys::COMPONENT $cc
            basys::sendCMD $order "" $sender
        }
        if {$waitCycles >0} {after [expr $CYCLETIME * $waitCycles]}
}

    proc resetToIdle {{sender "Anonymous"}} {
        variable COMPONENT
        setVarList "Resetting $COMPONENT to idle state:" [subst {
            ".CMD" "$sender;OCCUPY;"
            ".CMD" "$sender;ABORT;"
            ".CMD" "$sender;CLEAR;"
            ".CMD" "$sender;RESET;"
            ".CMD" "$sender;STOP;"
            ".CMD" "$sender;RESET;"
            ".CMD" "$sender;FREE;"
         }] 3
    }

    proc setCycletime {cyctime} {
        variable CYCLETIME $cyctime
        set cyctimeSecs [format %.4f [expr $CYCLETIME / 1000.0]]
        acplt::logger::info "Setting cyctime to $cyctimeSecs sec."
        acplt::ks::setVar "/Tasks/UrTask.cyctime" $cyctimeSecs
    }
}