/* shub
 * this file ist generated automatically by perl script
 * Path            : ../../01_lib/00_perl/90_script_tree 
 * File            : tree_prj.pl
 *
 * first creation
 * ----------------------------- 
 * Date            : Wednesday, March 1st 2017
 * Time            : 17:11:28h
 * User            : kalk
 *
 * last update
 * ---------------------------------------
 * Update Date     : Tuesday, September 4th 2018
 * Update Time     : 11:48:16h
 * --------------------------------------------------------------------------------
 */
 
#ifndef LC_PROT_LC_TD_HiltSimStruct
#define LC_PROT_LC_TD_HiltSimStruct

/*
 *  Typedefinition
 */
typedef struct _LC_TD_HiltSimStruct
{


 /*
  * input configuration: hilt_const_system
  * ----------------------------------------------------------
  * date: Wednesday, March 1st 2017
  *       17:11:28h
  * ----------------------------------------------------------
  */

  LC_TD_BOOL  A0000_0__SIM_INTERN__x_________x_______x_______RFM_Initialized_________________________; /* Index:      0 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_0___x_______LC_vg_ExtRtwSimRun______________________; /* Index:      1 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_0___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:      2 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_0___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:      3 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_0___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:      4 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwActT0_______________________; /* Index:      5 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwActT1_______________________; /* Index:      6 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwActT2_______________________; /* Index:      7 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwActT3_______________________; /* Index:      8 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:      9 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:     10 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:     11 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:     12 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:     13 */
  LC_TD_REAL  TCS______TARGET______real______RTW_0___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:     14 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_0___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:     15 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_0___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:     16 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_1___x_______LC_vg_ExtRtwSimRun______________________; /* Index:     17 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_1___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:     18 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_1___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:     19 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_1___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:     20 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwActT0_______________________; /* Index:     21 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwActT1_______________________; /* Index:     22 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwActT2_______________________; /* Index:     23 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwActT3_______________________; /* Index:     24 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:     25 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:     26 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:     27 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:     28 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:     29 */
  LC_TD_REAL  TCS______TARGET______real______RTW_1___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:     30 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_1___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:     31 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_1___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:     32 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_2___x_______LC_vg_ExtRtwSimRun______________________; /* Index:     33 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_2___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:     34 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_2___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:     35 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_2___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:     36 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwActT0_______________________; /* Index:     37 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwActT1_______________________; /* Index:     38 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwActT2_______________________; /* Index:     39 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwActT3_______________________; /* Index:     40 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:     41 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:     42 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:     43 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:     44 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:     45 */
  LC_TD_REAL  TCS______TARGET______real______RTW_2___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:     46 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_2___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:     47 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_2___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:     48 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_3___x_______LC_vg_ExtRtwSimRun______________________; /* Index:     49 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_3___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:     50 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_3___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:     51 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_3___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:     52 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwActT0_______________________; /* Index:     53 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwActT1_______________________; /* Index:     54 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwActT2_______________________; /* Index:     55 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwActT3_______________________; /* Index:     56 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:     57 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:     58 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:     59 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:     60 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:     61 */
  LC_TD_REAL  TCS______TARGET______real______RTW_3___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:     62 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_3___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:     63 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_3___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:     64 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_4___x_______LC_vg_ExtRtwSimRun______________________; /* Index:     65 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_4___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:     66 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_4___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:     67 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_4___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:     68 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwActT0_______________________; /* Index:     69 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwActT1_______________________; /* Index:     70 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwActT2_______________________; /* Index:     71 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwActT3_______________________; /* Index:     72 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:     73 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:     74 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:     75 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:     76 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:     77 */
  LC_TD_REAL  TCS______TARGET______real______RTW_4___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:     78 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_4___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:     79 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_4___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:     80 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_5___x_______LC_vg_ExtRtwSimRun______________________; /* Index:     81 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_5___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:     82 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_5___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:     83 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_5___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:     84 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwActT0_______________________; /* Index:     85 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwActT1_______________________; /* Index:     86 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwActT2_______________________; /* Index:     87 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwActT3_______________________; /* Index:     88 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:     89 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:     90 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:     91 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:     92 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:     93 */
  LC_TD_REAL  TCS______TARGET______real______RTW_5___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:     94 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_5___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:     95 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_5___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:     96 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_6___x_______LC_vg_ExtRtwSimRun______________________; /* Index:     97 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_6___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:     98 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_6___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:     99 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_6___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    100 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwActT0_______________________; /* Index:    101 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwActT1_______________________; /* Index:    102 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwActT2_______________________; /* Index:    103 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwActT3_______________________; /* Index:    104 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    105 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    106 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    107 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    108 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    109 */
  LC_TD_REAL  TCS______TARGET______real______RTW_6___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    110 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_6___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    111 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_6___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    112 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_7___x_______LC_vg_ExtRtwSimRun______________________; /* Index:    113 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_7___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    114 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_7___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    115 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_7___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    116 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwActT0_______________________; /* Index:    117 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwActT1_______________________; /* Index:    118 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwActT2_______________________; /* Index:    119 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwActT3_______________________; /* Index:    120 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    121 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    122 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    123 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    124 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    125 */
  LC_TD_REAL  TCS______TARGET______real______RTW_7___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    126 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_7___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    127 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_7___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    128 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_8___x_______LC_vg_ExtRtwSimRun______________________; /* Index:    129 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_8___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    130 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_8___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    131 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_8___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    132 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwActT0_______________________; /* Index:    133 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwActT1_______________________; /* Index:    134 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwActT2_______________________; /* Index:    135 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwActT3_______________________; /* Index:    136 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    137 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    138 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    139 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    140 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    141 */
  LC_TD_REAL  TCS______TARGET______real______RTW_8___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    142 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_8___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    143 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_8___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    144 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_9___x_______LC_vg_ExtRtwSimRun______________________; /* Index:    145 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_9___x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    146 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_9___x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    147 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_9___x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    148 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwActT0_______________________; /* Index:    149 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwActT1_______________________; /* Index:    150 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwActT2_______________________; /* Index:    151 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwActT3_______________________; /* Index:    152 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    153 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    154 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    155 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    156 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    157 */
  LC_TD_REAL  TCS______TARGET______real______RTW_9___x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    158 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_9___x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    159 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_9___x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    160 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_10__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    161 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_10__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    162 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_10__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    163 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_10__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    164 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    165 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    166 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    167 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    168 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    169 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    170 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    171 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    172 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    173 */
  LC_TD_REAL  TCS______TARGET______real______RTW_10__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    174 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_10__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    175 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_10__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    176 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_11__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    177 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_11__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    178 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_11__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    179 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_11__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    180 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    181 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    182 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    183 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    184 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    185 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    186 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    187 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    188 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    189 */
  LC_TD_REAL  TCS______TARGET______real______RTW_11__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    190 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_11__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    191 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_11__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    192 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_12__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    193 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_12__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    194 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_12__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    195 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_12__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    196 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    197 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    198 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    199 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    200 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    201 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    202 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    203 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    204 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    205 */
  LC_TD_REAL  TCS______TARGET______real______RTW_12__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    206 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_12__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    207 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_12__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    208 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_13__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    209 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_13__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    210 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_13__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    211 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_13__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    212 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    213 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    214 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    215 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    216 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    217 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    218 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    219 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    220 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    221 */
  LC_TD_REAL  TCS______TARGET______real______RTW_13__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    222 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_13__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    223 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_13__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    224 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_14__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    225 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_14__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    226 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_14__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    227 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_14__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    228 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    229 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    230 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    231 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    232 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    233 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    234 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    235 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    236 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    237 */
  LC_TD_REAL  TCS______TARGET______real______RTW_14__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    238 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_14__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    239 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_14__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    240 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_15__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    241 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_15__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    242 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_15__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    243 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_15__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    244 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    245 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    246 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    247 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    248 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    249 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    250 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    251 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    252 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    253 */
  LC_TD_REAL  TCS______TARGET______real______RTW_15__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    254 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_15__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    255 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_15__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    256 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_16__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    257 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_16__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    258 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_16__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    259 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_16__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    260 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    261 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    262 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    263 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    264 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    265 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    266 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    267 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    268 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    269 */
  LC_TD_REAL  TCS______TARGET______real______RTW_16__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    270 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_16__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    271 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_16__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    272 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_17__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    273 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_17__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    274 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_17__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    275 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_17__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    276 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    277 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    278 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    279 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    280 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    281 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    282 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    283 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    284 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    285 */
  LC_TD_REAL  TCS______TARGET______real______RTW_17__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    286 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_17__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    287 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_17__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    288 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_18__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    289 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_18__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    290 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_18__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    291 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_18__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    292 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    293 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    294 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    295 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    296 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    297 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    298 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    299 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    300 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    301 */
  LC_TD_REAL  TCS______TARGET______real______RTW_18__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    302 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_18__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    303 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_18__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    304 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_19__x_______LC_vg_ExtRtwSimRun______________________; /* Index:    305 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_19__x_______LC_vg_ExtRtwSimHold_____________________; /* Index:    306 */
  LC_TD_BOOL  TCS______TARGET______bool______RTW_19__x_______LC_vg_ExtRtwWatchDogFlg_________________; /* Index:    307 */
  LC_TD_DINT  TCS______TARGET______i16_______RTW_19__x_______LC_vg_ExtRtwActMdlState_________________; /* Index:    308 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwActT0_______________________; /* Index:    309 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwActT1_______________________; /* Index:    310 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwActT2_______________________; /* Index:    311 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwActT3_______________________; /* Index:    312 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwPt1T2_______________________; /* Index:    313 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwActRunTime__________________; /* Index:    314 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwActSimTime__________________; /* Index:    315 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwRefCycleTimeXPact___________; /* Index:    316 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwRefCycleTimeRTW_____________; /* Index:    317 */
  LC_TD_REAL  TCS______TARGET______real______RTW_19__x_______LC_vg_ExtRtwClkFreq_____________________; /* Index:    318 */
  LC_TD_BOOL  LINUX____TARGET______bool______RTW_19__x_______LC_vg_ExtRtwLifeSignCnt_________________; /* Index:    319 */
  LC_TD_DINT  LINUX____TARGET______i16_______RTW_19__x_______LC_vg_ExtRtwWatchDogCnt_________________; /* Index:    320 */
  LC_TD_UDINT TCS______TARGET______ui32______x_______x_______NanCnt_FromModel2Rfm____________________; /* Index:    321 */
  LC_TD_UDINT TCS______TARGET______ui32______x_______x_______NanCnt_FromRfm2Model____________________; /* Index:    322 */
  LC_TD_UDINT TCS______TARGET______ui32______x_______x_______NanCnt_FromApplication2Rfm______________; /* Index:    323 */
  LC_TD_UDINT TCS______TARGET______ui32______x_______x_______NanCnt_FromRfm2Application______________; /* Index:    324 */

 /*
  * input configuration: 01_hilt_const_matlab
  * ----------------------------------------------------------
  * date: Wednesday, March 1st 2017
  *       17:11:28h
  * ----------------------------------------------------------
  */

  LC_TD_REAL  MTLBSIG__SIM_INTERN__x_________RFM_____x_______Dummy___________________________________; /* Index:    325 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______CmdWrd1_________________________________; /* Index:    326 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______CmdWrd2_________________________________; /* Index:    327 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______CmdWrd3_________________________________; /* Index:    328 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______CmdWrd4_________________________________; /* Index:    329 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______CmdWrd5_________________________________; /* Index:    330 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______CmdWrd6_________________________________; /* Index:    331 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______And_CmdWrd1_____________________________; /* Index:    332 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______And_CmdWrd2_____________________________; /* Index:    333 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______And_CmdWrd3_____________________________; /* Index:    334 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______And_CmdWrd4_____________________________; /* Index:    335 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______And_CmdWrd5_____________________________; /* Index:    336 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______And_CmdWrd6_____________________________; /* Index:    337 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______Or_CmdWrd1______________________________; /* Index:    338 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______Or_CmdWrd2______________________________; /* Index:    339 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______Or_CmdWrd3______________________________; /* Index:    340 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______Or_CmdWrd4______________________________; /* Index:    341 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______Or_CmdWrd5______________________________; /* Index:    342 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______Or_CmdWrd6______________________________; /* Index:    343 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______CcarPOR_TravActPos______________________; /* Index:    344 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______CcarPOR_LiftActPos______________________; /* Index:    345 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______CcarPOR_LiftCoilWeightFr________________; /* Index:    346 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PORMandrelActDiam_______________________; /* Index:    347 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PORMandrelActCylStroke__________________; /* Index:    348 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T1__x_______PORMandrelCylRetracted__________________; /* Index:    349 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T1__x_______PORMandrelExpanded______________________; /* Index:    350 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______CcarTR_TravActPos_______________________; /* Index:    351 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______CcarTR_LiftActPos_______________________; /* Index:    352 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______CcarTR_LiftCoilWeightFr_________________; /* Index:    353 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______TRMandrelActDiam________________________; /* Index:    354 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______TRMandrelActCylStroke___________________; /* Index:    355 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T1__x_______TRMandrelCylRetracted___________________; /* Index:    356 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T1__x_______TRMandrelExpanded_______________________; /* Index:    357 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______CcarPREP_TravActPos_____________________; /* Index:    358 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______CcarPREP_LiftActPos_____________________; /* Index:    359 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______CcarPREP_LiftCoilWeightFr_______________; /* Index:    360 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______POR_ActInnerDiam________________________; /* Index:    361 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______TR_ActInnerDiam_________________________; /* Index:    362 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______POR_ActOuterDiam________________________; /* Index:    363 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______TR_ActOuterDiam_________________________; /* Index:    364 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PORMandrelActPrExpSide__________________; /* Index:    365 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PORMandrelActPrClpSide__________________; /* Index:    366 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______TRMandrelActPrExpSide___________________; /* Index:    367 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______TRMandrelActPrClpSide___________________; /* Index:    368 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PtsTT_RT03_ActAngleRad__________________; /* Index:    369 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PtsTT_RT15_ActAngleRad__________________; /* Index:    370 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PtsTT_RT20_ActAngleRad__________________; /* Index:    371 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PtsTT_RT25_ActAngleRad__________________; /* Index:    372 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PtsTravT_RT10_ActPos____________________; /* Index:    373 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______PtsTravT_RT24_ActPos____________________; /* Index:    374 */

 /*
  * input configuration: 02_hilt_const_unity
  * ----------------------------------------------------------
  * date: Wednesday, March 1st 2017
  *       17:11:28h
  * ----------------------------------------------------------
  */

  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T3__x_______Dummy___________________________________; /* Index:    375 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______CcarPREP_LiftCoilInnerDiam______________; /* Index:    376 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______CcarPREP_LiftCoilOuterDiam______________; /* Index:    377 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______CcarPREP_LiftCoilWidth__________________; /* Index:    378 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______CcarPOR_LiftCoilInnerDiam_______________; /* Index:    379 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______CcarPOR_LiftCoilOuterDiam_______________; /* Index:    380 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______CcarPOR_LiftCoilWidth___________________; /* Index:    381 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______CcarTR_LiftCoilInnerDiam________________; /* Index:    382 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______CcarTR_LiftCoilOuterDiam________________; /* Index:    383 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______CcarTR_LiftCoilWidth____________________; /* Index:    384 */
  LC_TD_UDINT UNITY____SIM_INTERN__ui32______RFM_T2__x_______PORMandrelSleeveUsed____________________; /* Index:    385 */
  LC_TD_UDINT UNITY____SIM_INTERN__ui32______RFM_T2__x_______TRMandrelSleeveUsed_____________________; /* Index:    386 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T1__x_______POR_CoilInnerDiam_______________________; /* Index:    387 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T1__x_______POR_CoilOuterDiam_______________________; /* Index:    388 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T1__x_______POR_CoilWidth___________________________; /* Index:    389 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T9__x_______POR_StripThk____________________________; /* Index:    390 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T1__x_______TR_CoilInnerDiam________________________; /* Index:    391 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T1__x_______TR_CoilOuterDiam________________________; /* Index:    392 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T1__x_______TR_CoilWidth____________________________; /* Index:    393 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T9__x_______TR_StripThk_____________________________; /* Index:    394 */

 /*
  * input configuration: 03_hilt_const_misc
  * ----------------------------------------------------------
  * date: Wednesday, March 1st 2017
  *       17:11:28h
  * ----------------------------------------------------------
  */

  LC_TD_REAL  MISC_____SIM_INTERN__real______RFM_____x_______Test_real_1_____________________________; /* Index:    395 */
  LC_TD_REAL  MISC_____SIM_INTERN__real______RFM_____x_______Test_real_2_____________________________; /* Index:    396 */

 /*
  * input configuration: 03_hilt_const_pvb
  * ----------------------------------------------------------
  * date: Wednesday, March 1st 2017
  *       17:11:28h
  * ----------------------------------------------------------
  */

  LC_TD_REAL  PVB______SIM_INTERN__x_________RFM_____x_______Dummy___________________________________; /* Index:    397 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_ID_________________________; /* Index:    398 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_ID_1_______________________; /* Index:    399 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_ID_2_______________________; /* Index:    400 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_ID_3_______________________; /* Index:    401 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_ID_4_______________________; /* Index:    402 */
  LC_TD_REAL  PVB______SIM_INTERN__real______RFM_____x_______Booking_Coil_OuterDiameter______________; /* Index:    403 */
  LC_TD_REAL  PVB______SIM_INTERN__real______RFM_____x_______Booking_Coil_InnerDiameter______________; /* Index:    404 */
  LC_TD_REAL  PVB______SIM_INTERN__real______RFM_____x_______Booking_Coil_Width______________________; /* Index:    405 */
  LC_TD_REAL  PVB______SIM_INTERN__real______RFM_____x_______Booking_Coil_Thickness__________________; /* Index:    406 */
  LC_TD_REAL  PVB______SIM_INTERN__real______RFM_____x_______Booking_Coil_Temperature________________; /* Index:    407 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_Location___________________; /* Index:    408 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_Location_1_________________; /* Index:    409 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_Location_2_________________; /* Index:    410 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_Location_3_________________; /* Index:    411 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Coil_Location_4_________________; /* Index:    412 */
  LC_TD_BOOL  PVB______SIM_INTERN__bool______RFM_____x_______Booking_Coil_BookIn_____________________; /* Index:    413 */
  LC_TD_BOOL  PVB______SIM_INTERN__bool______RFM_____x_______Booking_Coil_BookOut____________________; /* Index:    414 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_ID_______________________; /* Index:    415 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_ID_1_____________________; /* Index:    416 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_ID_2_____________________; /* Index:    417 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_ID_3_____________________; /* Index:    418 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_ID_4_____________________; /* Index:    419 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_Location_________________; /* Index:    420 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_Location_1_______________; /* Index:    421 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_Location_2_______________; /* Index:    422 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_Location_3_______________; /* Index:    423 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Pallet_Location_4_______________; /* Index:    424 */
  LC_TD_BOOL  PVB______SIM_INTERN__bool______RFM_____x_______Booking_Pallet_BookIn___________________; /* Index:    425 */
  LC_TD_BOOL  PVB______SIM_INTERN__bool______RFM_____x_______Booking_Pallet_BookOut__________________; /* Index:    426 */

 /*
  * input configuration: 03_hilt_const_tcs
  * ----------------------------------------------------------
  * date: Wednesday, March 1st 2017
  *       17:11:28h
  * ----------------------------------------------------------
  */

  LC_TD_REAL  TCS______SIM_INTERN__x_________RFM_____x_______Dummy___________________________________; /* Index:    427 */

 /*
  * input configuration: PTS
  * ----------------------------------------------------------
  * date: Wednesday, March 1st 2017
  *       17:11:28h
  * ----------------------------------------------------------
  */

  LC_TD_BOOL  PVB______AUX_________PBMS______PTS_____DPS_1___S_T_O_P__Interface______________________; /* Index:    428 */
  LC_TD_BOOL  PVB______AUX_________PBMS______PTS_____DPS_1___H_O_L_D__Interface______________________; /* Index:    429 */
  LC_TD_UDINT PLCDI____PTS_________K18PE001__BLST____000001__DPSIM__diK18PE001_BLST1_FwdSlow_________; /* Index:    430 */
  LC_TD_UDINT PLCDI____PTS_________K18PE001__BLST____000002__DPSIM__diK18PE001_BLST2_FwdStop_________; /* Index:    431 */
  LC_TD_UDINT PLCDI____PTS_________K18PE001__BLST____000003__DPSIM__diK18PE001_BLST3_BwdStop_________; /* Index:    432 */
  LC_TD_UDINT PLCDI____PTS_________K18PE001__BLST____000004__DPSIM__diK18PE001_BLST4_BwdSlow_________; /* Index:    433 */
  LC_TD_UDINT PLCDI____PTS_________K18PE001__BLSR____000001__DPSIM__diK18PE001_BLSR1_CoilDetected____; /* Index:    434 */
  LC_TD_UDINT PLCDI____PTS_________K18PE002__BLST____000001__DPSIM__diK18PE002_BLST1_FwdSlow_________; /* Index:    435 */
  LC_TD_UDINT PLCDI____PTS_________K18PE002__BLST____000002__DPSIM__diK18PE002_BLST2_FwdStop_________; /* Index:    436 */
  LC_TD_UDINT PLCDI____PTS_________K18PE002__BLST____000003__DPSIM__diK18PE002_BLST3_BwdStop_________; /* Index:    437 */
  LC_TD_UDINT PLCDI____PTS_________K18PE002__BLST____000004__DPSIM__diK18PE002_BLST4_BwdSlow_________; /* Index:    438 */
  LC_TD_UDINT PLCDI____PTS_________K18PE002__BLSR____000001__DPSIM__diK18PE002_BLSR1_CoilDetected____; /* Index:    439 */
  LC_TD_UDINT PLCDI____PTS_________K18PE003__BLST____000001__DPSIM__diK18PE003_BLST1_FwdSlow_________; /* Index:    440 */
  LC_TD_UDINT PLCDI____PTS_________K18PE003__BLST____000002__DPSIM__diK18PE003_BLST2_FwdStop_________; /* Index:    441 */
  LC_TD_UDINT PLCDI____PTS_________K18PE003__BLST____000003__DPSIM__diK18PE003_BLST3_BwdStop_________; /* Index:    442 */
  LC_TD_UDINT PLCDI____PTS_________K18PE003__BLST____000004__DPSIM__diK18PE003_BLST4_BwdSlow_________; /* Index:    443 */
  LC_TD_UDINT PLCDI____PTS_________K18PL003__SBE_____000001__DPSIM__diK18PL003_SBE1__TurnRefPos______; /* Index:    444 */
  LC_TD_UDINT PLCDI____PTS_________K18PN003__SBE_____000001__DPSIM__diK18PN003_SBE1__LckDevLckPos____; /* Index:    445 */
  LC_TD_UDINT PLCDI____PTS_________K18PN003__SBE_____000001__DPSIM__diK18PN003_SBE2__LckDevUnlckPos__; /* Index:    446 */
  LC_TD_UDINT PLCDO____PTS_________K18PN003__MKL_____000001__DPSIM__doK18PN003_MKL1__LckDevDrvLck____; /* Index:    447 */
  LC_TD_UDINT PLCDO____PTS_________K18PN003__MKL_____000001__DPSIM__doK18PN003_MKL1__LckDevDrvUnlck__; /* Index:    448 */
  LC_TD_UDINT PLCPEW___PTS_________K18PL003__BWL_____000001__DPSIM__cnK18PL003_BWL1__TurnActCnt______; /* Index:    449 */
  LC_TD_UDINT PLCDI____PTS_________K18PE004__BLST____000001__DPSIM__diK18PE004_BLST1_FwdSlow_________; /* Index:    450 */
  LC_TD_UDINT PLCDI____PTS_________K18PE004__BLST____000002__DPSIM__diK18PE004_BLST2_FwdStop_________; /* Index:    451 */
  LC_TD_UDINT PLCDI____PTS_________K18PE004__BLST____000003__DPSIM__diK18PE004_BLST3_BwdStop_________; /* Index:    452 */
  LC_TD_UDINT PLCDI____PTS_________K18PE004__BLST____000004__DPSIM__diK18PE004_BLST4_BwdSlow_________; /* Index:    453 */
  LC_TD_UDINT PLCDI____PTS_________K18PE004__BLSR____000001__DPSIM__diK18PE004_BLSR1_CoilDetected____; /* Index:    454 */
  LC_TD_UDINT PLCDI____PTS_________K18PE005__BLST____000001__DPSIM__diK18PE005_BLST1_FwdSlow_________; /* Index:    455 */
  LC_TD_UDINT PLCDI____PTS_________K18PE005__BLST____000002__DPSIM__diK18PE005_BLST2_FwdStop_________; /* Index:    456 */
  LC_TD_UDINT PLCDI____PTS_________K18PE005__BLST____000003__DPSIM__diK18PE005_BLST3_BwdStop_________; /* Index:    457 */
  LC_TD_UDINT PLCDI____PTS_________K18PE005__BLST____000004__DPSIM__diK18PE005_BLST4_BwdSlow_________; /* Index:    458 */
  LC_TD_UDINT PLCDI____PTS_________K18PE006__BLST____000001__DPSIM__diK18PE006_BLST1_FwdSlow_________; /* Index:    459 */
  LC_TD_UDINT PLCDI____PTS_________K18PE006__BLST____000002__DPSIM__diK18PE006_BLST2_FwdStop_________; /* Index:    460 */
  LC_TD_UDINT PLCDI____PTS_________K18PE006__BLST____000003__DPSIM__diK18PE006_BLST3_BwdStop_________; /* Index:    461 */
  LC_TD_UDINT PLCDI____PTS_________K18PE006__BLST____000004__DPSIM__diK18PE006_BLST4_BwdSlow_________; /* Index:    462 */
  LC_TD_UDINT PLCDI____PTS_________K18PE006__BLSR____000001__DPSIM__diK18PE006_BLSR1_CoilDetected____; /* Index:    463 */
  LC_TD_UDINT PLCDI____PTS_________K18PE008__BLST____000001__DPSIM__diK18PE008_BLST1_FwdSlow_________; /* Index:    464 */
  LC_TD_UDINT PLCDI____PTS_________K18PE008__BLST____000002__DPSIM__diK18PE008_BLST2_FwdStop_________; /* Index:    465 */
  LC_TD_UDINT PLCDI____PTS_________K18PE008__BLST____000003__DPSIM__diK18PE008_BLST3_BwdStop_________; /* Index:    466 */
  LC_TD_UDINT PLCDI____PTS_________K18PE008__BLST____000004__DPSIM__diK18PE008_BLST4_BwdSlow_________; /* Index:    467 */
  LC_TD_UDINT PLCDI____PTS_________K18PE008__BLSR____000001__DPSIM__diK18PE008_BLSR1_CoilDetected____; /* Index:    468 */
  LC_TD_UDINT PLCDI____PTS_________K18PE009__BLST____000001__DPSIM__diK18PE009_BLST1_FwdSlow_________; /* Index:    469 */
  LC_TD_UDINT PLCDI____PTS_________K18PE009__BLST____000002__DPSIM__diK18PE009_BLST2_FwdStop_________; /* Index:    470 */
  LC_TD_UDINT PLCDI____PTS_________K18PE009__BLST____000003__DPSIM__diK18PE009_BLST3_BwdStop_________; /* Index:    471 */
  LC_TD_UDINT PLCDI____PTS_________K18PE009__BLST____000004__DPSIM__diK18PE009_BLST4_BwdSlow_________; /* Index:    472 */
  LC_TD_UDINT PLCDI____PTS_________K18PE010__BLST____000001__DPSIM__diK18PE010_BLST1_FwdSlow_________; /* Index:    473 */
  LC_TD_UDINT PLCDI____PTS_________K18PE010__BLST____000002__DPSIM__diK18PE010_BLST2_FwdStop_________; /* Index:    474 */
  LC_TD_UDINT PLCDI____PTS_________K18PE010__BLST____000003__DPSIM__diK18PE010_BLST3_BwdStop_________; /* Index:    475 */
  LC_TD_UDINT PLCDI____PTS_________K18PE010__BLST____000004__DPSIM__diK18PE010_BLST4_BwdSlow_________; /* Index:    476 */
  LC_TD_UDINT PLCDI____PTS_________K18PE010__BLSR____000001__DPSIM__diK18PE010_BLSR1_CoilDetected____; /* Index:    477 */
  LC_TD_UDINT PLCDI____PTS_________K18PE011__BLST____000001__DPSIM__diK18PE011_BLST1_FwdSlow_________; /* Index:    478 */
  LC_TD_UDINT PLCDI____PTS_________K18PE011__BLST____000002__DPSIM__diK18PE011_BLST2_FwdStop_________; /* Index:    479 */
  LC_TD_UDINT PLCDI____PTS_________K18PE011__BLST____000003__DPSIM__diK18PE011_BLST3_BwdStop_________; /* Index:    480 */
  LC_TD_UDINT PLCDI____PTS_________K18PE011__BLST____000004__DPSIM__diK18PE011_BLST4_BwdSlow_________; /* Index:    481 */
  LC_TD_UDINT PLCDI____PTS_________K18PE011__BLSR____000001__DPSIM__diK18PE011_BLSR1_CoilDetected____; /* Index:    482 */
  LC_TD_UDINT PLCDI____PTS_________K18PE012__BLST____000001__DPSIM__diK18PE012_BLST1_FwdSlow_________; /* Index:    483 */
  LC_TD_UDINT PLCDI____PTS_________K18PE012__BLST____000002__DPSIM__diK18PE012_BLST2_FwdStop_________; /* Index:    484 */
  LC_TD_UDINT PLCDI____PTS_________K18PE012__BLST____000003__DPSIM__diK18PE012_BLST3_BwdStop_________; /* Index:    485 */
  LC_TD_UDINT PLCDI____PTS_________K18PE012__BLST____000004__DPSIM__diK18PE012_BLST4_BwdSlow_________; /* Index:    486 */
  LC_TD_UDINT PLCDI____PTS_________K18PE012__BLSR____000001__DPSIM__diK18PE012_BLSR1_CoilDetected____; /* Index:    487 */
  LC_TD_UDINT PLCDI____PTS_________K18PE013__BLST____000001__DPSIM__diK18PE013_BLST1_FwdSlow_________; /* Index:    488 */
  LC_TD_UDINT PLCDI____PTS_________K18PE013__BLST____000002__DPSIM__diK18PE013_BLST2_FwdStop_________; /* Index:    489 */
  LC_TD_UDINT PLCDI____PTS_________K18PE013__BLST____000003__DPSIM__diK18PE013_BLST3_BwdStop_________; /* Index:    490 */
  LC_TD_UDINT PLCDI____PTS_________K18PE013__BLST____000004__DPSIM__diK18PE013_BLST4_BwdSlow_________; /* Index:    491 */
  LC_TD_UDINT PLCDI____PTS_________K18PE014__BLST____000001__DPSIM__diK18PE014_BLST1_FwdSlow_________; /* Index:    492 */
  LC_TD_UDINT PLCDI____PTS_________K18PE014__BLST____000002__DPSIM__diK18PE014_BLST2_FwdStop_________; /* Index:    493 */
  LC_TD_UDINT PLCDI____PTS_________K18PE014__BLST____000003__DPSIM__diK18PE014_BLST3_BwdStop_________; /* Index:    494 */
  LC_TD_UDINT PLCDI____PTS_________K18PE014__BLST____000004__DPSIM__diK18PE014_BLST4_BwdSlow_________; /* Index:    495 */
  LC_TD_UDINT PLCDI____PTS_________K18PE014__BLSR____000001__DPSIM__diK18PE014_BLSR1_CoilDetected____; /* Index:    496 */
  LC_TD_UDINT PLCDI____PTS_________K18PE015__BLST____000001__DPSIM__diK18PE015_BLST1_FwdSlow_________; /* Index:    497 */
  LC_TD_UDINT PLCDI____PTS_________K18PE015__BLST____000002__DPSIM__diK18PE015_BLST2_FwdStop_________; /* Index:    498 */
  LC_TD_UDINT PLCDI____PTS_________K18PE015__BLST____000003__DPSIM__diK18PE015_BLST3_BwdStop_________; /* Index:    499 */
  LC_TD_UDINT PLCDI____PTS_________K18PE015__BLST____000004__DPSIM__diK18PE015_BLST4_BwdSlow_________; /* Index:    500 */
  LC_TD_UDINT PLCDI____PTS_________K18PL015__SBE_____000001__DPSIM__diK18PL015_SBE1__TurnRefPos______; /* Index:    501 */
  LC_TD_UDINT PLCDI____PTS_________K18PN015__SBE_____000001__DPSIM__diK18PN015_SBE1__LckDevLckPos____; /* Index:    502 */
  LC_TD_UDINT PLCDI____PTS_________K18PN015__SBE_____000001__DPSIM__diK18PN015_SBE2__LckDevUnlckPos__; /* Index:    503 */
  LC_TD_UDINT PLCDO____PTS_________K18PN015__MKL_____000001__DPSIM__doK18PN015_MKL1__LckDevDrvLck____; /* Index:    504 */
  LC_TD_UDINT PLCDO____PTS_________K18PN015__MKL_____000001__DPSIM__doK18PN015_MKL1__LckDevDrvUnlck__; /* Index:    505 */
  LC_TD_UDINT PLCPEW___PTS_________K18PL015__BWL_____000001__DPSIM__cnK18PL015_BWL1__TurnActCnt______; /* Index:    506 */
  LC_TD_UDINT PLCDI____PTS_________K18PE016__BLST____000001__DPSIM__diK18PE016_BLST1_FwdSlow_________; /* Index:    507 */
  LC_TD_UDINT PLCDI____PTS_________K18PE016__BLST____000002__DPSIM__diK18PE016_BLST2_FwdStop_________; /* Index:    508 */
  LC_TD_UDINT PLCDI____PTS_________K18PE016__BLST____000003__DPSIM__diK18PE016_BLST3_BwdStop_________; /* Index:    509 */
  LC_TD_UDINT PLCDI____PTS_________K18PE016__BLST____000004__DPSIM__diK18PE016_BLST4_BwdSlow_________; /* Index:    510 */
  LC_TD_UDINT PLCDI____PTS_________K18PE018__BLST____000001__DPSIM__diK18PE018_BLST1_FwdSlow_________; /* Index:    511 */
  LC_TD_UDINT PLCDI____PTS_________K18PE018__BLST____000002__DPSIM__diK18PE018_BLST2_FwdStop_________; /* Index:    512 */
  LC_TD_UDINT PLCDI____PTS_________K18PE018__BLST____000003__DPSIM__diK18PE018_BLST3_BwdStop_________; /* Index:    513 */
  LC_TD_UDINT PLCDI____PTS_________K18PE018__BLST____000004__DPSIM__diK18PE018_BLST4_BwdSlow_________; /* Index:    514 */
  LC_TD_UDINT PLCDI____PTS_________K18PE018__BLSR____000001__DPSIM__diK18PE018_BLSR1_CoilDetected____; /* Index:    515 */
  LC_TD_UDINT PLCDI____PTS_________K18PE019__BLST____000001__DPSIM__diK18PE019_BLST1_FwdSlow_________; /* Index:    516 */
  LC_TD_UDINT PLCDI____PTS_________K18PE019__BLST____000002__DPSIM__diK18PE019_BLST2_FwdStop_________; /* Index:    517 */
  LC_TD_UDINT PLCDI____PTS_________K18PE019__BLST____000003__DPSIM__diK18PE019_BLST3_BwdStop_________; /* Index:    518 */
  LC_TD_UDINT PLCDI____PTS_________K18PE019__BLST____000004__DPSIM__diK18PE019_BLST4_BwdSlow_________; /* Index:    519 */
  LC_TD_UDINT PLCDI____PTS_________K18PE020__BLST____000001__DPSIM__diK18PE020_BLST1_FwdSlow_________; /* Index:    520 */
  LC_TD_UDINT PLCDI____PTS_________K18PE020__BLST____000002__DPSIM__diK18PE020_BLST2_FwdStop_________; /* Index:    521 */
  LC_TD_UDINT PLCDI____PTS_________K18PE020__BLST____000003__DPSIM__diK18PE020_BLST3_BwdStop_________; /* Index:    522 */
  LC_TD_UDINT PLCDI____PTS_________K18PE020__BLST____000004__DPSIM__diK18PE020_BLST4_BwdSlow_________; /* Index:    523 */
  LC_TD_UDINT PLCDI____PTS_________K18PL020__SBE_____000001__DPSIM__diK18PL020_SBE1__TurnRefPos______; /* Index:    524 */
  LC_TD_UDINT PLCDI____PTS_________K18PN020__SBE_____000001__DPSIM__diK18PN020_SBE1__LckDevLckPos____; /* Index:    525 */
  LC_TD_UDINT PLCDI____PTS_________K18PN020__SBE_____000001__DPSIM__diK18PN020_SBE2__LckDevUnlckPos__; /* Index:    526 */
  LC_TD_UDINT PLCDO____PTS_________K18PN020__MKL_____000001__DPSIM__doK18PN020_MKL1__LckDevDrvLck____; /* Index:    527 */
  LC_TD_UDINT PLCDO____PTS_________K18PN020__MKL_____000001__DPSIM__doK18PN020_MKL1__LckDevDrvUnlck__; /* Index:    528 */
  LC_TD_UDINT PLCPEW___PTS_________K18PL020__BWL_____000001__DPSIM__cnK18PL020_BWL1__TurnActCnt______; /* Index:    529 */
  LC_TD_UDINT PLCDI____PTS_________K18PE021__BLST____000001__DPSIM__diK18PE021_BLST1_FwdSlow_________; /* Index:    530 */
  LC_TD_UDINT PLCDI____PTS_________K18PE021__BLST____000002__DPSIM__diK18PE021_BLST2_FwdStop_________; /* Index:    531 */
  LC_TD_UDINT PLCDI____PTS_________K18PE021__BLST____000003__DPSIM__diK18PE021_BLST3_BwdStop_________; /* Index:    532 */
  LC_TD_UDINT PLCDI____PTS_________K18PE021__BLST____000004__DPSIM__diK18PE021_BLST4_BwdSlow_________; /* Index:    533 */
  LC_TD_UDINT PLCDI____PTS_________K18PE021__BLSR____000001__DPSIM__diK18PE021_BLSR1_CoilDetected____; /* Index:    534 */
  LC_TD_UDINT PLCDI____PTS_________K18PE022__BLST____000001__DPSIM__diK18PE022_BLST1_FwdSlow_________; /* Index:    535 */
  LC_TD_UDINT PLCDI____PTS_________K18PE022__BLST____000002__DPSIM__diK18PE022_BLST2_FwdStop_________; /* Index:    536 */
  LC_TD_UDINT PLCDI____PTS_________K18PE022__BLST____000003__DPSIM__diK18PE022_BLST3_BwdStop_________; /* Index:    537 */
  LC_TD_UDINT PLCDI____PTS_________K18PE022__BLST____000004__DPSIM__diK18PE022_BLST4_BwdSlow_________; /* Index:    538 */
  LC_TD_UDINT PLCDI____PTS_________K18PE023__BLST____000001__DPSIM__diK18PE023_BLST1_FwdSlow_________; /* Index:    539 */
  LC_TD_UDINT PLCDI____PTS_________K18PE023__BLST____000002__DPSIM__diK18PE023_BLST2_FwdStop_________; /* Index:    540 */
  LC_TD_UDINT PLCDI____PTS_________K18PE023__BLST____000003__DPSIM__diK18PE023_BLST3_BwdStop_________; /* Index:    541 */
  LC_TD_UDINT PLCDI____PTS_________K18PE023__BLST____000004__DPSIM__diK18PE023_BLST4_BwdSlow_________; /* Index:    542 */
  LC_TD_UDINT PLCDI____PTS_________K18PE023__BLSR____000001__DPSIM__diK18PE023_BLSR1_CoilDetected____; /* Index:    543 */
  LC_TD_UDINT PLCDI____PTS_________K18PE025__BLST____000001__DPSIM__diK18PE025_BLST1_FwdSlow_________; /* Index:    544 */
  LC_TD_UDINT PLCDI____PTS_________K18PE025__BLST____000002__DPSIM__diK18PE025_BLST2_FwdStop_________; /* Index:    545 */
  LC_TD_UDINT PLCDI____PTS_________K18PE025__BLST____000003__DPSIM__diK18PE025_BLST3_BwdStop_________; /* Index:    546 */
  LC_TD_UDINT PLCDI____PTS_________K18PE025__BLST____000004__DPSIM__diK18PE025_BLST4_BwdSlow_________; /* Index:    547 */
  LC_TD_UDINT PLCDI____PTS_________K18PL025__SBE_____000001__DPSIM__diK18PL025_SBE1__TurnRefPos______; /* Index:    548 */
  LC_TD_UDINT PLCDI____PTS_________K18PN025__SBE_____000001__DPSIM__diK18PN025_SBE1__LckDevLckPos____; /* Index:    549 */
  LC_TD_UDINT PLCDI____PTS_________K18PN025__SBE_____000001__DPSIM__diK18PN025_SBE2__LckDevUnlckPos__; /* Index:    550 */
  LC_TD_UDINT PLCDO____PTS_________K18PN025__MKL_____000001__DPSIM__doK18PN025_MKL1__LckDevDrvLck____; /* Index:    551 */
  LC_TD_UDINT PLCDO____PTS_________K18PN025__MKL_____000001__DPSIM__doK18PN025_MKL1__LckDevDrvUnlck__; /* Index:    552 */
  LC_TD_UDINT PLCPEW___PTS_________K18PL025__BWL_____000001__DPSIM__cnK18PL025_BWL1__TurnActCnt______; /* Index:    553 */
  LC_TD_UDINT PLCDI____PTS_________K18PE026__BLST____000001__DPSIM__diK18PE026_BLST1_FwdSlow_________; /* Index:    554 */
  LC_TD_UDINT PLCDI____PTS_________K18PE026__BLST____000002__DPSIM__diK18PE026_BLST2_FwdStop_________; /* Index:    555 */
  LC_TD_UDINT PLCDI____PTS_________K18PE026__BLST____000003__DPSIM__diK18PE026_BLST3_BwdStop_________; /* Index:    556 */
  LC_TD_UDINT PLCDI____PTS_________K18PE026__BLST____000004__DPSIM__diK18PE026_BLST4_BwdSlow_________; /* Index:    557 */
  LC_TD_UDINT PLCDI____PTS_________K18PE027__BLST____000001__DPSIM__diK18PE027_BLST1_FwdSlow_________; /* Index:    558 */
  LC_TD_UDINT PLCDI____PTS_________K18PE027__BLST____000002__DPSIM__diK18PE027_BLST2_FwdStop_________; /* Index:    559 */
  LC_TD_UDINT PLCDI____PTS_________K18PE027__BLST____000003__DPSIM__diK18PE027_BLST3_BwdStop_________; /* Index:    560 */
  LC_TD_UDINT PLCDI____PTS_________K18PE027__BLST____000004__DPSIM__diK18PE027_BLST4_BwdSlow_________; /* Index:    561 */
  LC_TD_UDINT PLCDI____PTS_________K18PE028__BLST____000001__DPSIM__diK18PE028_BLST1_FwdSlow_________; /* Index:    562 */
  LC_TD_UDINT PLCDI____PTS_________K18PE028__BLST____000002__DPSIM__diK18PE028_BLST2_FwdStop_________; /* Index:    563 */
  LC_TD_UDINT PLCDI____PTS_________K18PE028__BLST____000003__DPSIM__diK18PE028_BLST3_BwdStop_________; /* Index:    564 */
  LC_TD_UDINT PLCDI____PTS_________K18PE028__BLST____000004__DPSIM__diK18PE028_BLST4_BwdSlow_________; /* Index:    565 */
  LC_TD_UDINT PLCDI____PTS_________K18PE028__BLSR____000001__DPSIM__diK18PE028_BLSR1_CoilDetected____; /* Index:    566 */
  LC_TD_UDINT PLCDI____PTS_________K18PE029__BLST____000001__DPSIM__diK18PE029_BLST1_FwdSlow_________; /* Index:    567 */
  LC_TD_UDINT PLCDI____PTS_________K18PE029__BLST____000002__DPSIM__diK18PE029_BLST2_FwdStop_________; /* Index:    568 */
  LC_TD_UDINT PLCDI____PTS_________K18PE029__BLST____000003__DPSIM__diK18PE029_BLST3_BwdStop_________; /* Index:    569 */
  LC_TD_UDINT PLCDI____PTS_________K18PE029__BLST____000004__DPSIM__diK18PE029_BLST4_BwdSlow_________; /* Index:    570 */
  LC_TD_UDINT PLCDI____PTS_________K18PE029__BLSR____000001__DPSIM__diK18PE029_BLSR1_CoilDetected____; /* Index:    571 */
  LC_TD_UDINT PLCDI____PTS_________K18PE030__BLST____000001__DPSIM__diK18PE030_BLST1_FwdSlow_________; /* Index:    572 */
  LC_TD_UDINT PLCDI____PTS_________K18PE030__BLST____000002__DPSIM__diK18PE030_BLST2_FwdStop_________; /* Index:    573 */
  LC_TD_UDINT PLCDI____PTS_________K18PE030__BLST____000003__DPSIM__diK18PE030_BLST3_BwdStop_________; /* Index:    574 */
  LC_TD_UDINT PLCDI____PTS_________K18PE030__BLST____000004__DPSIM__diK18PE030_BLST4_BwdSlow_________; /* Index:    575 */
  LC_TD_UDINT PLCDI____PTS_________K18PE031__BLST____000001__DPSIM__diK18PE031_BLST1_FwdSlow_________; /* Index:    576 */
  LC_TD_UDINT PLCDI____PTS_________K18PE031__BLST____000002__DPSIM__diK18PE031_BLST2_FwdStop_________; /* Index:    577 */
  LC_TD_UDINT PLCDI____PTS_________K18PE031__BLST____000003__DPSIM__diK18PE031_BLST3_BwdStop_________; /* Index:    578 */
  LC_TD_UDINT PLCDI____PTS_________K18PE031__BLST____000004__DPSIM__diK18PE031_BLST4_BwdSlow_________; /* Index:    579 */
  LC_TD_UDINT PLCDI____PTS_________K18PE033__BLST____000001__DPSIM__diK18PE033_BLST1_FwdSlow_________; /* Index:    580 */
  LC_TD_UDINT PLCDI____PTS_________K18PE033__BLST____000002__DPSIM__diK18PE033_BLST2_FwdStop_________; /* Index:    581 */
  LC_TD_UDINT PLCDI____PTS_________K18PE033__BLST____000003__DPSIM__diK18PE033_BLST3_BwdStop_________; /* Index:    582 */
  LC_TD_UDINT PLCDI____PTS_________K18PE033__BLST____000004__DPSIM__diK18PE033_BLST4_BwdSlow_________; /* Index:    583 */
  LC_TD_UDINT PLCDI____PTS_________K18PU010__SBE_____000001__DPSIM__diK18PU010_SBE1__TravBwdStop_____; /* Index:    584 */
  LC_TD_UDINT PLCDI____PTS_________K18PU010__SBE_____000002__DPSIM__diK18PU010_SBE2__TravBwdSlow_____; /* Index:    585 */
  LC_TD_UDINT PLCDI____PTS_________K18PU010__SBE_____000003__DPSIM__diK18PU010_SBE3__TravFwdSlow_____; /* Index:    586 */
  LC_TD_UDINT PLCDI____PTS_________K18PU010__SBE_____000004__DPSIM__diK18PU010_SBE4__TravFwdStop_____; /* Index:    587 */
  LC_TD_UDINT PLCDI____PTS_________K18PE034__BLST____000001__DPSIM__diK18PE034_BLST1_FwdSlow_________; /* Index:    588 */
  LC_TD_UDINT PLCDI____PTS_________K18PE034__BLST____000002__DPSIM__diK18PE034_BLST2_FwdStop_________; /* Index:    589 */
  LC_TD_UDINT PLCDI____PTS_________K18PE034__BLST____000003__DPSIM__diK18PE034_BLST3_BwdStop_________; /* Index:    590 */
  LC_TD_UDINT PLCDI____PTS_________K18PE034__BLST____000004__DPSIM__diK18PE034_BLST4_BwdSlow_________; /* Index:    591 */
  LC_TD_UDINT PLCDI____PTS_________K18PU024__SBE_____000001__DPSIM__diK18PU024_SBE1__TravBwdStop_____; /* Index:    592 */
  LC_TD_UDINT PLCDI____PTS_________K18PU024__SBE_____000002__DPSIM__diK18PU024_SBE2__TravBwdSlow_____; /* Index:    593 */
  LC_TD_UDINT PLCDI____PTS_________K18PU024__SBE_____000003__DPSIM__diK18PU024_SBE3__TravFwdSlow_____; /* Index:    594 */
  LC_TD_UDINT PLCDI____PTS_________K18PU024__SBE_____000004__DPSIM__diK18PU024_SBE4__TravFwdStop_____; /* Index:    595 */
  LC_TD_UDINT PLCDI____PTS_________K11BE060__SBE_____000001__DPSIM__diK11BE060_SBE1__PalDetected_____; /* Index:    596 */
  LC_TD_UDINT PLCDI____PTS_________K11BE020__FD______000001__DPSIM__diK11BE020_FD1___LiftHighPrOn____; /* Index:    597 */
  LC_TD_UDINT PLCDI____PTS_________K11BE010__SBE_____000001__DPSIM__diK11BE010_SBE1__TravRefPos______; /* Index:    598 */
  LC_TD_UDINT PLCDO____PTS_________K11BE030__YVH_____000001__DPSIM__doK11BE030_YVH1__LiftVlvLowPrOn__; /* Index:    599 */
  LC_TD_UDINT PLCDO____PTS_________K11BE020__YVH_____000001__DPSIM__doK11BE020_YVH1__LiftChkVlvDn____; /* Index:    600 */
  LC_TD_UDINT PLCDO____PTS_________K11BE020__YVH_____000001__DPSIM__doK11BE020_YVH1__LiftChkVlvUp____; /* Index:    601 */
  LC_TD_REAL  PLCAO____PTS_________K11BE020__YVHP____000001__DPSIM__aoK11BE020_YVHP1_LiftVlvUpDown___; /* Index:    602 */
  LC_TD_UDINT PLCPEW___PTS_________K11BE010__BWL_____000001__DPSIM__cnK11BE010_BWL1__TravActCnt______; /* Index:    603 */
  LC_TD_UDINT PLCPEW___PTS_________K11BE020__BWL_____000001__DPSIM__cnK11BE020_BWL1__LiftActCnt______; /* Index:    604 */
  LC_TD_UDINT PLCDI____PTS_________K13BE060__SBE_____000001__DPSIM__diK13BE060_SBE1__PalDetected_____; /* Index:    605 */
  LC_TD_UDINT PLCDI____PTS_________K13BE020__FD______000001__DPSIM__diK13BE020_FD1___LiftHighPrOn____; /* Index:    606 */
  LC_TD_UDINT PLCDI____PTS_________K13BE010__SBE_____000001__DPSIM__diK13BE010_SBE1__TravRefPos______; /* Index:    607 */
  LC_TD_UDINT PLCDO____PTS_________K13BE030__YVH_____000001__DPSIM__doK13BE030_YVH1__LiftVlvLowPrOn__; /* Index:    608 */
  LC_TD_UDINT PLCDO____PTS_________K13BE020__YVH_____000001__DPSIM__doK13BE020_YVH1__LiftChkVlvDn____; /* Index:    609 */
  LC_TD_UDINT PLCDO____PTS_________K13BE020__YVH_____000001__DPSIM__doK13BE020_YVH1__LiftChkVlvUp____; /* Index:    610 */
  LC_TD_REAL  PLCAO____PTS_________K13BE020__YVHP____000001__DPSIM__aoK13BE020_YVHP1_LiftVlvUpDown___; /* Index:    611 */
  LC_TD_UDINT PLCPEW___PTS_________K13BE010__BWL_____000001__DPSIM__cnK13BE010_BWL1__TravActCnt______; /* Index:    612 */
  LC_TD_UDINT PLCPEW___PTS_________K13BE020__BWL_____000001__DPSIM__cnK13BE020_BWL1__LiftActCnt______; /* Index:    613 */
  LC_TD_UDINT PLCDI____PTS_________K15BE060__SBE_____000001__DPSIM__diK15BE060_SBE1__PalDetected_____; /* Index:    614 */
  LC_TD_UDINT PLCDI____PTS_________K15BE020__FD______000001__DPSIM__diK15BE020_FD1___LiftHighPrOn____; /* Index:    615 */
  LC_TD_UDINT PLCDI____PTS_________K15BE010__SBE_____000001__DPSIM__diK15BE010_SBE1__TravRefPos______; /* Index:    616 */
  LC_TD_UDINT PLCDO____PTS_________K15BE030__YVH_____000001__DPSIM__doK15BE030_YVH1__LiftVlvLowPrOn__; /* Index:    617 */
  LC_TD_UDINT PLCDO____PTS_________K15BE020__YVH_____000001__DPSIM__doK15BE020_YVH1__LiftChkVlvDn____; /* Index:    618 */
  LC_TD_UDINT PLCDO____PTS_________K15BE020__YVH_____000001__DPSIM__doK15BE020_YVH1__LiftChkVlvUp____; /* Index:    619 */
  LC_TD_REAL  PLCAO____PTS_________K15BE020__YVHP____000001__DPSIM__aoK15BE020_YVHP1_LiftVlvUpDown___; /* Index:    620 */
  LC_TD_UDINT PLCPEW___PTS_________K15BE010__BWL_____000001__DPSIM__cnK15BE010_BWL1__TravActCnt______; /* Index:    621 */
  LC_TD_UDINT PLCPEW___PTS_________K15BE020__BWL_____000001__DPSIM__cnK15BE020_BWL1__LiftActCnt______; /* Index:    622 */
  LC_TD_UDINT PLCDI____PTS_________H10ND040__SBE_____000001__DPSIM__diH10ND040_SBE1__MndExp__________; /* Index:    623 */
  LC_TD_UDINT PLCDI____PTS_________H10ND040__SBE_____000002__DPSIM__diH10ND040_SBE2__MndClp__________; /* Index:    624 */
  LC_TD_UDINT PLCDO____PTS_________H10ND040__YVH_____000001__DPSIM__doH10ND040_YVH1__MndVlvClp_______; /* Index:    625 */
  LC_TD_UDINT PLCDO____PTS_________H10ND040__YVH_____000001__DPSIM__doH10ND040_YVH1__MndVlvExp_______; /* Index:    626 */
  LC_TD_UDINT PLCDO____PTS_________H10ND050__YVH_____000001__DPSIM__doH10ND050_YVH1__MndVlvPrChgOver_; /* Index:    627 */
  LC_TD_REAL  PLCAI____PTS_________H10ND040__BD______000001__DPSIM__aiH10ND040_BD1___MndExpActPr_____; /* Index:    628 */
  LC_TD_REAL  PLCAI____PTS_________H10ND040__BD______000002__DPSIM__aiH10ND040_BD2___MndClpActPr_____; /* Index:    629 */
  LC_TD_UDINT PLCDI____PTS_________F01ND040__SBE_____000001__DPSIM__diF01ND040_SBE1__MndExp__________; /* Index:    630 */
  LC_TD_UDINT PLCDI____PTS_________F01ND040__SBE_____000002__DPSIM__diF01ND040_SBE2__MndClp__________; /* Index:    631 */
  LC_TD_UDINT PLCDO____PTS_________F01ND040__YVH_____000001__DPSIM__doF01ND040_YVH1__MndVlvClp_______; /* Index:    632 */
  LC_TD_UDINT PLCDO____PTS_________F01ND040__YVH_____000001__DPSIM__doF01ND040_YVH1__MndVlvExp_______; /* Index:    633 */
  LC_TD_UDINT PLCDO____PTS_________F01ND050__YVH_____000001__DPSIM__doF01ND050_YVH1__MndVlvPrChgOver_; /* Index:    634 */
  LC_TD_REAL  PLCAI____PTS_________F01ND040__BD______000001__DPSIM__aiF01ND040_BD1___MndExpActPr_____; /* Index:    635 */
  LC_TD_REAL  PLCAI____PTS_________F01ND040__BD______000002__DPSIM__aiF01ND040_BD2___MndClpActPr_____; /* Index:    636 */
  LC_TD_BOOL  PVB______AUX_________PBMS______PTS_____DPS_2___S_T_O_P__Interface______________________; /* Index:    637 */
  LC_TD_BOOL  PVB______AUX_________PBMS______PTS_____DPS_2___H_O_L_D__Interface______________________; /* Index:    638 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE001__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    639 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE001__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    640 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE001__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    641 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE001__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    642 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE002__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    643 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE002__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    644 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE002__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    645 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE002__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    646 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE003__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    647 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE003__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    648 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE003__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    649 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE003__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    650 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE004__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    651 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE004__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    652 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE004__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    653 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE004__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    654 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE005__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    655 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE005__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    656 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE005__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    657 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE005__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    658 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE006__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    659 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE006__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    660 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE006__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    661 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE006__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    662 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE006__x_______000002__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    663 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE006__x_______000002__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    664 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE006__x_______000002__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    665 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE006__x_______000002__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    666 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE008__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    667 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE008__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    668 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE008__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    669 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE008__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    670 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE009__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    671 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE009__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    672 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE009__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    673 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE009__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    674 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE010__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    675 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE010__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    676 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE010__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    677 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE010__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    678 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE011__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    679 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE011__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    680 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE011__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    681 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE011__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    682 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE012__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    683 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE012__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    684 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE012__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    685 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE012__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    686 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE013__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    687 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE013__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    688 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE013__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    689 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE013__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    690 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE014__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    691 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE014__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    692 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE014__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    693 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE014__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    694 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE015__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    695 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE015__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    696 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE015__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    697 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE015__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    698 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE016__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    699 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE016__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    700 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE016__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    701 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE016__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    702 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE018__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    703 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE018__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    704 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE018__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    705 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE018__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    706 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE019__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    707 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE019__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    708 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE019__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    709 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE019__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    710 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE020__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    711 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE020__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    712 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE020__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    713 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE020__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    714 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE021__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    715 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE021__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    716 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE021__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    717 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE021__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    718 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE022__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    719 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE022__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    720 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE022__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    721 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE022__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    722 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE023__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    723 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE023__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    724 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE023__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    725 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE023__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    726 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE023__x_______000002__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    727 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE023__x_______000002__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    728 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE023__x_______000002__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    729 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE023__x_______000002__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    730 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE025__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    731 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE025__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    732 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE025__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    733 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE025__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    734 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE026__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    735 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE026__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    736 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE026__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    737 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE026__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    738 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE027__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    739 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE027__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    740 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE027__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    741 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE027__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    742 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE028__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    743 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE028__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    744 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE028__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    745 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE028__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    746 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE029__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    747 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE029__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    748 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE029__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    749 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE029__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    750 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE030__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    751 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE030__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    752 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE030__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    753 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE030__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    754 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE031__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    755 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE031__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    756 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE031__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    757 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE031__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    758 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE033__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    759 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE033__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    760 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE033__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    761 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE033__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    762 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE034__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    763 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE034__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    764 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE034__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    765 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE034__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    766 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE029__x_______000002__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    767 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE029__x_______000002__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    768 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE029__x_______000002__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    769 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE029__x_______000002__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    770 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE014__x_______000002__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    771 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE014__x_______000002__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    772 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE014__x_______000002__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    773 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE014__x_______000002__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    774 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE012__x_______000002__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    775 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PE012__x_______000002__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    776 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE012__x_______000002__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    777 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PE012__x_______000002__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    778 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PL003__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    779 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PL003__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    780 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PL003__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    781 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PL003__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    782 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PL015__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    783 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PL015__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    784 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PL015__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    785 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PL015__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    786 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PL020__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    787 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PL020__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    788 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PL020__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    789 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PL020__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    790 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PL025__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    791 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PL025__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    792 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PL025__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    793 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PL025__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    794 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PU010__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    795 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PU010__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    796 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PU010__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    797 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PU010__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    798 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PU024__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    799 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PU024__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    800 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PU024__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    801 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PU024__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    802 */
  LC_TD_UDINT PLCDRVI__PTS_________B11BB010__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    803 */
  LC_TD_UDINT PLCDRVI__PTS_________B11BB010__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    804 */
  LC_TD_UDINT PLCDRVO__PTS_________B11BB010__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    805 */
  LC_TD_UDINT PLCDRVO__PTS_________B11BB010__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    806 */
  LC_TD_UDINT PLCDRVI__PTS_________B41BB010__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    807 */
  LC_TD_UDINT PLCDRVI__PTS_________B41BB010__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    808 */
  LC_TD_UDINT PLCDRVO__PTS_________B41BB010__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    809 */
  LC_TD_UDINT PLCDRVO__PTS_________B41BB010__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    810 */
  LC_TD_UDINT PLCDRVI__PTS_________K11BE010__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    811 */
  LC_TD_UDINT PLCDRVI__PTS_________K11BE010__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    812 */
  LC_TD_UDINT PLCDRVO__PTS_________K11BE010__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    813 */
  LC_TD_UDINT PLCDRVO__PTS_________K11BE010__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    814 */
  LC_TD_UDINT PLCDRVI__PTS_________K13BE010__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    815 */
  LC_TD_UDINT PLCDRVI__PTS_________K13BE010__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    816 */
  LC_TD_UDINT PLCDRVO__PTS_________K13BE010__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    817 */
  LC_TD_UDINT PLCDRVO__PTS_________K13BE010__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    818 */
  LC_TD_UDINT PLCDRVI__PTS_________K15BE010__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    819 */
  LC_TD_UDINT PLCDRVI__PTS_________K15BE010__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    820 */
  LC_TD_UDINT PLCDRVO__PTS_________K15BE010__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    821 */
  LC_TD_UDINT PLCDRVO__PTS_________K15BE010__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    822 */
  LC_TD_UDINT PLCDRVI__PTS_________B71BB010__x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:    823 */
  LC_TD_UDINT PLCDRVI__PTS_________B71BB010__x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:    824 */
  LC_TD_UDINT PLCDRVO__PTS_________B71BB010__x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:    825 */
  LC_TD_UDINT PLCDRVO__PTS_________B71BB010__x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:    826 */


 /*
  * kalk: file patched
  * ----------------------------------------------------------
  * date: Wednesday, March 1st 2017
  *       17:11:35h
  * ----------------------------------------------------------
  */ 

  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T1_AccActRoll___________________________; /* Index:    827 */
  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T4_AlphaActRoll_________________________; /* Index:    828 */
  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T4_AlphaActDrv__________________________; /* Index:    829 */
  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T1_LngthActRoll_________________________; /* Index:    830 */
  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T4_OmgActDrv____________________________; /* Index:    831 */
  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T4_OmgActRoll___________________________; /* Index:    832 */
  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T4_PhiActDrv____________________________; /* Index:    833 */
  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T4_PhiActRoll___________________________; /* Index:    834 */
  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T1_SpdActRoll___________________________; /* Index:    835 */
  LC_TD_REAL  MTLBSIG__PTS_________B11BB010__x_______000001__T1_TrqActRoll___________________________; /* Index:    836 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T1_AccActRoll___________________________; /* Index:    837 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T4_AlphaActRoll_________________________; /* Index:    838 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T4_AlphaActDrv__________________________; /* Index:    839 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T1_LngthActRoll_________________________; /* Index:    840 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T4_OmgActDrv____________________________; /* Index:    841 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T4_OmgActRoll___________________________; /* Index:    842 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T4_PhiActDrv____________________________; /* Index:    843 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T4_PhiActRoll___________________________; /* Index:    844 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T1_SpdActRoll___________________________; /* Index:    845 */
  LC_TD_REAL  MTLBSIG__PTS_________B41BB010__x_______000001__T1_TrqActRoll___________________________; /* Index:    846 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T1_AccActRoll___________________________; /* Index:    847 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T4_AlphaActRoll_________________________; /* Index:    848 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T4_AlphaActDrv__________________________; /* Index:    849 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T1_LngthActRoll_________________________; /* Index:    850 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T4_OmgActDrv____________________________; /* Index:    851 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T4_OmgActRoll___________________________; /* Index:    852 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T4_PhiActDrv____________________________; /* Index:    853 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T4_PhiActRoll___________________________; /* Index:    854 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T1_SpdActRoll___________________________; /* Index:    855 */
  LC_TD_REAL  MTLBSIG__PTS_________B71BB010__x_______000001__T1_TrqActRoll___________________________; /* Index:    856 */
  LC_TD_REAL  MTLBSIG__PTS_________K11BE010__x_______000001__T1_LngthActRoll_________________________; /* Index:    857 */
  LC_TD_REAL  MTLBSIG__PTS_________K11BE010__x_______000001__T4_OmgActDrv____________________________; /* Index:    858 */
  LC_TD_REAL  MTLBSIG__PTS_________K11BE010__x_______000001__T4_OmgActRoll___________________________; /* Index:    859 */
  LC_TD_REAL  MTLBSIG__PTS_________K11BE010__x_______000001__T4_PhiActRoll___________________________; /* Index:    860 */
  LC_TD_REAL  MTLBSIG__PTS_________K11BE010__x_______000001__T1_SpdActRoll___________________________; /* Index:    861 */
  LC_TD_REAL  MTLBSIG__PTS_________K13BE010__x_______000001__T1_LngthActRoll_________________________; /* Index:    862 */
  LC_TD_REAL  MTLBSIG__PTS_________K13BE010__x_______000001__T4_OmgActDrv____________________________; /* Index:    863 */
  LC_TD_REAL  MTLBSIG__PTS_________K13BE010__x_______000001__T4_OmgActRoll___________________________; /* Index:    864 */
  LC_TD_REAL  MTLBSIG__PTS_________K13BE010__x_______000001__T4_PhiActRoll___________________________; /* Index:    865 */
  LC_TD_REAL  MTLBSIG__PTS_________K13BE010__x_______000001__T1_SpdActRoll___________________________; /* Index:    866 */
  LC_TD_REAL  MTLBSIG__PTS_________K15BE010__x_______000001__T1_LngthActRoll_________________________; /* Index:    867 */
  LC_TD_REAL  MTLBSIG__PTS_________K15BE010__x_______000001__T4_OmgActDrv____________________________; /* Index:    868 */
  LC_TD_REAL  MTLBSIG__PTS_________K15BE010__x_______000001__T4_OmgActRoll___________________________; /* Index:    869 */
  LC_TD_REAL  MTLBSIG__PTS_________K15BE010__x_______000001__T4_PhiActRoll___________________________; /* Index:    870 */
  LC_TD_REAL  MTLBSIG__PTS_________K15BE010__x_______000001__T1_SpdActRoll___________________________; /* Index:    871 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE001__x_______000001__T1_LngthActRoll_________________________; /* Index:    872 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE001__x_______000001__T4_OmgActDrv____________________________; /* Index:    873 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE001__x_______000001__T4_OmgActRoll___________________________; /* Index:    874 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE001__x_______000001__T4_PhiActRoll___________________________; /* Index:    875 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE001__x_______000001__T1_SpdActRoll___________________________; /* Index:    876 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE002__x_______000001__T1_LngthActRoll_________________________; /* Index:    877 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE002__x_______000001__T4_OmgActDrv____________________________; /* Index:    878 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE002__x_______000001__T4_OmgActRoll___________________________; /* Index:    879 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE002__x_______000001__T4_PhiActRoll___________________________; /* Index:    880 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE002__x_______000001__T1_SpdActRoll___________________________; /* Index:    881 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE003__x_______000001__T1_LngthActRoll_________________________; /* Index:    882 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE003__x_______000001__T4_OmgActDrv____________________________; /* Index:    883 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE003__x_______000001__T4_OmgActRoll___________________________; /* Index:    884 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE003__x_______000001__T4_PhiActRoll___________________________; /* Index:    885 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE003__x_______000001__T1_SpdActRoll___________________________; /* Index:    886 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE004__x_______000001__T1_LngthActRoll_________________________; /* Index:    887 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE004__x_______000001__T4_OmgActDrv____________________________; /* Index:    888 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE004__x_______000001__T4_OmgActRoll___________________________; /* Index:    889 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE004__x_______000001__T4_PhiActRoll___________________________; /* Index:    890 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE004__x_______000001__T1_SpdActRoll___________________________; /* Index:    891 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE005__x_______000001__T1_LngthActRoll_________________________; /* Index:    892 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE005__x_______000001__T4_OmgActDrv____________________________; /* Index:    893 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE005__x_______000001__T4_OmgActRoll___________________________; /* Index:    894 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE005__x_______000001__T4_PhiActRoll___________________________; /* Index:    895 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE005__x_______000001__T1_SpdActRoll___________________________; /* Index:    896 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000001__T1_LngthActRoll_________________________; /* Index:    897 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000001__T4_OmgActDrv____________________________; /* Index:    898 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000001__T4_OmgActRoll___________________________; /* Index:    899 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000001__T4_PhiActRoll___________________________; /* Index:    900 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000001__T1_SpdActRoll___________________________; /* Index:    901 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000002__T1_LngthActRoll_________________________; /* Index:    902 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000002__T4_OmgActDrv____________________________; /* Index:    903 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000002__T4_OmgActRoll___________________________; /* Index:    904 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000002__T4_PhiActRoll___________________________; /* Index:    905 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE006__x_______000002__T1_SpdActRoll___________________________; /* Index:    906 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE008__x_______000001__T1_LngthActRoll_________________________; /* Index:    907 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE008__x_______000001__T4_OmgActDrv____________________________; /* Index:    908 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE008__x_______000001__T4_OmgActRoll___________________________; /* Index:    909 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE008__x_______000001__T4_PhiActRoll___________________________; /* Index:    910 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE008__x_______000001__T1_SpdActRoll___________________________; /* Index:    911 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE009__x_______000001__T1_LngthActRoll_________________________; /* Index:    912 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE009__x_______000001__T4_OmgActDrv____________________________; /* Index:    913 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE009__x_______000001__T4_OmgActRoll___________________________; /* Index:    914 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE009__x_______000001__T4_PhiActRoll___________________________; /* Index:    915 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE009__x_______000001__T1_SpdActRoll___________________________; /* Index:    916 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE010__x_______000001__T1_LngthActRoll_________________________; /* Index:    917 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE010__x_______000001__T4_OmgActDrv____________________________; /* Index:    918 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE010__x_______000001__T4_OmgActRoll___________________________; /* Index:    919 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE010__x_______000001__T4_PhiActRoll___________________________; /* Index:    920 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE010__x_______000001__T1_SpdActRoll___________________________; /* Index:    921 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE011__x_______000001__T1_LngthActRoll_________________________; /* Index:    922 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE011__x_______000001__T4_OmgActDrv____________________________; /* Index:    923 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE011__x_______000001__T4_OmgActRoll___________________________; /* Index:    924 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE011__x_______000001__T4_PhiActRoll___________________________; /* Index:    925 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE011__x_______000001__T1_SpdActRoll___________________________; /* Index:    926 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000001__T1_LngthActRoll_________________________; /* Index:    927 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000001__T4_OmgActDrv____________________________; /* Index:    928 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000001__T4_OmgActRoll___________________________; /* Index:    929 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000001__T4_PhiActRoll___________________________; /* Index:    930 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000001__T1_SpdActRoll___________________________; /* Index:    931 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000002__T1_LngthActRoll_________________________; /* Index:    932 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000002__T4_OmgActDrv____________________________; /* Index:    933 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000002__T4_OmgActRoll___________________________; /* Index:    934 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000002__T4_PhiActRoll___________________________; /* Index:    935 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE012__x_______000002__T1_SpdActRoll___________________________; /* Index:    936 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE013__x_______000001__T1_LngthActRoll_________________________; /* Index:    937 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE013__x_______000001__T4_OmgActDrv____________________________; /* Index:    938 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE013__x_______000001__T4_OmgActRoll___________________________; /* Index:    939 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE013__x_______000001__T4_PhiActRoll___________________________; /* Index:    940 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE013__x_______000001__T1_SpdActRoll___________________________; /* Index:    941 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000001__T1_LngthActRoll_________________________; /* Index:    942 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000001__T4_OmgActDrv____________________________; /* Index:    943 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000001__T4_OmgActRoll___________________________; /* Index:    944 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000001__T4_PhiActRoll___________________________; /* Index:    945 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000001__T1_SpdActRoll___________________________; /* Index:    946 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000002__T1_LngthActRoll_________________________; /* Index:    947 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000002__T4_OmgActDrv____________________________; /* Index:    948 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000002__T4_OmgActRoll___________________________; /* Index:    949 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000002__T4_PhiActRoll___________________________; /* Index:    950 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE014__x_______000002__T1_SpdActRoll___________________________; /* Index:    951 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE015__x_______000001__T1_LngthActRoll_________________________; /* Index:    952 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE015__x_______000001__T4_OmgActDrv____________________________; /* Index:    953 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE015__x_______000001__T4_OmgActRoll___________________________; /* Index:    954 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE015__x_______000001__T4_PhiActRoll___________________________; /* Index:    955 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE015__x_______000001__T1_SpdActRoll___________________________; /* Index:    956 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE016__x_______000001__T1_LngthActRoll_________________________; /* Index:    957 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE016__x_______000001__T4_OmgActDrv____________________________; /* Index:    958 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE016__x_______000001__T4_OmgActRoll___________________________; /* Index:    959 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE016__x_______000001__T4_PhiActRoll___________________________; /* Index:    960 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE016__x_______000001__T1_SpdActRoll___________________________; /* Index:    961 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE018__x_______000001__T1_LngthActRoll_________________________; /* Index:    962 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE018__x_______000001__T4_OmgActDrv____________________________; /* Index:    963 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE018__x_______000001__T4_OmgActRoll___________________________; /* Index:    964 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE018__x_______000001__T4_PhiActRoll___________________________; /* Index:    965 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE018__x_______000001__T1_SpdActRoll___________________________; /* Index:    966 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE019__x_______000001__T1_LngthActRoll_________________________; /* Index:    967 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE019__x_______000001__T4_OmgActDrv____________________________; /* Index:    968 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE019__x_______000001__T4_OmgActRoll___________________________; /* Index:    969 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE019__x_______000001__T4_PhiActRoll___________________________; /* Index:    970 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE019__x_______000001__T1_SpdActRoll___________________________; /* Index:    971 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE020__x_______000001__T1_LngthActRoll_________________________; /* Index:    972 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE020__x_______000001__T4_OmgActDrv____________________________; /* Index:    973 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE020__x_______000001__T4_OmgActRoll___________________________; /* Index:    974 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE020__x_______000001__T4_PhiActRoll___________________________; /* Index:    975 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE020__x_______000001__T1_SpdActRoll___________________________; /* Index:    976 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE021__x_______000001__T1_LngthActRoll_________________________; /* Index:    977 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE021__x_______000001__T4_OmgActDrv____________________________; /* Index:    978 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE021__x_______000001__T4_OmgActRoll___________________________; /* Index:    979 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE021__x_______000001__T4_PhiActRoll___________________________; /* Index:    980 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE021__x_______000001__T1_SpdActRoll___________________________; /* Index:    981 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE022__x_______000001__T1_LngthActRoll_________________________; /* Index:    982 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE022__x_______000001__T4_OmgActDrv____________________________; /* Index:    983 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE022__x_______000001__T4_OmgActRoll___________________________; /* Index:    984 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE022__x_______000001__T4_PhiActRoll___________________________; /* Index:    985 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE022__x_______000001__T1_SpdActRoll___________________________; /* Index:    986 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000001__T1_LngthActRoll_________________________; /* Index:    987 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000001__T4_OmgActDrv____________________________; /* Index:    988 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000001__T4_OmgActRoll___________________________; /* Index:    989 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000001__T4_PhiActRoll___________________________; /* Index:    990 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000001__T1_SpdActRoll___________________________; /* Index:    991 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000002__T1_LngthActRoll_________________________; /* Index:    992 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000002__T4_OmgActDrv____________________________; /* Index:    993 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000002__T4_OmgActRoll___________________________; /* Index:    994 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000002__T4_PhiActRoll___________________________; /* Index:    995 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE023__x_______000002__T1_SpdActRoll___________________________; /* Index:    996 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE025__x_______000001__T1_LngthActRoll_________________________; /* Index:    997 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE025__x_______000001__T4_OmgActDrv____________________________; /* Index:    998 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE025__x_______000001__T4_OmgActRoll___________________________; /* Index:    999 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE025__x_______000001__T4_PhiActRoll___________________________; /* Index:   1000 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE025__x_______000001__T1_SpdActRoll___________________________; /* Index:   1001 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE026__x_______000001__T1_LngthActRoll_________________________; /* Index:   1002 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE026__x_______000001__T4_OmgActDrv____________________________; /* Index:   1003 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE026__x_______000001__T4_OmgActRoll___________________________; /* Index:   1004 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE026__x_______000001__T4_PhiActRoll___________________________; /* Index:   1005 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE026__x_______000001__T1_SpdActRoll___________________________; /* Index:   1006 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE027__x_______000001__T1_LngthActRoll_________________________; /* Index:   1007 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE027__x_______000001__T4_OmgActDrv____________________________; /* Index:   1008 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE027__x_______000001__T4_OmgActRoll___________________________; /* Index:   1009 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE027__x_______000001__T4_PhiActRoll___________________________; /* Index:   1010 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE027__x_______000001__T1_SpdActRoll___________________________; /* Index:   1011 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE028__x_______000001__T1_LngthActRoll_________________________; /* Index:   1012 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE028__x_______000001__T4_OmgActDrv____________________________; /* Index:   1013 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE028__x_______000001__T4_OmgActRoll___________________________; /* Index:   1014 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE028__x_______000001__T4_PhiActRoll___________________________; /* Index:   1015 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE028__x_______000001__T1_SpdActRoll___________________________; /* Index:   1016 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000001__T1_LngthActRoll_________________________; /* Index:   1017 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000001__T4_OmgActDrv____________________________; /* Index:   1018 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000001__T4_OmgActRoll___________________________; /* Index:   1019 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000001__T4_PhiActRoll___________________________; /* Index:   1020 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000001__T1_SpdActRoll___________________________; /* Index:   1021 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000002__T1_LngthActRoll_________________________; /* Index:   1022 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000002__T4_OmgActDrv____________________________; /* Index:   1023 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000002__T4_OmgActRoll___________________________; /* Index:   1024 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000002__T4_PhiActRoll___________________________; /* Index:   1025 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE029__x_______000002__T1_SpdActRoll___________________________; /* Index:   1026 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE030__x_______000001__T1_LngthActRoll_________________________; /* Index:   1027 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE030__x_______000001__T4_OmgActDrv____________________________; /* Index:   1028 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE030__x_______000001__T4_OmgActRoll___________________________; /* Index:   1029 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE030__x_______000001__T4_PhiActRoll___________________________; /* Index:   1030 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE030__x_______000001__T1_SpdActRoll___________________________; /* Index:   1031 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE031__x_______000001__T1_LngthActRoll_________________________; /* Index:   1032 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE031__x_______000001__T4_OmgActDrv____________________________; /* Index:   1033 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE031__x_______000001__T4_OmgActRoll___________________________; /* Index:   1034 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE031__x_______000001__T4_PhiActRoll___________________________; /* Index:   1035 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE031__x_______000001__T1_SpdActRoll___________________________; /* Index:   1036 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE033__x_______000001__T1_LngthActRoll_________________________; /* Index:   1037 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE033__x_______000001__T4_OmgActDrv____________________________; /* Index:   1038 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE033__x_______000001__T4_OmgActRoll___________________________; /* Index:   1039 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE033__x_______000001__T4_PhiActRoll___________________________; /* Index:   1040 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE033__x_______000001__T1_SpdActRoll___________________________; /* Index:   1041 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE034__x_______000001__T1_LngthActRoll_________________________; /* Index:   1042 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE034__x_______000001__T4_OmgActDrv____________________________; /* Index:   1043 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE034__x_______000001__T4_OmgActRoll___________________________; /* Index:   1044 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE034__x_______000001__T4_PhiActRoll___________________________; /* Index:   1045 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PE034__x_______000001__T1_SpdActRoll___________________________; /* Index:   1046 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL003__x_______000001__T1_LngthActRoll_________________________; /* Index:   1047 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL003__x_______000001__T4_OmgActDrv____________________________; /* Index:   1048 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL003__x_______000001__T4_OmgActRoll___________________________; /* Index:   1049 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL003__x_______000001__T4_PhiActRoll___________________________; /* Index:   1050 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL003__x_______000001__T1_SpdActRoll___________________________; /* Index:   1051 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL015__x_______000001__T1_LngthActRoll_________________________; /* Index:   1052 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL015__x_______000001__T4_OmgActDrv____________________________; /* Index:   1053 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL015__x_______000001__T4_OmgActRoll___________________________; /* Index:   1054 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL015__x_______000001__T4_PhiActRoll___________________________; /* Index:   1055 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL015__x_______000001__T1_SpdActRoll___________________________; /* Index:   1056 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL020__x_______000001__T1_LngthActRoll_________________________; /* Index:   1057 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL020__x_______000001__T4_OmgActDrv____________________________; /* Index:   1058 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL020__x_______000001__T4_OmgActRoll___________________________; /* Index:   1059 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL020__x_______000001__T4_PhiActRoll___________________________; /* Index:   1060 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL020__x_______000001__T1_SpdActRoll___________________________; /* Index:   1061 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL025__x_______000001__T1_LngthActRoll_________________________; /* Index:   1062 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL025__x_______000001__T4_OmgActDrv____________________________; /* Index:   1063 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL025__x_______000001__T4_OmgActRoll___________________________; /* Index:   1064 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL025__x_______000001__T4_PhiActRoll___________________________; /* Index:   1065 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PL025__x_______000001__T1_SpdActRoll___________________________; /* Index:   1066 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1067 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1068 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1069 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1070 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1071 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU024__x_______000001__T1_LngthActRoll_________________________; /* Index:   1072 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU024__x_______000001__T4_OmgActDrv____________________________; /* Index:   1073 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU024__x_______000001__T4_OmgActRoll___________________________; /* Index:   1074 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU024__x_______000001__T4_PhiActRoll___________________________; /* Index:   1075 */
  LC_TD_REAL  MTLBSIG__PTS_________K18PU024__x_______000001__T1_SpdActRoll___________________________; /* Index:   1076 */
 /*
  * kalk: file patched
  * ----------------------------------------------------------
  * date: Friday, April 7th 2017
  *       10:05:29h
  * ----------------------------------------------------------
  */ 

 /*
  * kalk: file patched
  * ----------------------------------------------------------
  * date: Friday, April 7th 2017
  *       10:06:37h
  * ----------------------------------------------------------
  */ 

 /*
  * tler: file patched
  * ----------------------------------------------------------
  * date: Monday, July 3rd 2017
  *       10:10:33h
  * ----------------------------------------------------------
  */ 

  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______Furnace_BurnerOn________________________; /* Index:   1077 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______Furnace_RefTemperature__________________; /* Index:   1078 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______Furnace_ActTemperature__________________; /* Index:   1079 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______Furnace_EntryDoorOpen___________________; /* Index:   1080 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______Furnace_EntryDoorClose__________________; /* Index:   1081 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______Furnace_EntryDoorOpnPos_________________; /* Index:   1082 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______Furnace_EntryDoorClsPos_________________; /* Index:   1083 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______Furnace_ExitDoorOpen____________________; /* Index:   1084 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______Furnace_ExitDoorClose___________________; /* Index:   1085 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______Furnace_ExitDoorOpnPos__________________; /* Index:   1086 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______Furnace_ExitDoorClsPos__________________; /* Index:   1087 */
 /*
  * tler: file patched
  * ----------------------------------------------------------
  * date: Monday, October 2nd 2017
  *       14:33:20h
  * ----------------------------------------------------------
  */ 

  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______KebaPanel_EmergencyStopCH1______________; /* Index:   1088 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______KebaPanel_EmergencyStopCH2______________; /* Index:   1089 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______KebaPanel_AcknowledgeCH1________________; /* Index:   1090 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______KebaPanel_AcknowledgeCH2________________; /* Index:   1091 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______KebaPanel_KeySwitchCH1__________________; /* Index:   1092 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______KebaPanel_KeySwitchCH2__________________; /* Index:   1093 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______KebaPanel_KeySwitchCH3__________________; /* Index:   1094 */
 /*
  * tler: file patched
  * ----------------------------------------------------------
  * date: Wednesday, November 22nd 2017
  *       13:20:43h
  * ----------------------------------------------------------
  */ 

  LC_TD_UDINT PLCDRVI__PTS_________K18PU10___x_______000001__DPSIM__DRV_T1_PZD1R_ui16________________; /* Index:   1095 */
  LC_TD_UDINT PLCDRVI__PTS_________K18PU10___x_______000001__DPSIM__DRV_T2_PZD2R_ui16________________; /* Index:   1096 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PU10___x_______000001__DPSIM__DRV_T1_PZD1S_ui16________________; /* Index:   1097 */
  LC_TD_UDINT PLCDRVO__PTS_________K18PU10___x_______000001__DPSIM__DRV_T2_PZD2S_ui16________________; /* Index:   1098 */
 /*
  * tler: file patched
  * ----------------------------------------------------------
  * date: Wednesday, November 22nd 2017
  *       15:34:59h
  * ----------------------------------------------------------
  */ 

 /*
  * tler: file patched
  * ----------------------------------------------------------
  * date: Wednesday, November 22nd 2017
  *       15:54:28h
  * ----------------------------------------------------------
  */ 


 /*
  * morm: file patched
  * ----------------------------------------------------------
  * date: Wednesday, January 24th 2018
  *       11:41:32h
  * ----------------------------------------------------------
  */ 

  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K13VE005_YVL1_Expand_Diam_Measurement___; /* Index:   1099 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K13VE005_SBE1_Diam_Measurement_Retracted; /* Index:   1100 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K13VE005_SBE2_Diam_Measurement_Expanded_; /* Index:   1101 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K13VE010_BLSR1_Diam_Measurement_________; /* Index:   1102 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K13VE020_BLSR1_Width_Measurement________; /* Index:   1103 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__Mandril_Expand_Release__________________; /* Index:   1104 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__RT_Inner_Release________________________; /* Index:   1105 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__RT_Outer_Release________________________; /* Index:   1106 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__CC_In_Mandril_Area______________________; /* Index:   1107 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__CC_In_Decoil_Pos________________________; /* Index:   1108 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__CC_Lift_Lowest_Pos______________________; /* Index:   1109 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__CC_Lift_Low_Pos_________________________; /* Index:   1110 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__CC_Lift_Palett__________________________; /* Index:   1111 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__Release_To_Entry_Manipulator____________; /* Index:   1112 */
  LC_TD_BOOL  PLCDO____SIM_INTERN__bool______K13_____FRMK13__Diam_Measuring_System_Retracted_________; /* Index:   1113 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___Mandril_Expanded________________________; /* Index:   1114 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___Mandril_Collapsed_______________________; /* Index:   1115 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___RT_Inner_PutDown_Release________________; /* Index:   1116 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___RT_Outer_PutDown_Release________________; /* Index:   1117 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___RT_Inner_PickUp_Release_________________; /* Index:   1118 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___RT_Outer_PickUp_Release_________________; /* Index:   1119 */
  LC_TD_REAL  PLCDI____SIM_INTERN__real______K13_____TOK13___POR_speed_______________________________; /* Index:   1120 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___POR_PresRol_Engaged_____________________; /* Index:   1121 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___POR_TelescopicTable_Engaged_____________; /* Index:   1122 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___POR_OuterBearing_Engaged________________; /* Index:   1123 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___Mandril_Expand_Cmd______________________; /* Index:   1124 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___Mandril_Collapse_Cmd____________________; /* Index:   1125 */
  LC_TD_BOOL  PLCDI____SIM_INTERN__bool______K13_____TOK13___Release_From_Entry_Manipulator__________; /* Index:   1126 */

 /*
  * jpju: file patched
  * ----------------------------------------------------------
  * date: Tuesday, April 3rd 2018
  *       10:23:58h
  * ----------------------------------------------------------
  */ 

  LC_TD_UDINT PLCDI____PTS_________K18PU010__SBE_____000005__DPSIM__diK18PU010_SBE5__TravNewSlow_____; /* Index:   1127 */
  LC_TD_UDINT PLCDI____PTS_________K18PU010__SBE_____000006__DPSIM__diK18PU010_SBE6__TravNewStop_____; /* Index:   1128 */
  LC_TD_UDINT PLCDI____PTS_________K18PU024__SBE_____000005__DPSIM__diK18PU024_SBE5__TravNewSlow_____; /* Index:   1129 */
  LC_TD_UDINT PLCDI____PTS_________K18PU024__SBE_____000006__DPSIM__diK18PU024_SBE6__TravNewStop_____; /* Index:   1130 */
 /*
  * TLER: file patched
  * ----------------------------------------------------------
  * date: Tuesday, May 15th 2018
  *       10:17:18h
  * ----------------------------------------------------------
  */ 

 /*
  * TLER: file patched
  * ----------------------------------------------------------
  * date: Tuesday, May 15th 2018
  *       14:48:37h
  * ----------------------------------------------------------
  */ 

  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E30_0____________________________; /* Index:   1131 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E30_1____________________________; /* Index:   1132 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E30_2____________________________; /* Index:   1133 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E30_3____________________________; /* Index:   1134 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E31_0____________________________; /* Index:   1135 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E31_1____________________________; /* Index:   1136 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E31_2____________________________; /* Index:   1137 */
  LC_TD_UDINT PLCDO____PTS_________XX00______XXX_____000000__DPSIM__A31_1____________________________; /* Index:   1138 */
  LC_TD_UDINT PLCDO____PTS_________XX00______XXX_____000000__DPSIM__A31_2____________________________; /* Index:   1139 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E40_0____________________________; /* Index:   1140 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E40_1____________________________; /* Index:   1141 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E40_2____________________________; /* Index:   1142 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E40_3____________________________; /* Index:   1143 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E40_4____________________________; /* Index:   1144 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E50_0____________________________; /* Index:   1145 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E50_1____________________________; /* Index:   1146 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E50_2____________________________; /* Index:   1147 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E50_3____________________________; /* Index:   1148 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E60_0____________________________; /* Index:   1149 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E60_1____________________________; /* Index:   1150 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E60_2____________________________; /* Index:   1151 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E60_3____________________________; /* Index:   1152 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E60_4____________________________; /* Index:   1153 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E80_0____________________________; /* Index:   1154 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E80_1____________________________; /* Index:   1155 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E80_2____________________________; /* Index:   1156 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E80_3____________________________; /* Index:   1157 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E80_4____________________________; /* Index:   1158 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E90_0____________________________; /* Index:   1159 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E90_1____________________________; /* Index:   1160 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E90_2____________________________; /* Index:   1161 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E90_3____________________________; /* Index:   1162 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E100_0___________________________; /* Index:   1163 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E100_1___________________________; /* Index:   1164 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E100_2___________________________; /* Index:   1165 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E100_3___________________________; /* Index:   1166 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E100_4___________________________; /* Index:   1167 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E110_0___________________________; /* Index:   1168 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E110_1___________________________; /* Index:   1169 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E110_2___________________________; /* Index:   1170 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E110_3___________________________; /* Index:   1171 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E110_4___________________________; /* Index:   1172 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E120_0___________________________; /* Index:   1173 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E120_1___________________________; /* Index:   1174 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E120_2___________________________; /* Index:   1175 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E120_3___________________________; /* Index:   1176 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E120_4___________________________; /* Index:   1177 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E130_0___________________________; /* Index:   1178 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E130_1___________________________; /* Index:   1179 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E130_2___________________________; /* Index:   1180 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E130_3___________________________; /* Index:   1181 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E140_0___________________________; /* Index:   1182 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E140_1___________________________; /* Index:   1183 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E140_2___________________________; /* Index:   1184 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E140_3___________________________; /* Index:   1185 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E140_4___________________________; /* Index:   1186 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E150_0___________________________; /* Index:   1187 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E150_1___________________________; /* Index:   1188 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E150_2___________________________; /* Index:   1189 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E150_3___________________________; /* Index:   1190 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E151_0___________________________; /* Index:   1191 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E151_1___________________________; /* Index:   1192 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E151_2___________________________; /* Index:   1193 */
  LC_TD_UDINT PLCDO____PTS_________XX00______XXX_____000000__DPSIM__A151_1___________________________; /* Index:   1194 */
  LC_TD_UDINT PLCDO____PTS_________XX00______XXX_____000000__DPSIM__A151_2___________________________; /* Index:   1195 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E160_0___________________________; /* Index:   1196 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E160_1___________________________; /* Index:   1197 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E160_2___________________________; /* Index:   1198 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E160_3___________________________; /* Index:   1199 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E180_0___________________________; /* Index:   1200 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E180_1___________________________; /* Index:   1201 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E180_2___________________________; /* Index:   1202 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E180_3___________________________; /* Index:   1203 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E180_4___________________________; /* Index:   1204 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E190_0___________________________; /* Index:   1205 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E190_1___________________________; /* Index:   1206 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E190_2___________________________; /* Index:   1207 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E190_3___________________________; /* Index:   1208 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E200_0___________________________; /* Index:   1209 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E200_1___________________________; /* Index:   1210 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E200_2___________________________; /* Index:   1211 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E200_3___________________________; /* Index:   1212 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E201_0___________________________; /* Index:   1213 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E201_1___________________________; /* Index:   1214 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E201_2___________________________; /* Index:   1215 */
  LC_TD_UDINT PLCDO____PTS_________XX00______XXX_____000000__DPSIM__A201_1___________________________; /* Index:   1216 */
  LC_TD_UDINT PLCDO____PTS_________XX00______XXX_____000000__DPSIM__A201_2___________________________; /* Index:   1217 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E210_0___________________________; /* Index:   1218 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E210_1___________________________; /* Index:   1219 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E210_2___________________________; /* Index:   1220 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E210_3___________________________; /* Index:   1221 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E210_4___________________________; /* Index:   1222 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E220_0___________________________; /* Index:   1223 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E220_1___________________________; /* Index:   1224 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E220_2___________________________; /* Index:   1225 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E220_3___________________________; /* Index:   1226 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E230_0___________________________; /* Index:   1227 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E230_1___________________________; /* Index:   1228 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E230_2___________________________; /* Index:   1229 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E230_3___________________________; /* Index:   1230 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E230_4___________________________; /* Index:   1231 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E250_0___________________________; /* Index:   1232 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E250_1___________________________; /* Index:   1233 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E250_2___________________________; /* Index:   1234 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E250_3___________________________; /* Index:   1235 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E251_0___________________________; /* Index:   1236 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E251_1___________________________; /* Index:   1237 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E251_2___________________________; /* Index:   1238 */
  LC_TD_UDINT PLCDO____PTS_________XX00______XXX_____000000__DPSIM__A251_1___________________________; /* Index:   1239 */
  LC_TD_UDINT PLCDO____PTS_________XX00______XXX_____000000__DPSIM__A251_2___________________________; /* Index:   1240 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E260_0___________________________; /* Index:   1241 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E260_1___________________________; /* Index:   1242 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E260_2___________________________; /* Index:   1243 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E260_3___________________________; /* Index:   1244 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E270_0___________________________; /* Index:   1245 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E270_1___________________________; /* Index:   1246 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E270_2___________________________; /* Index:   1247 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E270_3___________________________; /* Index:   1248 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E280_0___________________________; /* Index:   1249 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E280_1___________________________; /* Index:   1250 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E280_2___________________________; /* Index:   1251 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E280_3___________________________; /* Index:   1252 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E280_4___________________________; /* Index:   1253 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E290_0___________________________; /* Index:   1254 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E290_1___________________________; /* Index:   1255 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E290_2___________________________; /* Index:   1256 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E290_3___________________________; /* Index:   1257 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E290_4___________________________; /* Index:   1258 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E300_0___________________________; /* Index:   1259 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E300_1___________________________; /* Index:   1260 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E300_2___________________________; /* Index:   1261 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E300_3___________________________; /* Index:   1262 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E310_0___________________________; /* Index:   1263 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E310_1___________________________; /* Index:   1264 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E310_2___________________________; /* Index:   1265 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E310_3___________________________; /* Index:   1266 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E330_0___________________________; /* Index:   1267 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E330_1___________________________; /* Index:   1268 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E330_2___________________________; /* Index:   1269 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E330_3___________________________; /* Index:   1270 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E331_0___________________________; /* Index:   1271 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E331_1___________________________; /* Index:   1272 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E331_2___________________________; /* Index:   1273 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E331_3___________________________; /* Index:   1274 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E340_0___________________________; /* Index:   1275 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E340_1___________________________; /* Index:   1276 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E340_2___________________________; /* Index:   1277 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E340_3___________________________; /* Index:   1278 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E341_0___________________________; /* Index:   1279 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E341_1___________________________; /* Index:   1280 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E341_2___________________________; /* Index:   1281 */
  LC_TD_UDINT PLCDI____PTS_________XX00______XXX_____000000__DPSIM__E341_3___________________________; /* Index:   1282 */
 /*
  * TLER: file patched
  * ----------------------------------------------------------
  * date: Tuesday, May 15th 2018
  *       14:49:15h
  * ----------------------------------------------------------
  */ 

 /*
  * TLER: file patched
  * ----------------------------------------------------------
  * date: Tuesday, May 15th 2018
  *       14:58:24h
  * ----------------------------------------------------------
  */ 

 /*
  * jpju: file patched
  * ----------------------------------------------------------
  * date: Wednesday, July 25th 2018
  *       07:43:51h
  * ----------------------------------------------------------
  */ 

  LC_TD_REAL  RFMEXCG__SIM_INTERN__real______x_______x_______LC_vg_RfmTransfActCycleTime1msCycle_____; /* Index:   1283 */
  LC_TD_REAL  RFMEXCG__SIM_INTERN__real______x_______x_______LC_vg_RfmTransfActCycleTime10msCycle____; /* Index:   1284 */
  LC_TD_REAL  RFMEXCG__SIM_INTERN__real______x_______x_______LC_vg_RfmTransfActCycleTime100msCycle___; /* Index:   1285 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_ID_______________________; /* Index:   1286 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_ID_1_____________________; /* Index:   1287 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_ID_2_____________________; /* Index:   1288 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_ID_3_____________________; /* Index:   1289 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_ID_4_____________________; /* Index:   1290 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_Location_________________; /* Index:   1291 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_Location_1_______________; /* Index:   1292 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_Location_2_______________; /* Index:   1293 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_Location_3_______________; /* Index:   1294 */
  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_Location_4_______________; /* Index:   1295 */
  LC_TD_REAL  PVB______SIM_INTERN__real______RFM_____x_______Booking_Sleeve_OuterDiameter____________; /* Index:   1296 */
  LC_TD_REAL  PVB______SIM_INTERN__real______RFM_____x_______Booking_Sleeve_InnerDiameter____________; /* Index:   1297 */
  LC_TD_REAL  PVB______SIM_INTERN__real______RFM_____x_______Booking_Sleeve_Width____________________; /* Index:   1298 */
  LC_TD_BOOL  PVB______SIM_INTERN__bool______RFM_____x_______Booking_Sleeve_BookIn___________________; /* Index:   1299 */
  LC_TD_BOOL  PVB______SIM_INTERN__bool______RFM_____x_______Booking_Sleeve_BookOut__________________; /* Index:   1300 */


 /*
  * morm: file patched
  * ----------------------------------------------------------
  * date: Wednesday, August 22nd 2018
  *       08:38:57h
  * ----------------------------------------------------------
  */ 

  LC_TD_REAL  MTLBSIG__PTS_________K20GD010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1301 */
  LC_TD_REAL  MTLBSIG__PTS_________K20GD010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1302 */
  LC_TD_REAL  MTLBSIG__PTS_________K20GD010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1303 */
  LC_TD_REAL  MTLBSIG__PTS_________K20GD010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1304 */
  LC_TD_REAL  MTLBSIG__PTS_________K20GD010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1305 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RT010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1306 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RT010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1307 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RT010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1308 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RT010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1309 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RT010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1310 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RX010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1311 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RX010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1312 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RX010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1313 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RX010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1314 */
  LC_TD_REAL  MTLBSIG__PTS_________K20RX010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1315 */
  LC_TD_REAL  MTLBSIG__PTS_________K30GD010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1316 */
  LC_TD_REAL  MTLBSIG__PTS_________K30GD010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1317 */
  LC_TD_REAL  MTLBSIG__PTS_________K30GD010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1318 */
  LC_TD_REAL  MTLBSIG__PTS_________K30GD010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1319 */
  LC_TD_REAL  MTLBSIG__PTS_________K30GD010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1320 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RF010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1321 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RF010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1322 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RF010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1323 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RF010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1324 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RF010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1325 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RG010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1326 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RG010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1327 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RG010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1328 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RG010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1329 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RG010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1330 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1331 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1332 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1333 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1334 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1335 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ020__x_______000001__T1_LngthActRoll_________________________; /* Index:   1336 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ020__x_______000001__T4_OmgActDrv____________________________; /* Index:   1337 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ020__x_______000001__T4_OmgActRoll___________________________; /* Index:   1338 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ020__x_______000001__T4_PhiActRoll___________________________; /* Index:   1339 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ020__x_______000001__T1_SpdActRoll___________________________; /* Index:   1340 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ030__x_______000001__T1_LngthActRoll_________________________; /* Index:   1341 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ030__x_______000001__T4_OmgActDrv____________________________; /* Index:   1342 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ030__x_______000001__T4_OmgActRoll___________________________; /* Index:   1343 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ030__x_______000001__T4_PhiActRoll___________________________; /* Index:   1344 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RQ030__x_______000001__T1_SpdActRoll___________________________; /* Index:   1345 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RT010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1346 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RT010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1347 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RT010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1348 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RT010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1349 */
  LC_TD_REAL  MTLBSIG__PTS_________K30RT010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1350 */

 /*
  * morm: file patched
  * ----------------------------------------------------------
  * date: Thursday, August 30th 2018
  *       13:55:40h
  * ----------------------------------------------------------
  */ 

  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______K20_GD010_SpManES_TravActPos____________; /* Index:   1351 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______ESManArmSwvl_ActCylStroke_______________; /* Index:   1352 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______ESManArmSwvl_ActTurnAngle_______________; /* Index:   1353 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______ESManArmSwvl_CylExpanded________________; /* Index:   1354 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______ESManArmSwvl_CylRetracted_______________; /* Index:   1355 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______ESManTongHeadSwvl_ActCylStroke__________; /* Index:   1356 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______ESManTongHeadSwvl_ActTurnAngle__________; /* Index:   1357 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______ESManTongHeadSwvl_CylExpanded___________; /* Index:   1358 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______ESManTongHeadSwvl_CylRetracted__________; /* Index:   1359 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______ESManTong_ActCylStroke__________________; /* Index:   1360 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______ESManTong_ActTurnAngle__________________; /* Index:   1361 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______ESManTong_CylExpanded___________________; /* Index:   1362 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______ESManTong_CylRetracted__________________; /* Index:   1363 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______K30_GD010_SpManXS_TravActPos____________; /* Index:   1364 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______XSManArmSwvl_ActCylStroke_______________; /* Index:   1365 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______XSManArmSwvl_ActTurnAngle_______________; /* Index:   1366 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______XSManArmSwvl_CylExpanded________________; /* Index:   1367 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______XSManArmSwvl_CylRetracted_______________; /* Index:   1368 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______XSManTongHeadSwvl_ActCylStroke__________; /* Index:   1369 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______XSManTongHeadSwvl_ActTurnAngle__________; /* Index:   1370 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______XSManTongHeadSwvl_CylExpanded___________; /* Index:   1371 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______XSManTongHeadSwvl_CylRetracted__________; /* Index:   1372 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______XSManTong_ActCylStroke__________________; /* Index:   1373 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T5__x_______XSManTong_ActTurnAngle__________________; /* Index:   1374 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______XSManTong_CylExpanded___________________; /* Index:   1375 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______XSManTong_CylRetracted__________________; /* Index:   1376 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______TenSec_POR_TO_TR_ActTenAbsFr____________; /* Index:   1377 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______TenSec_POR_TO_TR_ActDltLngth____________; /* Index:   1378 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______TenSec_POR_TO_TR_ActDltSpd______________; /* Index:   1379 */
  LC_TD_UDINT MTLBSIG__SIM_INTERN__ui32______RFM_T5__x_______TenSec_POR_TO_TR_Active_________________; /* Index:   1380 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T1__x_______K20GD040_EsManTong_CollisionDetected____; /* Index:   1381 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T1__x_______K30GD040_XsManTong_CollisionDetected____; /* Index:   1382 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K20RT010_BLSR1_InSpoolPckUpPos__________; /* Index:   1383 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K20RX010_BLSR1_InSpoolPckUpPos__________; /* Index:   1384 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K30RF010_BLSR1_InSpoolPckUpPos__________; /* Index:   1385 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K30RG010_BLSR1_InSpoolPckUpPos__________; /* Index:   1386 */
  LC_TD_BOOL  UNITY____SIM_INTERN__bool______RFM_T5__x_______K30RT010_BLSR1_InSpoolPckUpPos__________; /* Index:   1387 */
  LC_TD_UDINT PLCDRVI__PTS_________K20GD010__x_______000001__DPSIM__SpManES_DRV_T1_PZD1R_ui16________; /* Index:   1388 */
  LC_TD_UDINT PLCDRVI__PTS_________K20GD010__x_______000001__DPSIM__SpManES_DRV_T2_PZD2R_ui16________; /* Index:   1389 */
  LC_TD_UDINT PLCDRVO__PTS_________K20GD010__x_______000001__DPSIM__SpManES_DRV_T1_PZD1S_ui16________; /* Index:   1390 */
  LC_TD_UDINT PLCDRVO__PTS_________K20GD010__x_______000001__DPSIM__SpManES_DRV_T2_PZD2S_ui16________; /* Index:   1391 */
  LC_TD_UDINT PLCDRVI__PTS_________K20RT010__x_______000001__DPSIM__SpChES_DRV_T1_PZD1R_ui16_________; /* Index:   1392 */
  LC_TD_UDINT PLCDRVI__PTS_________K20RT010__x_______000001__DPSIM__SpChES_DRV_T2_PZD2R_ui16_________; /* Index:   1393 */
  LC_TD_UDINT PLCDRVO__PTS_________K20RT010__x_______000001__DPSIM__SpChES_DRV_T1_PZD1S_ui16_________; /* Index:   1394 */
  LC_TD_UDINT PLCDRVO__PTS_________K20RT010__x_______000001__DPSIM__SpChES_DRV_T2_PZD2S_ui16_________; /* Index:   1395 */
  LC_TD_UDINT PLCDRVI__PTS_________K20RX010__x_______000001__DPSIM__SpChXS_DRV_T1_PZD1R_ui16_________; /* Index:   1396 */
  LC_TD_UDINT PLCDRVI__PTS_________K20RX010__x_______000001__DPSIM__SpChXS_DRV_T2_PZD2R_ui16_________; /* Index:   1397 */
  LC_TD_UDINT PLCDRVO__PTS_________K20RX010__x_______000001__DPSIM__SpChXS_DRV_T1_PZD1S_ui16_________; /* Index:   1398 */
  LC_TD_UDINT PLCDRVO__PTS_________K20RX010__x_______000001__DPSIM__SpChXS_DRV_T2_PZD2S_ui16_________; /* Index:   1399 */
  LC_TD_UDINT PLCDRVI__PTS_________K30GD010__x_______000001__DPSIM__SpManXS_DRV_T1_PZD1R_ui16________; /* Index:   1400 */
  LC_TD_UDINT PLCDRVI__PTS_________K30GD010__x_______000001__DPSIM__SpManXS_DRV_T2_PZD2R_ui16________; /* Index:   1401 */
  LC_TD_UDINT PLCDRVO__PTS_________K30GD010__x_______000001__DPSIM__SpManXS_DRV_T1_PZD1S_ui16________; /* Index:   1402 */
  LC_TD_UDINT PLCDRVO__PTS_________K30GD010__x_______000001__DPSIM__SpManXS_DRV_T2_PZD2S_ui16________; /* Index:   1403 */
  LC_TD_UDINT PLCDRVI__PTS_________K30RF010__x_______000001__DPSIM__SpCh1_DRV_T1_PZD1R_ui16__________; /* Index:   1404 */
  LC_TD_UDINT PLCDRVI__PTS_________K30RF010__x_______000001__DPSIM__SpCh1_DRV_T2_PZD2R_ui16__________; /* Index:   1405 */
  LC_TD_UDINT PLCDRVO__PTS_________K30RF010__x_______000001__DPSIM__SpCh1_DRV_T1_PZD1S_ui16__________; /* Index:   1406 */
  LC_TD_UDINT PLCDRVO__PTS_________K30RF010__x_______000001__DPSIM__SpCh1_DRV_T2_PZD2S_ui16__________; /* Index:   1407 */
  LC_TD_UDINT PLCDRVI__PTS_________K30RG010__x_______000001__DPSIM__SpCh2_DRV_T1_PZD1R_ui16__________; /* Index:   1408 */
  LC_TD_UDINT PLCDRVI__PTS_________K30RG010__x_______000001__DPSIM__SpCh2_DRV_T2_PZD2R_ui16__________; /* Index:   1409 */
  LC_TD_UDINT PLCDRVO__PTS_________K30RG010__x_______000001__DPSIM__SpCh2_DRV_T1_PZD1S_ui16__________; /* Index:   1410 */
  LC_TD_UDINT PLCDRVO__PTS_________K30RG010__x_______000001__DPSIM__SpCh2_DRV_T2_PZD2S_ui16__________; /* Index:   1411 */
  LC_TD_UDINT PLCDRVI__PTS_________K30RT010__x_______000001__DPSIM__SpChEsToXs_DRV_T1_PZD1R_ui16_____; /* Index:   1412 */
  LC_TD_UDINT PLCDRVI__PTS_________K30RT010__x_______000001__DPSIM__SpChEsToXs_DRV_T2_PZD2R_ui16_____; /* Index:   1413 */
  LC_TD_UDINT PLCDRVO__PTS_________K30RT010__x_______000001__DPSIM__SpChEsToXs_DRV_T1_PZD1S_ui16_____; /* Index:   1414 */
  LC_TD_UDINT PLCDRVO__PTS_________K30RT010__x_______000001__DPSIM__SpChEsToXs_DRV_T2_PZD2S_ui16_____; /* Index:   1415 */
  LC_TD_UDINT PLCDRVI__PTS_________B11BB010__x_______000001__DPSIM__DRV_T1_PZD3R_ui16________________; /* Index:   1416 */
  LC_TD_UDINT PLCDRVO__PTS_________B11BB010__x_______000001__DPSIM__DRV_T1_PZD3S_ui16________________; /* Index:   1417 */
  LC_TD_UDINT PLCDRVI__PTS_________B71BB010__x_______000001__DPSIM__DRV_T1_PZD3R_ui16________________; /* Index:   1418 */
  LC_TD_UDINT PLCDRVO__PTS_________B71BB010__x_______000001__DPSIM__DRV_T1_PZD3S_ui16________________; /* Index:   1419 */
  LC_TD_REAL  PLCAI____PTS_________K20GD020__BS______000001__DPSIM__aiK20GD020_BS1_ActCylStroke______; /* Index:   1420 */
  LC_TD_UDINT PLCDI____PTS_________K20GD020__SBE_____000001__DPSIM__diK20GD020_SBE1_CylExpanded______; /* Index:   1421 */
  LC_TD_UDINT PLCDI____PTS_________K20GD020__SBE_____000001__DPSIM__diK20GD020_SBE2_CylRetracted_____; /* Index:   1422 */
  LC_TD_UDINT PLCDO____PTS_________K20GD020__YVH_____000001__DPSIM__doK20GD020_YVH1_ChkVlv___________; /* Index:   1423 */
  LC_TD_REAL  PLCAO____PTS_________K20GD020__YVHP____000001__DPSIM__aoK20GD020_YVHP1_SvoVlv__________; /* Index:   1424 */
  LC_TD_REAL  PLCAI____PTS_________K20GD030__BS______000001__DPSIM__aiK20GD030_BS1_ActCylStroke______; /* Index:   1425 */
  LC_TD_UDINT PLCDI____PTS_________K20GD030__SBE_____000001__DPSIM__diK20GD030_SBE1_CylExpanded______; /* Index:   1426 */
  LC_TD_UDINT PLCDI____PTS_________K20GD030__SBE_____000001__DPSIM__diK20GD030_SBE2_CylRetracted_____; /* Index:   1427 */
  LC_TD_UDINT PLCDO____PTS_________K20GD030__YVH_____000001__DPSIM__doK20GD030_YVH1_ChkVlv___________; /* Index:   1428 */
  LC_TD_REAL  PLCAO____PTS_________K20GD030__YVHP____000001__DPSIM__aoK20GD030_YVHP1_SvoVlv__________; /* Index:   1429 */
  LC_TD_REAL  PLCAI____PTS_________K20GD040__BS______000001__DPSIM__aiK20GD040_BS1_ActCylStroke______; /* Index:   1430 */
  LC_TD_UDINT PLCDI____PTS_________K20GD040__SBE_____000001__DPSIM__diK20GD040_SBE1_CylExpanded______; /* Index:   1431 */
  LC_TD_UDINT PLCDI____PTS_________K20GD040__SBE_____000001__DPSIM__diK20GD040_SBE2_CylRetracted_____; /* Index:   1432 */
  LC_TD_UDINT PLCDO____PTS_________K20GD040__YVH_____000001__DPSIM__doK20GD040_YVH1_ChkVlv___________; /* Index:   1433 */
  LC_TD_REAL  PLCAO____PTS_________K20GD040__YVHP____000001__DPSIM__aoK20GD040_YVHP1_SvoVlv__________; /* Index:   1434 */
  LC_TD_UDINT PLCDI____PTS_________K20GD040__FD______000001__DPSIM__diK20GD040_FD1_PresReached_______; /* Index:   1435 */
  LC_TD_UDINT PLCPEW___PTS_________K20GD010__BWL_____000001__DPSIM__cnK20GD010_BWL1_ActCntVal________; /* Index:   1436 */
  LC_TD_UDINT PLCDI____PTS_________K20GD010__SBE_____000001__DPSIM__diK20GD010_SBE1_Pos1_____________; /* Index:   1437 */
  LC_TD_UDINT PLCDI____PTS_________K20GD010__SBE_____000001__DPSIM__diK20GD010_SBE2_Pos2_____________; /* Index:   1438 */
  LC_TD_UDINT PLCDI____PTS_________K20GD010__SBE_____000001__DPSIM__diK20GD010_SBE3_Pos3_____________; /* Index:   1439 */
  LC_TD_UDINT PLCDO____PTS_________K20GD010__BWL_____000001__DPSIM__doK20GD010_BWL1_CntInit__________; /* Index:   1440 */
  LC_TD_REAL  PLCAI____PTS_________K30GD020__BS______000001__DPSIM__aiK30GD020_BS1_ActCylStroke______; /* Index:   1441 */
  LC_TD_UDINT PLCDI____PTS_________K30GD020__SBE_____000001__DPSIM__diK30GD020_SBE1_CylExpanded______; /* Index:   1442 */
  LC_TD_UDINT PLCDI____PTS_________K30GD020__SBE_____000001__DPSIM__diK30GD020_SBE2_CylRetracted_____; /* Index:   1443 */
  LC_TD_UDINT PLCDO____PTS_________K30GD020__YVH_____000001__DPSIM__doK30GD020_YVH1_ChkVlv___________; /* Index:   1444 */
  LC_TD_REAL  PLCAO____PTS_________K30GD020__YVHP____000001__DPSIM__aoK30GD020_YVHP1_SvoVlv__________; /* Index:   1445 */
  LC_TD_REAL  PLCAI____PTS_________K30GD030__BS______000001__DPSIM__aiK30GD030_BS1_ActCylStroke______; /* Index:   1446 */
  LC_TD_UDINT PLCDI____PTS_________K30GD030__SBE_____000001__DPSIM__diK30GD030_SBE1_CylExpanded______; /* Index:   1447 */
  LC_TD_UDINT PLCDI____PTS_________K30GD030__SBE_____000001__DPSIM__diK30GD030_SBE2_CylRetracted_____; /* Index:   1448 */
  LC_TD_UDINT PLCDO____PTS_________K30GD030__YVH_____000001__DPSIM__doK30GD030_YVH1_ChkVlv___________; /* Index:   1449 */
  LC_TD_REAL  PLCAO____PTS_________K30GD030__YVHP____000001__DPSIM__aoK30GD030_YVHP1_SvoVlv__________; /* Index:   1450 */
  LC_TD_REAL  PLCAI____PTS_________K30GD040__BS______000001__DPSIM__aiK30GD040_BS1_ActCylStroke______; /* Index:   1451 */
  LC_TD_UDINT PLCDI____PTS_________K30GD040__SBE_____000001__DPSIM__diK30GD040_SBE1_CylExpanded______; /* Index:   1452 */
  LC_TD_UDINT PLCDI____PTS_________K30GD040__SBE_____000001__DPSIM__diK30GD040_SBE2_CylRetracted_____; /* Index:   1453 */
  LC_TD_UDINT PLCDO____PTS_________K30GD040__YVH_____000001__DPSIM__doK30GD040_YVH1_ChkVlv___________; /* Index:   1454 */
  LC_TD_REAL  PLCAO____PTS_________K30GD040__YVHP____000001__DPSIM__aoK30GD040_YVHP1_SvoVlv__________; /* Index:   1455 */
  LC_TD_UDINT PLCDI____PTS_________K30GD040__FD______000001__DPSIM__diK30GD040_FD1_PresReached_______; /* Index:   1456 */
  LC_TD_UDINT PLCPEW___PTS_________K30GD010__BWL_____000001__DPSIM__cnK30GD010_BWL1_ActCntVal________; /* Index:   1457 */
  LC_TD_UDINT PLCDI____PTS_________K30GD010__SBE_____000001__DPSIM__diK30GD010_SBE1_Pos1_____________; /* Index:   1458 */
  LC_TD_UDINT PLCDI____PTS_________K30GD010__SBE_____000001__DPSIM__diK30GD010_SBE2_Pos2_____________; /* Index:   1459 */
  LC_TD_UDINT PLCDI____PTS_________K30GD010__SBE_____000001__DPSIM__diK30GD010_SBE3_Pos3_____________; /* Index:   1460 */
  LC_TD_UDINT PLCDO____PTS_________K30GD010__BWL_____000001__DPSIM__doK30GD010_BWL1_CntInit__________; /* Index:   1461 */
  LC_TD_REAL  PLCAI____PTS_________K20RT010__BWL_____000001__DPSIM__aiK20RT010_BWL1_ActCntVal_REAL___; /* Index:   1462 */
  LC_TD_UDINT PLCPEW___PTS_________K20RT010__BWL_____000001__DPSIM__cnK20RT010_BWL1_ActCntVal________; /* Index:   1463 */
  LC_TD_UDINT PLCDI____PTS_________K20RT010__SBE_____000001__DPSIM__diK20RT010_SBE1_InSpoolPos_______; /* Index:   1464 */
  LC_TD_REAL  PLCAI____PTS_________K30RT010__BWL_____000001__DPSIM__aiK30RT010_BWL1_ActCntVal_REAL___; /* Index:   1465 */
  LC_TD_UDINT PLCPEW___PTS_________K30RT010__BWL_____000001__DPSIM__cnK30RT010_BWL1_ActCntVal________; /* Index:   1466 */
  LC_TD_UDINT PLCDI____PTS_________K30RT010__SBE_____000001__DPSIM__diK30RT010_SBE1_InSpoolPos_______; /* Index:   1467 */
  LC_TD_REAL  PLCAI____PTS_________K30RF010__BWL_____000001__DPSIM__aiK30RF010_BWL1_ActCntVal_REAL___; /* Index:   1468 */
  LC_TD_UDINT PLCPEW___PTS_________K30RF010__BWL_____000001__DPSIM__cnK30RF010_BWL1_ActCntVal________; /* Index:   1469 */
  LC_TD_UDINT PLCDI____PTS_________K30RF010__SBE_____000001__DPSIM__diK30RF010_SBE1_InSpoolPos_______; /* Index:   1470 */
  LC_TD_REAL  PLCAI____PTS_________K30RG010__BWL_____000001__DPSIM__aiK30RG010_BWL1_ActCntVal_REAL___; /* Index:   1471 */
  LC_TD_UDINT PLCPEW___PTS_________K30RG010__BWL_____000001__DPSIM__cnK30RG010_BWL1_ActCntVal________; /* Index:   1472 */
  LC_TD_UDINT PLCDI____PTS_________K30RG010__SBE_____000001__DPSIM__diK30RG010_SBE1_InSpoolPos_______; /* Index:   1473 */
  LC_TD_REAL  PLCAI____PTS_________K20RX010__BWL_____000001__DPSIM__aiK20RX010_BWL1_ActCntVal_REAL___; /* Index:   1474 */
  LC_TD_UDINT PLCPEW___PTS_________K20RX010__BWL_____000001__DPSIM__cnK20RX010_BWL1_ActCntVal________; /* Index:   1475 */
  LC_TD_UDINT PLCDI____PTS_________K20RX010__SBE_____000001__DPSIM__diK20RX010_SBE1_InSpoolPos_______; /* Index:   1476 */
  LC_TD_REAL  PLCAI____PTS_________C01CC010__BDI_____000001__DPSIM__aiC01CC010_BDI1_ActStrpThk_______; /* Index:   1477 */
  LC_TD_REAL  PLCAI____PTS_________C01CC020__BDI_____000001__DPSIM__aiC01CC020_BDI1_ActStrpThk_______; /* Index:   1478 */
  LC_TD_REAL  PLCAI____PTS_________C01CC010__BV______000001__DPSIM__aiC01CC010_BSBV1_ActStrpSpd______; /* Index:   1479 */
  LC_TD_REAL  PLCAI____PTS_________C01CC020__BV______000001__DPSIM__aiC01CC020_BSBV1_ActStrpSpd______; /* Index:   1480 */
  LC_TD_REAL  PLCAI____PTS_________C01CC030__BSD_____000001__DPSIM__aiC01CC010_BSD1_POR_ActCoilDiam__; /* Index:   1481 */
  LC_TD_REAL  PLCAI____PTS_________C01CC030__BSD_____000001__DPSIM__aiC01CC020_BSD1_TR_ActCoilDiam___; /* Index:   1482 */
  LC_TD_REAL  PLCAI____PTS_________C01CC040__BZ______000001__DPSIM__aiC01CC040_BZ1__TR_ActStrpTen____; /* Index:   1483 */
  LC_TD_UDINT PLCDI____PTS_________K13VE005__YVL_____000001__DPSIM__diK13VE005_YVL1_Expand_DiamMes___; /* Index:   1484 */
  LC_TD_UDINT PLCDI____PTS_________K13VE005__SBE_____000001__DPSIM__diK13VE005_SBE1_DiamMes_CylRtr___; /* Index:   1485 */
  LC_TD_UDINT PLCDI____PTS_________K13VE005__SBE_____000001__DPSIM__diK13VE005_SBE2_DiamMes_CylExp___; /* Index:   1486 */
  LC_TD_UDINT PLCDI____PTS_________K13VE010__BLSR____000001__DPSIM__diK13VE010_BLSR1_DiamMes_Trig____; /* Index:   1487 */
  LC_TD_UDINT PLCDI____PTS_________K13VE020__BLSR____000001__DPSIM__diK13VE020_BLSR1_WidthMes_Trig___; /* Index:   1488 */
  LC_TD_UDINT PLCDI____PTS_________K20RT010__BLSR____000001__DPSIM__diK20RT010_BLSR1_SpoolDet_Trig___; /* Index:   1489 */
  LC_TD_UDINT PLCDI____PTS_________K20RX010__BLSR____000001__DPSIM__diK20RX010_BLSR1_SpoolDet_Trig___; /* Index:   1490 */
  LC_TD_UDINT PLCDI____PTS_________K30RF010__BLSR____000001__DPSIM__diK30RF010_BLSR1_SpoolDet_Trig___; /* Index:   1491 */
  LC_TD_UDINT PLCDI____PTS_________K30RG010__BLSR____000001__DPSIM__diK30RG010_BLSR1_SpoolDet_Trig___; /* Index:   1492 */
  LC_TD_UDINT PLCDI____PTS_________K30RT010__BLSR____000001__DPSIM__diK30RT010_BLSR1_SpoolDet_Trig___; /* Index:   1493 */

 /*
  * morm: file patched
  * ----------------------------------------------------------
  * date: Monday, September 3rd 2018
  *       09:22:55h
  * ----------------------------------------------------------
  */ 

  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T2__x_______ES_POR_SupportBearing_ActCylStroke______; /* Index:   1494 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T2__x_______XS_TR_SupportBearing_ActCylStroke_______; /* Index:   1495 */
  LC_TD_UDINT PLCDO____PTS_________F25EC010__YVH_____000001__DPSIM__doF25EC010_YVH1a_POR_ExpCyl______; /* Index:   1496 */
  LC_TD_UDINT PLCDO____PTS_________F25EC010__YVH_____000001__DPSIM__doF25EC010_YVH1b_POR_RtrCyl______; /* Index:   1497 */
  LC_TD_UDINT PLCDO____PTS_________F25EC010__YVH_____000001__DPSIM__doF25EC010_YVH2_POR_ChkVlv_______; /* Index:   1498 */
  LC_TD_UDINT PLCDI____PTS_________F25EC010__SBE_____000001__DPSIM__diF25EC010_SBE1_POR_CylRtr_______; /* Index:   1499 */
  LC_TD_UDINT PLCDI____PTS_________F25EC010__SBE_____000001__DPSIM__diF25EC010_SBE2_POR_CylExp_______; /* Index:   1500 */
  LC_TD_UDINT PLCDO____PTS_________H01VW010__YVH_____000001__DPSIM__doH01VW010_YVH1a_TR_ExpCyl_______; /* Index:   1501 */
  LC_TD_UDINT PLCDO____PTS_________H01VW010__YVH_____000001__DPSIM__doH01VW010_YVH1b_TR_RtrCyl_______; /* Index:   1502 */
  LC_TD_UDINT PLCDO____PTS_________H01VW010__YVH_____000001__DPSIM__doH01VW010_YVH2_TR_ChkVlv________; /* Index:   1503 */
  LC_TD_UDINT PLCDI____PTS_________H01VW010__SBE_____000001__DPSIM__diH01VW010_SBE1_TR_CylRtr________; /* Index:   1504 */
  LC_TD_UDINT PLCDI____PTS_________H01VW010__SBE_____000001__DPSIM__diH01VW010_SBE2_TR_CylExp________; /* Index:   1505 */
 /*
  * morm: file patched
  * ----------------------------------------------------------
  * date: Monday, September 3rd 2018
  *       10:28:30h
  * ----------------------------------------------------------
  */ 

  LC_TD_REAL  MTLBSIG__PTS_________K91AA010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1506 */
  LC_TD_REAL  MTLBSIG__PTS_________K91AA010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1507 */
  LC_TD_REAL  MTLBSIG__PTS_________K91AA010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1508 */
  LC_TD_REAL  MTLBSIG__PTS_________K91AA010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1509 */
  LC_TD_REAL  MTLBSIG__PTS_________K91AA010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1510 */
  LC_TD_REAL  MTLBSIG__PTS_________K91BA010__x_______000001__T1_LngthActRoll_________________________; /* Index:   1511 */
  LC_TD_REAL  MTLBSIG__PTS_________K91BA010__x_______000001__T4_OmgActDrv____________________________; /* Index:   1512 */
  LC_TD_REAL  MTLBSIG__PTS_________K91BA010__x_______000001__T4_OmgActRoll___________________________; /* Index:   1513 */
  LC_TD_REAL  MTLBSIG__PTS_________K91BA010__x_______000001__T4_PhiActRoll___________________________; /* Index:   1514 */
  LC_TD_REAL  MTLBSIG__PTS_________K91BA010__x_______000001__T1_SpdActRoll___________________________; /* Index:   1515 */
 /*
  * morm: file patched
  * ----------------------------------------------------------
  * date: Monday, September 3rd 2018
  *       11:05:50h
  * ----------------------------------------------------------
  */ 


 /*
  * morm: file patched
  * ----------------------------------------------------------
  * date: Monday, September 3rd 2018
  *       13:56:57h
  * ----------------------------------------------------------
  */ 

  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______K91AA010_CoilTrspCar1_TravActPos________; /* Index:   1516 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______K91BA010_CoilTrspCar2_TravActPos________; /* Index:   1517 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______K91AA020_Lifter_ActCylStroke____________; /* Index:   1518 */
  LC_TD_REAL  MTLBSIG__SIM_INTERN__real______RFM_T1__x_______K91BA020_Lifter_ActCylStroke____________; /* Index:   1519 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91AA020_CoilTrspCar1_Lft_CoilInnerDiam_; /* Index:   1520 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91AA020_CoilTrspCar1_Lft_CoilOuterDiam_; /* Index:   1521 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91AA020_CoilTrspCar1_Lft_CoilStripWidth; /* Index:   1522 */
  LC_TD_BOOL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91AA020_CoilTrspCar1_Lft_CoilLoaded____; /* Index:   1523 */
  LC_TD_BOOL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91AA020_CoilTrspCar1_Lft_CoilTouch_____; /* Index:   1524 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91BA020_CoilTrspCar1_Lft_CoilInnerDiam_; /* Index:   1525 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91BA020_CoilTrspCar1_Lft_CoilOuterDiam_; /* Index:   1526 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91BA020_CoilTrspCar1_Lft_CoilStripWidth; /* Index:   1527 */
  LC_TD_BOOL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91BA020_CoilTrspCar1_Lft_CoilLoaded____; /* Index:   1528 */
  LC_TD_BOOL  UNITY____SIM_INTERN__real______RFM_T5__x_______K91BA020_CoilTrspCar1_Lft_CoilTouch_____; /* Index:   1529 */
  LC_TD_UDINT PLCDRVI__PTS_________K91AA010__x_______000001__DPSIM__CoilTrspCar1_DRV_T1_PZD1R_ui16___; /* Index:   1530 */
  LC_TD_UDINT PLCDRVI__PTS_________K91AA010__x_______000001__DPSIM__CoilTrspCar1_DRV_T2_PZD2R_ui16___; /* Index:   1531 */
  LC_TD_UDINT PLCDRVO__PTS_________K91AA010__x_______000001__DPSIM__CoilTrspCar1_DRV_T1_PZD1S_ui16___; /* Index:   1532 */
  LC_TD_UDINT PLCDRVO__PTS_________K91AA010__x_______000001__DPSIM__CoilTrspCar1_DRV_T2_PZD2S_ui16___; /* Index:   1533 */
  LC_TD_UDINT PLCDRVI__PTS_________K91BA010__x_______000001__DPSIM__CoilTrspCar2_DRV_T1_PZD1R_ui16___; /* Index:   1534 */
  LC_TD_UDINT PLCDRVI__PTS_________K91BA010__x_______000001__DPSIM__CoilTrspCar2_DRV_T2_PZD2R_ui16___; /* Index:   1535 */
  LC_TD_UDINT PLCDRVO__PTS_________K91BA010__x_______000001__DPSIM__CoilTrspCar2_DRV_T1_PZD1S_ui16___; /* Index:   1536 */
  LC_TD_UDINT PLCDRVO__PTS_________K91BA010__x_______000001__DPSIM__CoilTrspCar2_DRV_T2_PZD2S_ui16___; /* Index:   1537 */
  LC_TD_UDINT PLCDI____PTS_________K91AA010__SBE_____000001__DPSIM__diK91AA010_SBE1_TravRefPos_______; /* Index:   1538 */
  LC_TD_UDINT PLCPEW___PTS_________K91AA010__BWL_____000001__DPSIM__cnK91AA010_BWL1_TravActCnt_______; /* Index:   1539 */
  LC_TD_UDINT PLCDI____PTS_________K91AA020__FD______000001__DPSIM__diK91AA020_FD1_LiftHighPrOn______; /* Index:   1540 */
  LC_TD_UDINT PLCPEW___PTS_________K91AA020__BWL_____000001__DPSIM__cnK91AA020_BWL1_LiftActCnt_______; /* Index:   1541 */
  LC_TD_UDINT PLCDO____PTS_________K91AA020__YVH_____000001__DPSIM__doK91AA020_YVH1_LiftChkVlvDn_____; /* Index:   1542 */
  LC_TD_UDINT PLCDO____PTS_________K91AA020__YVH_____000001__DPSIM__doK91AA020_YVH1_LiftChkVlvUp_____; /* Index:   1543 */
  LC_TD_REAL  PLCAO____PTS_________K91AA020__YVHP____000001__DPSIM__aoK91AA020_YVHP1_Lift_SvoVlv_____; /* Index:   1544 */
  LC_TD_UDINT PLCDO____PTS_________K91AA030__YVH_____000001__DPSIM__doK91AA030_YVH1__LiftVlvLowPrOn__; /* Index:   1545 */
  LC_TD_UDINT PLCDI____PTS_________K91AA060__SBE_____000001__DPSIM__diK91AA060_SBE1__CoilDetected____; /* Index:   1546 */
  LC_TD_UDINT PLCDI____PTS_________K91BA010__SBE_____000001__DPSIM__diK91BA010_SBE1_TravRefPos_______; /* Index:   1547 */
  LC_TD_UDINT PLCPEW___PTS_________K91BA010__BWL_____000001__DPSIM__cnK91BA010_BWL1_TravActCnt_______; /* Index:   1548 */
  LC_TD_UDINT PLCDI____PTS_________K91BA020__FD______000001__DPSIM__diK91BA020_FD1_LiftHighPrOn______; /* Index:   1549 */
  LC_TD_UDINT PLCPEW___PTS_________K91BA020__BWL_____000001__DPSIM__cnK91BA020_BWL1_LiftActCnt_______; /* Index:   1550 */
  LC_TD_UDINT PLCDO____PTS_________K91BA020__YVH_____000001__DPSIM__doK91BA020_YVH1_LiftChkVlvDn_____; /* Index:   1551 */
  LC_TD_UDINT PLCDO____PTS_________K91BA020__YVH_____000001__DPSIM__doK91BA020_YVH1_LiftChkVlvUp_____; /* Index:   1552 */
  LC_TD_REAL  PLCAO____PTS_________K91BA020__YVHP____000001__DPSIM__aoK91BA020_YVHP1_Lift_SvoVlv_____; /* Index:   1553 */
  LC_TD_UDINT PLCDO____PTS_________K91BA030__YVH_____000001__DPSIM__doK91BA030_YVH1__LiftVlvLowPrOn__; /* Index:   1554 */
  LC_TD_UDINT PLCDI____PTS_________K91BA060__SBE_____000001__DPSIM__diK91BA060_SBE1__CoilDetected____; /* Index:   1555 */
  
  
 /*
  * morm: file patched
  * ----------------------------------------------------------
  * date: Tuesday, September 4th 2018
  *       10:12:57h
  * ----------------------------------------------------------
  */ 

  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______PORMandrelSleeveInnerDiam_______________; /* Index:   1556 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______TRMandrelSleeveInnerDiam________________; /* Index:   1557 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______PORMandrelSleeveOuterDiam_______________; /* Index:   1558 */
  LC_TD_REAL  UNITY____SIM_INTERN__real______RFM_T5__x_______TRMandrelSleeveOuterDiam________________; /* Index:   1559 */
 /*
  * morm: file patched
  * ----------------------------------------------------------
  * date: Tuesday, September 4th 2018
  *       11:48:16h
  * ----------------------------------------------------------
  */ 

  LC_TD_UDINT PVB______SIM_INTERN__ui32______RFM_____x_______Booking_Sleeve_Type_____________________; /* Index:   1560 */
}
LC_TD_HiltSimStruct;

#endif
