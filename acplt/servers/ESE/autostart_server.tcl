#!/bin/tclsh
# No parameters.
# Starts the server in this directory, loads fbds and configures via ksHTTP
# Has to be inside the acplt/servers/SERVERNAME/ dir

##### load requried libs #####
#source ../../system/systools/acplt.tcl ;#already sourced by basys.tcl #TODO create acplt package instead
source ../../system/systools/basys.tcl
#namespace import acplt::* ;#TODO imports don't work the way they should --> startServer instead of acplt::startServer e.g.

##### set global vars #####
#set acplt::logger::LOGLEVEL 0 ;# Set loglevel to trace
# server variables #TODO read from ov_server.conf
set acplt::SERVERNAME ESE
set acplt::SERVERPORT 7510
set acplt::SERVERIP localhost
### Server specific variables ###
variable CYCLETIME 200

# mark the start of the sript
acplt::logger::info "Script autostart_server for $acplt::SERVERNAME started on: [clock format [clock seconds] -format %d.%m.%y]"

##### start server #####
set started [acplt::startServer]
if {$started == 1} {
    acplt::logger::info "Script autostart_server for $acplt::SERVERNAME finished early, as server is already running."
    exit 0
}

##### Custom Functions for ESE server ##### 
proc configureConveyorSHM {UseSHM UseRFM} {

    basys::setVarList "Switch RFM Outputs in Interlock" [subst {
        "/Interlock/PE.useSHM" $UseSHM
        "/Interlock/PU.useSHM" $UseSHM
        "/Interlock/PN.useSHM" $UseSHM
        "/Interlock/PL.useSHM" $UseSHM
        "/Interlock/Furnace.useSHM" $UseSHM
    }]
    basys::setVarList "Switch RFM Inputs in Skills" [subst {
        "/Skills/Move/PE.useSHM" $UseSHM
        "/Skills/Move/Furnace.useSHM" $UseSHM
        "/Skills/Shift/PU.useSHM" $UseSHM
        "/Skills/Shift2/PU.useSHM" $UseSHM
        "/Skills/Turn/PL.useSHM" $UseSHM
        "/Skills/Heat/Furnace.useSHM" $UseSHM
    }]
    #TODO: Replace Shift2 P3STOP and P3SLOW
    basys::setVarList "Switch HandPanel Inputs in ExMode" [subst {
        "/ExMode/HandPanel.useSHM" $UseSHM
        "/ExMode/HandPanel.iexreq" $UseRFM
        "/ExMode/hcon916.on" $UseRFM
        "/ExMode/hcon2428.on" $UseRFM
        "/ExMode/NoESTOP.IN" $UseRFM
        "/ExMode/AbortOrESTOP.IN1" $UseRFM
    }]
    #TODO only works for change to SHM
    basys::setVarList "Rename IDs for PU,PN,PL Outputs in Interlock" [subst {
        "/Interlock/PUID.value" $basys::COMPONENT
        "/Interlock/PLID.value" $basys::COMPONENT
        "/Interlock/PNID.value" $basys::COMPONENT
        "/Skills/PUID.value" $basys::COMPONENT
        "/Skills/PLID.value" $basys::COMPONENT
    }]
}

proc loadConveyorCC {Name IsShift IsTurn IsFurnace Xpos Ypos {MoveDirection 0}} {
    acplt::logger::separator
    set basys::COMPONENT $Name
    basys::loadComponent PX
    # Configure conveyor specific settings:
    basys::setVarList "Disalbe unused RFM outputs" [subst {
        "/Interlock/PU.actimode" $IsShift
        "/Interlock/PN.actimode" $IsTurn
        "/Interlock/PL.actimode" $IsTurn
        "/Interlock/Furnace.actimode" "$IsFurnace"
        "/Skills/Move/Furnace.actimode" "$IsFurnace"
    }]
    basys::setVarList "Disalbe unused Skills (for performance)" [subst {
        "/Skills/Shift.actimode" $IsShift
        "/Skills/Turn.actimode" $IsTurn
        "/Skills/Heat.actimode" $IsFurnace
        "/Skills/Shift2.actimode" 0
    }]
    basys::setVarList "Disalbe OpMode changes from pass to take" {
        "/OpMode/hcon3877.on" FALSE
        "/OpMode/hcon5704.on" FALSE
    }
    #Configure move direction
    basys::setVar "/Interlock/PE.MountDir" $MoveDirection
    
    if {$IsFurnace == 1} {
        basys::setVarList "Configure Move for doors of furnace" {
            "/Skills/Move/hcon1391.on" TRUE
            "/Skills/Move/hcon6458.on" TRUE
            "/Skills/Move/ready.result" FALSE
            "/Skills/Move/end.result" FALSE
        }
    } else {
        basys::setVarList "Configure Move for doors of furnace" {
            "/Skills/Move/hcon1391.on" FALSE
            "/Skills/Move/hcon6458.on" FALSE
            "/Skills/Move/ready.result" TRUE
            "/Skills/Move/end.result" TRUE
        }
    }

    acplt::logger::info "Set orderlist and executor"
    set OrderList "\{BTAKE\}%20\{FTAKE\}%20\{BPASS\}%20\{FPASS\}%20\{JOG\}%20\{ABORT\}%20\{CLEAR\}%20\{RESET\}%20\{START\}%20\{HOLD\}%20\{UNHOLD\}%20\{SUSPEND\}%20\{UNSUSPEND\}%20\{STOP\}%20\{FREE\}%20\{OCCUPY\}%20\{PRIO\}%20\{AUTO\}%20\{MANUAL\}"
    set OrderExecutor "\{cmMode\}%20\{cmMode\}%20\{cmMode\}%20\{cmMode\}%20\{cmMode\}%20\{cmExe\}%20\{cmExe\}%20\{cmExe\}%20\{cmExe\}%20\{cmExe\}%20\{cmExe\}%20\{cmExe\}%20\{cmExe\}%20\{cmExe\}%20\{cmOcc\}%20\{cmOcc\}%20\{cmOcc\}%20\{cmExe\}%20\{cmExe\}"
    if {$IsShift == 1} {
        set OrderList "\{USHIFT\}%20\{DSHIFT\}%20$OrderList"
        set OrderExecutor "\{cmMode\}%20\{cmMode\}%20$OrderExecutor"
    }
    if {$IsTurn == 1} {
        set OrderList "\{TURN\}%20$OrderList"
        set OrderExecutor "\{cmMode\}%20$OrderExecutor" 
    }
    if {$IsFurnace == 1} {
        set OrderList "\{HEAT\}%20$OrderList"
        set OrderExecutor "\{cmMode\}%20$OrderExecutor" 
    }
    basys::setVar ".ORDERLIST" $OrderList
    basys::setVar ".ORDEREXECUTOR" $OrderExecutor

    acplt::logger::info "Delete type specific signal outputs"
    if {$IsShift == 1} {
    basys::setVar "/Skills/Shift2/SHIFTPOS.value" 1
    } else {
        basys::setVar "/c_SHIFTPOS.on" FALSE
        basys::deleteObject "/c_SHIFTPOS"
        basys::unlink ".ports" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/SHIFTPOS"
        basys::deleteObject "/SHIFTPOS"
    }
    if {$IsFurnace == 0} {
        basys::setVar "/c_TEMPFURN.on" FALSE
        basys::deleteObject "/c_TEMPFURN"
        basys::unlink ".ports" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/TEMPFURN"
        basys::deleteObject "/TEMPFURN"
    }
    basys::setVarList "Set engineering x and y position" [subst {
        ".Xpos" $Xpos
        ".Ypos" $Ypos
    }]
    configureConveyorSHM TRUE FALSE
    acplt::logger::info "Done for $basys::COMPONENT"
}


proc configureShift2 {Name} {
    acplt::logger::info "Configure extended shift to 3rd row for $Name"
    set basys::COMPONENT $Name

    #disable actimode
	basys::setVar "/Skills/Shift.actimode" 0
	#rename Shift and Shift2
	basys::renameObject "/Skills/Shift" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/Skills/ShiftTemp"
	basys::renameObject "/Skills/Shift2" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/Skills/Shift"
	basys::renameObject "/Skills/ShiftTemp" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/Skills/Shift2"
	#rename ShiftToWORKST and Shift2ToWORKST
   	basys::setVar "/Skills/ShiftToWORKST.on" FALSE
	basys::renameObject "/Skills/ShiftToWORKST" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/Skills/ShiftToWORKSTTemp"
	basys::renameObject "/Skills/Shift2ToWORKST" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/Skills/ShiftToWORKST"
	basys::renameObject "/Skills/ShiftToWORKSTTemp" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/Skills/Shift2ToWORKST"
	#change SHIFTPOS connection link
	basys::unlink "/c_SHIFTPOS.sourcefb" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/Skills/Shift2/SHIFTPOS"
	basys::link "/c_SHIFTPOS.sourcefb" "/TechUnits/$basys::FOLDERNAME/$basys::COMPONENT/Skills/Shift/SHIFTPOS"
	basys::setVar "/c_SHIFTPOS.on" TRUE
	#enable actimode
	basys::setVar "/Skills/Shift.actimode" 1

    if {$Name == "PE033"} {
        basys::setVarList "Configure shift conveyer shm adresses" {
        "/Skills/Shift/P3SLOW.address" 212
        "/Skills/Shift/P3STOP.address" 213
        }
    }
}

##### load fbds and configure server #####
# set cycletime
basys::setCycletime $CYCLETIME
acplt::ks::setVar /Tasks/UrTask.actimode 0

### Load ESEs ###
set basys::FOLDERNAME ESE
# create ESE domain
acplt::ks::createObject "/acplt/ov/domain" "/TechUnits/ESE"

## Load conveyors of firt row PE004 - PE018 ##
loadConveyorCC PE004 0 0 0 0 100 
loadConveyorCC PE005 0 0 0 360 100
loadConveyorCC PE008 0 0 0 540 100
loadConveyorCC PE009 0 0 1 720 100
loadConveyorCC PE010 0 0 0 900 100
loadConveyorCC PE011 0 0 0 1080 100
loadConveyorCC PE018 0 0 0 1440 100

## Load conveyors of second row PE031 - PE028 ##
loadConveyorCC PE031 0 0 0 360 300
loadConveyorCC PE030 0 0 0 540 300
loadConveyorCC PE025 0 1 0 720 300
loadConveyorCC PE028 0 0 0 900 300
loadConveyorCC PE027 0 0 0 1080 300

## Load shift conveyors ##
loadConveyorCC PE034 1 0 0 180 200
loadConveyorCC PE033 1 0 0 1260 200
# The lightbarriers of PE033 in move direction are reversed
basys::setVar "/Skills/Move/InvertMountDir.IN2" TRUE

## Load conveyors of third row PE013 - PE026 ##
# The motors of the 
loadConveyorCC PE013 0 0 0 360 500 1
loadConveyorCC PE016 0 0 0 540 500 1
loadConveyorCC PE019 0 0 0 720 500 1
loadConveyorCC PE021 0 0 0 900 500 1
loadConveyorCC PE026 0 0 0 1080 500 1
configureShift2 PE034
configureShift2 PE033

### reset all ###
acplt::logger::separator
acplt::ks::setVar /Tasks/UrTask.actimode 1
set ESElist [basys::getCCList]
acplt::logger::info "Reset all ESEs: $ESElist"
basys::sendCMDToAll $ESElist OCCUPY AUTOSTART 5
basys::sendCMDToAll $ESElist ABORT AUTOSTART 5
basys::sendCMDToAll $ESElist CLEAR AUTOSTART 5
basys::sendCMDToAll $ESElist RESET AUTOSTART 5
# acplt::logger::info "Select UHIFT and TURN. START"
# basys::sendCMDToAll $ESElist USHIFT AUTOSTART 3
# basys::sendCMDToAll $ESElist TURN AUTOSTART 3
# basys::sendCMDToAll $ESElist START AUTOSTART 3
# acplt::logger::info "Wait for 60 seconds to complete DHIFT and TURN"
# #TODO read EXSTs instead
# after 45000
acplt::logger::info "Stop Reset and free"
basys::sendCMDToAll $ESElist STOP AUTOSTART 5
basys::sendCMDToAll $ESElist RESET AUTOSTART 5
basys::sendCMDToAll $ESElist FREE AUTOSTART 5

### Book physical palettes ###
#TODO: Book with shm library and DO blocks instead:
# <ioelement name="PalletBooking.PalletBooking.BookIn" direction="FromSHM" type="bool" size="4" unit="none" signal="36" />
# <ioelement name="PalletBooking.PalletBooking.BookOut" direction="FromSHM" type="bool" size="4" unit="none" signal="37" />
# <ioelement name="PalletBooking.PalletBooking.PalletID" direction="FromSHM" type="string" size="16" unit="none" signal="38" />
# <ioelement name="PalletBooking.PalletBooking.PalletNumber" direction="FromSHM" type="int" size="4" unit="none" signal="42" />
# <ioelement name="PalletBooking.PalletBooking.Position" direction="FromSHM" type="string" size="16" unit="none" signal="43" />
# <ioelement name="PalletBooking.PalletBooking.PositionIndex" direction="FromSHM" type="int" size="4" unit="none" signal="47" />
proc bookPalette {name pos} {
    acplt::logger::info "Booking in physical palette $name at $pos"
    acplt::ks::createObject "/acplt/smscrm/BookPallet" "/TechUnits/Booking$name"
    acplt::ks::setVar "/TechUnits/Booking$name.cyctime" 1.0
    acplt::ks::setVar "/TechUnits/Booking$name.actimode" 1
    acplt::ks::link "/TechUnits/Booking$name.taskparent" /Tasks/UrTask
    acplt::ks::setVar "/TechUnits/Booking$name.useSHM" TRUE
    acplt::ks::setVar "/TechUnits/Booking$name.ID" $name
    acplt::ks::setVar "/TechUnits/Booking$name.Location" $pos
    after 1000
    acplt::ks::setVar "/TechUnits/Booking$name.Book" TRUE
    after 1000
    acplt::ks::setVar "/TechUnits/Booking$name.Book" FALSE
}
acplt::logger::separator
bookPalette PA001 PE004
bookPalette PA002 PE031

##### finish script #####
acplt::logger::separator
acplt::logger::info "Script autostart_server for $acplt::SERVERNAME finished."
exit 0