#!/bin/tclsh
# No parameters.
# Starts the server in this directory, loads fbds and configures via ksHTTP
# Has to be inside the acplt/servers/SERVERNAME/ dir

##### load requried libs #####
#source ../../system/systools/acplt.tcl ;#already sourced by basys.tcl #TODO create acplt package instead
source ../../system/systools/basys.tcl
#namespace import acplt::* ;#TODO imports don't work the way they should --> startServer instead of acplt::startServer e.g.

##### set global vars #####
#set acplt::logger::LOGLEVEL 0 ;# Set loglevel to trace
# server variables #TODO read from ov_server.conf
set acplt::SERVERNAME GSE
set acplt::SERVERPORT 7511
set acplt::SERVERIP localhost
### Server specific variables ###
variable CYCLETIME 100

# mark the start of the sript
acplt::logger::info "Script autostart_server for $acplt::SERVERNAME started on: [clock format [clock seconds] -format %d.%m.%y]"

##### start server #####
set started [acplt::startServer]
if {$started == 1} {
    acplt::logger::info "Script autostart_server for $acplt::SERVERNAME finished early, as server is already running."
    exit 0
}

##### Custom Functions for GSE server ##### 
proc loadMLOpModeAndModel {Name} {
    acplt::logger::info "Loading ML operation mode and ML model for $Name"
    # ML operation mode
    set templateFBD [file normalize $::acplt::THISACPLTSYSTEM/templates/basys/TechUnits_GSE_PX-ML.fbd]
    set targetFBD [file normalize $::acplt::THISACPLTSYSTEM/servers/$::acplt::SERVERNAME/modelinstances/$Name-ML.fbd]
    file copy -force $templateFBD $targetFBD
    basys::replaceName $targetFBD PX $Name ""
    acplt::loadInstance $Name-ML.fbd
    
    # MLModel
    set templateFBD [file normalize $::acplt::THISACPLTSYSTEM/templates/basys/TechUnits_GSE_PX-MLModel.fbd]
    set targetFBD [file normalize $::acplt::THISACPLTSYSTEM/servers/$::acplt::SERVERNAME/modelinstances/$Name-MLModel.fbd]
    file copy -force $templateFBD $targetFBD
    basys::replaceName $targetFBD PX $Name ""
    acplt::loadInstance $Name-MLModel.fbd
}

proc loadPaletteCC {Name Position Xpos Ypos} {
    acplt::logger::separator
    set basys::COMPONENT $Name
    basys::loadComponent PX
    basys::setVarList "Reset order output and process image" {
        "/Skills/PImage/CStatusExt.doReset" TRUE
        "/Skills/PImage/NStatusExt.doReset" TRUE
        "/Interlock/SetCCMDext.doReset" TRUE
        "/Interlock/SetNCMDext.doReset" TRUE
    }
    after 500
    basys::setVarList "Set order output connection" {
        "/Skills/PImage/CStatusExt.doCyclic" TRUE
        "/Skills/PImage/NStatusExt.doCyclic" TRUE
    }
    basys::setVarList "Set position" [subst {
        "/Current.value" $Position
        ".Xpos" $Xpos
        ".Ypos" $Ypos
    }]
    # Invert shift direction ?
    #curl "http://"$SERVERIP":"$SERVERPORT"/setVar?format=plain&path=/TechUnits/GSE/PA001/Skills/Move/Execute/UorD.IN1" DSHIFT"
    #curl "http://"$SERVERIP":"$SERVERPORT"/setVar?format=plain&path=/TechUnits/GSE/PA001/Skills/Move/Execute/UorD.IN2" USHIFT"
    basys::resetToIdle AUTOSTART
}

##### load fbds and configure server #####
### Load GSEs ###
set basys::FOLDERNAME GSE
# Create GSE domain
acplt::ks::createObject "/acplt/ov/domain" "/TechUnits/GSE"

## Load palette PA001 ##
loadPaletteCC "PA001" "PE004" 0 100
loadMLOpModeAndModel "PA001"

## Topology ##
# Load topology
acplt::loadTemplate basys/TechUnits_Topology-erweitert.fbd
# Config topology agent
acplt::logger::info "Configuring palette topology agent"
set PalleteList "\{/TechUnits/GSE/PA001\}"
#PALETTELIST="\{/TechUnits/GSE/PA001\}%20\{/TechUnits/GSE/PA002\}%20\{/TechUnits/GSE/PA003\}"
acplt::ks::setVar /TechUnits/GSE/TopologyAgent.PalettePaths $PalleteList

##### finish script #####
acplt::logger::separator
# set cycletime
basys::setCycletime $CYCLETIME
acplt::logger::info "Script autostart_server for $acplt::SERVERNAME finished."
exit 0