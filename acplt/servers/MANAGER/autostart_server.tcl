#!/bin/tclsh
# No parameters.
# Starts the server in this directory, loads fbds and configures via ksHTTP
# Has to be inside the acplt/servers/SERVERNAME/ dir

##### load requried libs #####
source ../../system/systools/acplt.tcl
#namespace import acplt::* ;#TODO imports don't work the way they should --> startServer instead of acplt::startServer e.g.

##### set global vars #####
# server variables #TODO read from ov_server.conf
#set acplt::SERVERNAME MANAGER
#set acplt::SERVERPORT 7509
#set acplt::SERVERIP localhost

# mark the start of the sript
acplt::logger::info "Script autostart_server for $acplt::SERVERNAME started on: [clock format [clock seconds] -format %d.%m.%y]"

##### start server #####
set started [acplt::startServer]

### Demo specific: load HMI (engineering and control) ###
# only load on new startup of server
if {$started == 0} {
    acplt::loadTemplate cshmi/StaticDisplayComponent.fbd
    acplt::loadTemplate cshmi/Templates_Engineering.fbd
    acplt::loadTemplate cshmi/Sheet_EngineeringFB.fbd
    acplt::loadInstance data_kshttp_ese.fbd
    acplt::loadInstance data_kshttp_gse.fbd
}

##### finish script #####
acplt::logger::info "Script autostart_server for $acplt::SERVERNAME finished."
exit 0